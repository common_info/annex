# List of Terms and Abbreviations #

| Термин | Расшифровка | Русский перевод | Комментарий |
| :----- | :---------- | :-------------- | :---------- |
| 3DES | Triple-DES |  | алгоритм шифрования на основе последовательного применения DES трижды |
| 3GPP | 3rd Generation Partnership Project | Проект партнерства третьего поколения |  |
| A2P-SMS | Application-to-Person SMS | сообщение, отправленное абоненту от приложения или сервиса |  |
| AAA | Authentication, Authorization, and Accounting | аутентификация, авторизация и учет |  |
| AAL5 | ATM Adaptation Layer Type 5 | уровень адаптации ATM |  |
| ABAS | Aircraft Based Augmentation System | бортовая система дифференциальных поправок |  |
| ABC | Application Based Charging | тарификация на базе приложений |  |
| ABC-номер |  |  | телефонный код, присвоенный по географическому признаку |
| ABMF | Account Balance Management Function | функция управления состоянием счета |  |
| ABNF | Augmented Backus-Naur Form | расширенная форма Бэкуса-Наура |  |
| ACDC | Application-Specific Congestion Control for Data Communication | специализированная прикладная система управления перегрузкой для обмена информацией |  |
| ACE | Application Control Engine | система управления приложением |  |
| ACK | Acknowledge | подтверждение |  |
| ACL | APN Control List | управляющий список APN |  |
| ACM | Address Complete Message | сообщение о приеме полного номера ISUP |  |
| ACN | Application Context Name | имя контекста приложения |  |
| ACQ | All Call Query | запрос по всем вызовам |  |
| ACS | Access Control Server | сервер управления доступом |  |
| ACS | Active Codec Set | набор активных кодеков |  |
| ACSE | Association Control Service Element | сервисный элемент управления ассоциацией |  |
| ACT | Active Codec Type | тип активных кодеков |  |
| ADC | Automatic Device Configuration | автоматическая настройка устройств |  |
| ADD | Access Domain Data | данные домена доступа |  |
| ADD | Automatic Device Detection | автоматическое обнаружение устройств |  |
| ADF | Application Data File | файл данных приложения |  |
| ADN | Abbreviated Dialling Numbers | номер сокращенного набора |  |
| ADSI | Analog Display Services Interface | аналоговый интерфейс системы обслуживания дисплеев |  |
| ADSP | Analog-Digital Signal Processing | аналого-цифровая обработка сигналов |  |
| AERM | Alignment Error Rate Monitoring | контроль коэффициента ошибок синхронизации |  |
| AES | Advanced Encrypting Scheme |  | общепринятый стандарт шифрования Rijndael |
| AF | Application Function | прикладная функция |  |
| AFTN | Aeronautical Fixed Telecommunications Network | наземная сеть авиационной фиксированной связи |  |
| AGCH | Access Grant Channel | канал предоставления доступа |  |
| AICA | Automatic Initial Call Attempt | автоматическая попытка начального вызова | один из этапов вызова |
| AID | Application Identifier | идентификатор приложения |  |
| AIDC | Air Traffic Services Interfacility Data Communications | передача данных между центрами управления воздушным движением |  |
| AIS | Alarm Indication Signal | сигнал индикации тревоги |  |
| ALB | Adaptive Load Balancing | адаптивная балансировка нагрузки |  |
| ALCAP | Access Link Control Application Protocol | прикладной протокол управления доступом к каналу |  |
| ALG | Application Layer Gateway | шлюз уровня приложений |  |
| ALPN | Application-Layer Protocol Negotiation | согласование протокола прикладного уровня |  |
| ALSI | Application Level Subscriber Identity | идентификатор абонента прикладного уровня |  |
| AMA | Automatic Message Accounting | автоматическая система учета |  |
| AMBR | Aggregate Maximum Bit Rate | суммарная максимальная скорость передачи цифровых данных |  |
| AMF | Access and Mobility Management Function | функция управления доступом и мобильностью |  |
| AMHS-ATS | Air Traffic Services Message Handling Services | служба обработки авиационных сообщений |  |
| AMR | Adaptive Multi Rate | адаптивное кодирование с переменной скоростью |  |
| ANDSP | Access Network Discovery and Selection Policy | политика обнаружения и выбора сети доступа |  |
| ANID | Access Network Identity | идентификатор сети доступа |  |
| ANM | Answer Message | ответное сообщение от вызываемого абонента ISUP |  |
| AoC | Advice of Charge | уведомление об оплате |  |
| AOCC | Advice of Charge Charging | сбор абонентской платы |  |
| AOCI | Advice of Charge Information | информация уведомления об оплате |  |
| AOR | Address of Record | адрес регистрации |  |
| AP | Alarm Processor | подсистема обработки аварий |  |
| APC | Alias Point Code | код смежной сигнальной точки |  |
| APDU | Application Protocol Data Unit | блок данных прикладного протокола |  |
| APM | Application Transport Mechanism | прикладной транспортный механизм |  |
| APN | Access Point Name | имя точки доступа |  |
| APRI | Address Presentation Restriction Indicator | индикатор запрета отображения адреса |  |
| APSD | Application Provider Security Domain | домен безопасности поставщика приложений |  |
| ARP | Address Resolution Protocol | протокол разрешения адресов |  |
| ARP | Allocation and Retention Priority | приоритет при распределении и хранении |  |
| ARPF | Authentication Credential Repository and Processing Function | функция хранилища и обработки учетных данных аутентификации |  |
| AS | Application Server |  | узел дополнительного управления вызовом или логический объект SIGTRAN, обрабатывающий сигнализацию |
| AS | Application Service | служба приложений |  |
| ASCII | American Standard Code for Information Interchange | американский стандартный код обмена информацией |  |
| ASE | Application Service Element | прикладной сервисный элемент |  |
| ASIC | Application Specific integrated Circuit | специализированная прикладная интегральная схема |  |
| ASME | Access Security Management Entity | узел управления безопасностью доступа |  |
| ASN1 | Abstract Syntax Notation One | абстрактная синтаксическая нотация |  |
| ASP | Application Server Process | отдельный экземпляр AS SIGTRAN |  |
| ASP | Application Service Provider | поставщик услуг доступа к приложениям |  |
| ATA | Advanced Technology Attachment | параллельный интерфейс подключения накопителей |  |
| ATA | Analog Telephony Adapter | аналоговый телефонный адаптер |  |
| ATD | Absolute Time Difference | абсолютная разница во времени |  |
| ATE | Automatic Test Equipment | автоматическая система контроля |  |
| ATM | Asynchronous Transfer Mode | режим асинхронной передачи |  |
| ATVN | AMR-TFO Version Number | номер версии AMR-TFO |  |
| AuC | Authentication Center | центр аутентификации |  |
| AUSF | Authentication Server Function | функция сервера аутентификации |  |
| AUTN | Authentication Token | токен для аутентификации |  |
| AVP | Attribute-Value Pair | пара "атрибут-значение" |  |
| AVS | Application Virtual Switch | виртуальный коммутатор приложений |  |
| B-TID | Bootstrapping Transaction Identifier | идентификатор транзакции начальной загрузки | используется для привязки идентификатора пользователя к контексту безопасности |
| B2BUA | Back-to-Back User Agent | пользовательский агент Back-to-Back |  |
| BAIC | Barring of All Incoming Calls | запрет всех входящих вызовов |  |
| BAOC | Barring of All Outgoing Calls | запрет всех исходящих вызовов |  |
| BBSM | Building Broadband Service Manager | шлюз для доступа к публичным сетям |  |
| BCC | Basic Call Controller | контроллер базового вызова |  |
| BCC | Basic Station Colour Code | код раскраски базовой станции |  |
| BCCM | Basic Call Controller Manager | узел управления контроллером базового вызова |  |
| BCI | Backward Call Indicator | индикатор обратного вызова |  |
| BCM | Bearer Control Mode | режим управления переносом информации |  |
| BCSM | Basic Call State Model | базовая модель состояния вызова CAMEL |  |
| BDN | Barred Dialling Number | запрещенный номер набора |  |
| BER | Basic Encoding Rules | базовые правила кодирования |  |
| BFCP | Binary Floor Control Protocol | протокол управления на двоичном уровне | протокол для обмена презентациями во время видеоконференций |
| BGCF | Breakout Gateway Control Function | узел управления шлюзами |  |
| BGP | Border Gateway Protocol | протокол граничного шлюза |  |
| BIB | Backward Indicator Bit | указательный бит назад |  |
| BIC | Barring of Incoming Calls when roaming outside HPLMN | запрет входящих вызовов в роуминге за пределами домашней сети оператора |  |
| BICC | Bearer Independent Call Control | управление вызовом независимо от несущего канала |  |
| BIP | Bearer Independent Protocol | протокол обмена данными независимо от канала передачи |  |
| BLA | Blocking Acknowledgement | подтверждение блокировки канала ISUP |  |
| BLF | Busy Lamp Field | панель индикации состояния абонентов |  |
| BLO | Blocking | блокировка канала ISUP |  |
| BMD | Billing Mediation Device | промежуточное устройство биллинга |  |
| BOIC | Barring of Outgoing International Calls | запрет исходящих международных вызовов |  |
| BOIC-exHC | Barring of Outgoing International Calls Except Those Directed to the HPLMN country | запрет исходящих международных вызовов, за исключением направленных в домашнюю сеть PLMN |  |
| BPC | Backplane Programmable Automation Controller | программируемый контроллер автоматизации системной платы |  |
| BRAS | Broadband Remote Access Server | широкополосный сервер удаленного доступа |  |
| BRI | Basic Rate Interface | интерфейс базового доступа |  |
| BRT | Billing Real-Time | биллинг в режиме реального времени |  |
| BSC | Base Station Controller | контроллер базовых станций |  |
| BSDB | Business Services Database | база данных агентов бизнес-логики |  |
| BSF | Binding Support Function | функция поддержки привязки |  |
| BSN | Backward Sequence Number | номер последовательности назад |  |
| BSNR | Backward Sequence Number Routing | маршрутизация номера последовательности назад |  |
| BSS | Base Station Subsystem | подсистема базовых станций |  |
| BSSAP | Base Station Subsystem Application Part | протокол прикладной части подсистемы базовых станций |  |
| BSSGP | Base Station Subsystem GPRS Protocol | протокол подсистемы базовых станций в сетях GPRS |  |
| BTC | Bothway Throughconnect | двухстороннее проключение |  |
| BTS | Base Transceiver Station | базовая передающая станция |  |
| BV | Bon Voyage | система оповещения роумеров при пересечении границы партнерской сети связи |  |
| BVCI | BSSGP Virtual Connection Identifier | идентификатор виртуального подключения по протоколу BSSGP |  |
| BWT | Block Waiting Time | время ожидания блока |  |
| C-APDU | Command-APDU | команда протокольного блока данных прикладного уровня |  |
| C-TPDU | Command-TPDU | команда протокольного блока данных транспортного уровня |  |
| CA | Call Alert | уведомление о вызовах |  |
| CA | Component Address | компонентный адрес |  |
| CAB | Conference Acceptance Backward | конференцсвязь в обратном направлении |  |
| CAC | Connection Admission Control | контроль допустимости соединений |  |
| CACS | Common Active Codec Set | общий набор активных кодеков |  |
| CAF | Conference Acceptance Forward | конференцсвязь в прямом направлении |  |
| CAMEL | Customized Applications for Mobile Networks Enhanced Logic |  | набор стандартов, реализующих интеллектуальные услуги связи в сетях GSM |
| CAP | CAMEL Application Part | прикладная часть протокола CAMEL |  |
| CAPS | Call Attempts per Second | количество попыток вызова в секунду |  |
| captcha | Completely Automated Public Turing Test to Tell Computers and Humans Apart | тест для различения человека и компьютера |  |
| CAS | Channel Associated Signaling | сигнализация по выделенному каналу |  |
| CASD | Controlling Authority Security Domain | контрольный орган доменов безопасности |  |
| CASP | Content and Application Service Provider | провайдер услуг с контентом и приложениями |  |
| CAT | Card Application Toolkit | пакет прикладного программного обеспечения для карт |  |
| CATPT | Card Application Toolkit Protocol Teleservice | услуга протокола пакетов прикладного программного обеспечения для карт |  |
| CBC | Cell Broadcast Center | центр вещания по сотам |  |
| CBC | Cipher Block Chaining | сцепление шифрованных блоков | один из режимов шифрования |
| CBE | Cell Broadcast Entity | узел вещания по сотам |  |
| CBMI | Cell Broadcast Message Identifier | идентификатор сообщения сотового вещания |  |
| CBMID | Cell Broadcast Message Identifier for Data Download | идентификатор сообщения сотового вещания для загрузки данных |  |
| CBMIR | Cell Broadcast Message Identifier Range | интервал допустимых идентификаторов сообщений сотового вещания |  |
| CBS | Cell Broadcast Service | услуга трансляции по сотам |  |
| CBSP | Cell Broadcast Service Protocol | протокол трансляции по сотам | протокол взаимодействия CBC и RNC |
| CC | Credit-Control | кредитный контроль |  |
| CC | Cryptographic Checksum | криптографическая хэш-сумма |  |
| CCB | Customer Care and Billing System | система обслуживания абонентов и учёта стоимости |  |
| CCBS | Completion of Calls to Busy Subscriber | дозвон до занятого абонента |  |
| CCCH | Common Control Channel | общий канал управления |  |
| CCD | Call Center Dialler | абонент с оформленной подписку с помощью оператора поддержки пользователей |  |
| CCD | Conference Call Device | устройство конференц-связи |  |
| CCE | Control Channel Element | элемент канала управления |  |
| CCF | Call Control Function | функция управления вызовами |  |
| CCF | Call Control Function | функция управления вызовом |  |
| CCF | Charging Collection Function | функция сбора тарификационных данных |  |
| CCH | Control Channel | канал управления |  |
| CCI | Capability/Configuration Identifier | идентификатор возможностей/конфигурации |  |
| CCO | Cell Change Order | порядок смены соты |  |
| CCP | Capability Configuration Parameter | параметр настройки возможностей |  |
| CCPCH | Common Control Physical Channel | общий физический канал управления |  |
| CCT | Cryptographic Checksum Template | шаблон криптографической хэш-суммы |  |
| CCU | Channel Codec Unit | блок кодека канала |  |
| CD | Call Deflection | отбой вызова |  |
| CDF | Charging Data Function | функция обработки данных о тарификации |  |
| CDIV | Call Diversion | переключение связи |  |
| CDM | Continuous Diagnostics and Mitigation | система диагностики и предотвращения аварий |  |
| CDMA | Code Division Multiple Access | многостанционный доступ с кодовым разделением каналов |  |
| CdPA | Called Party Address | адрес вызываемого абонента |  |
| CdPN | Called Party Number | номер вызываемого абонента |  |
| CDR | Call Detail Record | подробная запись о вызове |  |
| CED | Called Tone |  | сигнал тональной частоты 2100 Гц, определяющий вызываемое устройство как факсимильный аппарат |
| CellID | Cell Identifier | идентификатор базовой станции сети GSM |  |
| CER | Canonical Encoding Rules | канонические правила кодирования |  |
| CES | Circuit Emulation Service | служба эмуляции соединений |  |
| CF | Call Forwarding | переадресация вызова |  |
| CFR | Confirmation to Receive | кадр подтверждения готовности к приёму |  |
| CFSI | Change F-TEID support indication | индикация поддержки изменения F-TEID |  |
| CG | Cell Global | глобальная сота |  |
| CGBA | Circuit Group Blocking Acknowledge | подтверждение блокировки группы каналов |  |
| CGF | Charging Gateway Function | функция биллингового шлюза |  |
| CGI | Cell Global Identifier | глобальный идентификатор соты |  |
| CgPA | Calling Party Address | адрес вызывающего абонента |  |
| CgPC | Calling Party Category | категория вызывающего абонента |  |
| CgPN | Calling Party Number | номер вызывающего абонента |  |
| CGUA | Circuit Group Unblocking Acknowledge | подтверждение разблокировки группы каналов |  |
| CHAP | Challenge Handshake Authentication Protocol |  | протокол аутентификации с предварительным согласованием вызова |
| CHF | Charging Function | функция тарификации |  |
| CHV | Card Holder Verification | подтверждение владельца карты |  |
| CI | Carrier Identifier | идентификатор оператора сотовой связи |  |
| CI | Cell Identity | идентификатор соты |  |
| CIC | Circuit Identity Code | код идентификации канала |  |
| CIDR | Classless Inter-Domain Routing | бесклассовая адресация |  |
| CIR | Carrier to Interference Ratio | отношение несущей к помехе |  |
| CL | Connection Logic | логика, работающая с соединениями |  |
| CLI | Calling Line Identifier | идентификатор линии вызова |  |
| CLIP | Calling Line Identification Presentation | идентификация номера вызывающего абонента |  |
| CLIR | Calling Line Identification Restriction | запрет идентификации номера вызывающего абонента |  |
| CLK | Clock | часы |  |
| CMAS | Commercial Mobile Alert Service | коммерческая услуга мобильного оповещения |  |
| CMC | Connection Mobility Control | управление мобильностью соединения |  |
| CMI | CSG Membership Indication | индикатор членства в закрытой группе абонентов |  |
| CMIP | Common Management Information Protocol | протокол общей управляющей информации |  |
| CMISE | Common Management Information Service | услуга общей управляющей информации |  |
| CMM | Circuit Mobility Management | управление мобильностью каналов |  |
| CMSDB | Call Management Services Database | база данных услуг управления вызовами |  |
| CMT | Cellular Messaging | передача сообщений в сотовой сети |  |
| CNAP | Calling Name Presentation | представление имени вызывающего абонента |  |
| CNG | Calling Tone |  | повторяющийся сигнал тональной частоты 1100 Гц, определяющий вызывающее устройство как факсимильный аппарат |
| CNG | Comfort Noise Generator | генератор комфортного шума |  |
| CNL | Co-Operative Network List | список совместно действующих сетей |  |
| CNP | Connected/Called IN Number Presentation | представление номера вызываемой стороны |  |
| CNTR | Counter | счетчик |  |
| CO | Call to be Offered | попытка вызова |  |
| CoA | Care-Of Address | адрес, назначаемый в момент регистрации пользователя в сети |  |
| COLP | Connected Line Identification Presentation | представление идентифицированного номера на подключенном соединении |  |
| COLR | Connected Line Identification Restriction | запрет представления идентифицированного номера на подключенном соединении |  |
| COM | Component Operation and Maintenance | эксплуатация и техническое обслуживание компонент |  |
| CON | Connect | сообщение о получении ответа от вызываемого абонента в ISUP |  |
| CoS | Class of Service | класс обслуживания |  |
| CPBCCH | Compact Packet Broadcast Control Channel | компактный канал широковещательной пакетной передачи |  |
| CPDLC | Controller Pilot Data Link Communications | связь "диспетчер-пилот" по линии передачи данных |  |
| CPE | Control Point Entity | модуль служебных логик |  |
| CPE | Control Processing Entity | управляющий процессор |  |
| CPE | Customer Premises Equipment | оборудование конечных пользователей | используется при указании на платформу Protei CPE |
| CPM | Central Processor Module | модуль главного процессора |  |
| CPS | Calls per Second | количество вызовов в секунду |  |
| CPT | Cellular Paging | сотовая пейджинговая связь |  |
| CPU | Central Processing Unit | центральный процессор |  |
| CQI | Channel Quality Information | информация о качестве работы канала |  |
| CRC4 | Cyclic Redundancy Check 4 | метод контроля целостности данных потока Е1 | ITU-T G.704 |
| CRS | Call Recording System | система записи вызовов |  |
| CRSI | Change Reporting support indication | индикация поддержки отправки отчетов об изменении |  |
| CS | Call Segment | сегмент вызова |  |
| CS | Circuit Switch | коммутация каналов |  |
| CSA | Call Segment Association | ассоциация сегментов вызова |  |
| CSA | Connection Security Association | ассоциация защищенных соединений | задание набора общих параметров для поддержания безопасной связи между двумя сетевыми узлами |
| CSA_ID | Connection Security Association Identity | идентификатор CSA |  |
| CSAMAC | Connection Security Association Message Authentication Code | код проверки подлинности сообщения CSA |  |
| CSCF | Call Session Control Function | функция управления сеансом голосового вызова |  |
| CSCS | Common Supported Codec Set | общий набор поддерживаемых кодеков |  |
| CSD | Circuit Switched Data | передача данных по коммутируемым каналам |  |
| CSE | CAMEL Service Environment | окружение услуги CAMEL |  |
| CSFB | Circuit Switch Fallback | переключение на коммутируемую сеть | технология, обеспечивающая возможность входящих и исходящих вызовов в сети LTE с использованием мобильных сетей с коммутацией каналов |
| CSG | Closed Subscriber Group | закрытая группа абонентов |  |
| CSI | CAMEL Subscription Information | информация о подписках для протоколов CAMEL |  |
| CSL | Component Sublevel | подуровень компонентов | подуровень системы TCAP |
| CSRN | CS Domain Routing Number | номер маршрутизации в домене CS |  |
| CSU | Channel Service Unit | устройство обслуживания канала |  |
| CSV | Comma Separated Value | формат данных с разделением значений запятыми |  |
| CT | Command Type | дальнейшая операция с сообщением |  |
| CUG | Closed User Group | замкнутая группа пользователей |  |
| CUPS | Control Plane and User Plane Separation | разделение управляющей и пользовательской плоскостей |  |
| CW | Call Waiting | ожидание вызова |  |
| D2D | Device-to-Device | режим связи между двумя абонентскими терминалами |  |
| DA | Diameter Agent | оборудование с функцией агента протокола Diameter |  |
| DACP | Diameter Credit-Control Application Protocol |  | один из базовых протоколов Diameter |
| DACS | Distant Active Codec Set | общий набор активных кодеков удаленного доступа |  |
| DAD | Destination Address | адрес назначения |  |
| DAF | Dual Address Bearer Flag | флаг поддержки адресов транспортных сервисов обоих форматов: IPv4 и IPv6 |  |
| DAP | Data Authentication Pattern | шаблон аутентификации данных |  |
| DAVA | Destination Available | доступ к пункту назначения свободен |  |
| DBI | Database Interface | интерфейс базы данных |  |
| DBRM | Database Request Machine | подсистема запросов к базе данных |  |
| DCC | Diameter Credit-Control |  | один из базовых протоколов Diameter |
| DCK | Depersonalization Control Key | ключ управления деперсонализацией |  |
| DCN | Dedicated Core Network | выделенная опорная сеть |  |
| DCS | Data Coding Scheme | схема кодирования данных |  |
| DCS | Digital Command Signal | сигнал цифровой команды |  |
| DEA | Diameter Edge Agent | пограничный агент сигнальной сети оператора в протоколе Diameter |  |
| DECT | Digital Enhanced Cordless Telecommunications | усовершенствованная цифровая беспроводная связь |  |
| DEF-номер |  |  | телефонный код, присвоенный по не географическому признаку |
| DER | Distinguished Encoding Rules | особые правила кодирования |  |
| DES | Data Encryption Standard | стандарт для шифрования данных |  |
| DET | Detach | отсоединение |  |
| DF | Dedicated File | специальный выделенный файл |  |
| DFI | Direct Forwarding Indication | индикация прямой переадресации вызовов |  |
| DGI | Data Grouping Identifier | идентификатор группирования данных |  |
| DHCP | Dynamic Host Configuration Protocol | протокол динамической настройки хоста |  |
| DIS | Digital Identification Signal | цифровой опознавательный сигнал |  |
| DISA | Dial-In System Access | прямой внутрисистемный доступ |  |
| DL | Downlink | нисходящая линия |  |
| DLCI | Data Link Connection Identifier | идентификатор соединения линии передачи данных |  |
| DLD | Data Link Discriminator | дискриминатор линии передачи данных |  |
| DME | Distance Measuring Equipment | дальномерное оборудование |  |
| DNIC | Data Network Identification Code | цифровой код сети передачи данных |  |
| DNIS | Dialled Number Identification Service | служба определения набранного номера |  |
| DNL | Destination Number Length | длина номера назначения |  |
| DNNS | Intra Domain NAS Node Selector | выбор узла NAS в пределах домена |  |
| DNS | Domain Name Server | сервер доменных имен |  |
| DoS | Denial of Service | отказ в обслуживании |  |
| DP | Detection Point | точка обнаружения | место генерирования уведомлений в сервис-логике в процессе обработки данных и передачи управления в gsmSCF |
| DPC | Destination Point Code | код сигнальной точки назначения |  |
| DPCCH | Dedicated Physical Control Channel | выделенный физический канал управления |  |
| DPCH | Dedicated Physical Channel | выделенный физический канал |  |
| DPDCH | Dedicated Physical Data Channel | выделенный физический канал передачи данных |  |
| DPDK | Data Plane Development Kit | набор библиотек для ускорения обработки пакетных данных |  |
| DPXA | Diameter Proxy Agent | функция прокси протокола Diameter |  |
| DRA | Diameter Routing Agent | агент маршрутизации протокола Diameter |  |
| DRAC | Dynamic Resource Allocation Control | управление динамическим распределением ресурсов |  |
| DRB | Data Radio Bearer | радиоканал передачи данных |  |
| DRDA | Diameter Redirect Agent | функция перенаправления протокола Diameter |  |
| DRLA | Diameter Relay Agent | функция переключения протокола Diameter |  |
| DRMP | Diameter Routing Message Priority | приоритет сообщения маршрутизации в протоколе Diameter |  |
| DRNC | Drift Radio Network Controller | дрейфующий контроллер радиосети |  |
| DRST | Destination Restricted | доступ к пункту назначения запрещен |  |
| DRX | Discontinuous Reception | технология прерывистой передачи |  |
| DS | Digital Signature | цифровая подпись |  |
| DSAI | Dynamic Service Activation Information | информация о динамической активации услуги |  |
| DSCP | Differentiated Services Code Point | поле кода дифференцированных услуг |  |
| DSCS | Distant Supported Codec Set | общий набор поддерживаемых кодеков удаленного доступа |  |
| DSMIP | Dual-Stack Mobile IPv6 | двухстековый протокол IPv6 обеспечения мобильности |  |
| DSP | Digital Signal Processor | процессор цифровой обработки сигналов |  |
| DSS | Direct Station Selection | прямой выбор терминала |  |
| DSS1 | Digital Subscriber Signaling | цифровая абонентская сигнализация | протокол сигнализации ISDN для взаимодействия оконечного абонентского оборудования с другими устройствами |
| DST | Daylight Saving Time | переход на летнее время |  |
| DST | Digital Signature Template | шаблон цифровой подписи |  |
| DSU | Data Service Unit | сервисный блок передачи данных |  |
| DT | Discharge Time | время прекращения валидности состояния |  |
| DTAP | Direct Transfer Application Part | прикладная часть протокола прямой передачи |  |
| DTF | Direct Tunnel Flag | флаг прямого туннеля |  |
| DTI | Direct Tunnel Indicator | индикатор прямого туннеля |  |
| DTMF | Dual-Tone Multi-Frequency | двухтональный многочастотный набор |  |
| DUNA | Destination Unavailable | пункт назначения недоступен |  |
| DUP | Data User Part | абонентская подсистема передачи данных |  |
| DUPU | Destination User Part Unavailable | пользовательская часть пункта назначения недоступна |  |
| DVMRP | Distance Vector Multicast Routing Protocol | дистанционно-векторный протокол многоадресной маршрутизации |  |
| DWDM | Dense Wavelength Division Multiplexing | мультиплексирование с разделением по длине волны |  |
| E-DCH | Enhanced Dedicated Channel | усовершенствованный выделенный канал |  |
| E-DSS1 | Extended Digital Subscriber Signaling | расширенная цифровая абонентская сигнализация | протокол сигнализации ISDN для взаимодействия оконечного абонентского оборудования с другими устройствами |
| E-UTRAN | Evolved Universal Terrestrial Radio Access Network | улучшенная универсальная сеть наземного радиодоступа стандарта LTE |  |
| EAL | Environment Abstraction Layer | уровень абстракции окружения | программное обеспечение, обеспечивающее работу DPDK на каком-либо конкретном оборудовании для конкретной операционной системы |
| EAP | Extensible Authentication Protocol | расширяемый протокол проверки подлинности |  |
| EAP-AKA | Extensible Authentication Protocol Method for UMTS Authentication and Key Agreement | расширяемый протокол аутентификации и согласования ключей пользователей UMTS |  |
| EARFCN | Evolved Absolute Radio Frequency Channel Number | усовершенствованный абсолютный радиочастотный номер канала |  |
| EBCF | Event Based Charging Function | функция тарификации на основе событий |  |
| EBI | EPS Bearer Identifier | идентификатор службы передачи данных EPS |  |
| ECB | Electronic Code Book | электронная кодовая книга | один из режимов шифрования |
| ECC | Emergency Call Code | код экстренного вызова |  |
| ECD | Echo Control Device | эхозаградитель |  |
| ECF | Event Charging Function | функция учета расходов на основе событий |  |
| ECGI | E-UTRAN Cell Global Identifier | глобальный идентификатор соты в сети E-UTRAN |  |
| ECI | E-UTRAN Cell Identifier | идентификатор соты в сети E-UTRAN |  |
| ECI | Embedded Common Interface | встроенный CI-модуль | интерфейс подключения модулей для взаимодействия с закодированными каналами |
| ECM | EPS Connection Management | управление соединением в EPS |  |
| ECM | Error Correction Mode | режим коррекции ошибок |  |
| ECT | Explicit Call Transfer | явная переадресация вызовов |  |
| ECUR | Event Charging with Unit Reservation | тарификация на основе событий с резервированием |  |
| EDE | Encrypt-Decrypt-Encrypt | шифрование-дешифрование-шифрование | один из вариантов применения криптографического алгоритма DES |
| EDE3 | Encrypt-Decrypt-Encrypt | шифрование-дешифрование-шифрование | один из вариантов применения криптографического алгоритма Triple-DES |
| EDGE | Enhanced Data Rates for GSM Evolution | усовершенствование передачи данных для эволюции GSM |  |
| EDR | Event Detail Record | подробная запись о случившемся событии |  |
| eDRX | Extended Discontinuous Reception | расширенный режим прерывистого приема |  |
| EEA | EPS Encrypting Algorithm | алгоритм шифрования EPS |  |
| EEL | Electric Echo Loss | затухание эхо-сигнала |  |
| EF | Elementary File | элементарный файл |  |
| EFDIR | Elementary File Directory | элементарная файловая директория |  |
| EFR | Enhanced Full Rate | система улучшенного скоростного кодирования речи |  |
| EHPLMN | Equivalent HPLMN | эквивалентная домашней PLMN |  |
| EI | Error Indication | индикация ошибок |  |
| EIA | EPS Integrity Algorithm | алгоритм проверки целостности EPS |  |
| EIR | Equipment Identity Register | регистр идентификаторов оборудования |  |
| eMLPP | Enhanced Multi-Level Precedence and Pre-Emption Service | улучшенная услуга многоуровневого приоритета и приоритетного прерывания обслуживания |  |
| EMM | EPC Mobility Management | управление мобильностью в сетях EPC |  |
| EMS | Enhanced Message Service | расширенный сервис сообщений |  |
| eMSS | Enhanced MSC Server | улучшенный сервер MSC |  |
| ENIP | Enhanced Network Intelligent Platform | усовершенствованная сетевая интеллектуальная платформа |  |
| eNodeB | Evolved Node B | базовая станция сети LTE |  |
| ENUM | E.164 Number Mapping | протоколы для преобразования номеров E.164 к системе Интернет-адресации |  |
| EOP | End Of Procedure | окончание процедуры |  |
| EPC | Evolved Packet Core | улучшенное пакетное ядро |  |
| ePDG | Evolved Packet Data Gateway |  | оборудование с функцией доступа к оборудованию коммутации LTE из интернета при использовании доступа UTWAN |
| EPS | Evolved Packet System | пакетная система нового поколения |  |
| ERMES | European Radio Messaging System | европейская система передачи сообщений на радиочастотах | один из стандартов, ныне не действующий |
| ES | Encoding Scheme | схема кодирования |  |
| ESM | EPS Session Management | управление сессиями в сетях EPS |  |
| ESM | EPS Session Management | управления сессией EPS |  |
| ESME | External Short Messaging Entity | внешнее приложение для обмена короткими сообщениями |  |
| ESP | Encapsulating Security Payload | защищенная инкапсуляция |  |
| EST | Enabled Services Table | таблица активированных услуг |  |
| ETE | End-To-End | сквозной |  |
| ETFTN | Extended TFT Support Network | сеть поддержки расширенной TFT |  |
| ETFTU | Extended TFT Support UE | абонентское оборудование поддержки расширенной TFT |  |
| ETS | Emergency Telecommunications Services | Служба электросвязи в чрезвычайных ситуациях |  |
| ETSI | European Telecommunications Standards Institute | Европейский институт телекоммуникационных стандартов |  |
| F-TEID | Fully Qualified Tunnel Endpoint Identifier | полностью уточненный идентификатор конечной точки туннеля |  |
| FACoA | Foreign Agent Care-Of Address | адрес агента визитной сети |  |
| FAR | Forwarding Action Rule | правило передачи | определяет дальнейшее действие для принятых пакетов |
| FBC | Flow Based Charging | потоковая тарификация |  |
| FCS | Failure Cause | причина неуспешного выполнения |  |
| FCS | Frame Check Sequence | последовательность проверки кадров |  |
| FDD | Frequency Division Duplex | режим частотного дуплексного разноса |  |
| FDN | Fixed Dialling Number | фиксированный номер набора |  |
| FEC | Forward Equivalence Class | класс эквивалентности пересылки |  |
| FEC | Forward Error Correction | упреждающая коррекция ошибок | техника помехоустойчивого кодирования/декодирования |
| FER | Frame Error Rate | частота появления ошибочных кадров |  |
| FIB | Forward Indicator Bit | указательный бит вперед |  |
| FIBR | Forward Indicator Bit Routing | маршрутизация указательного бита вперед |  |
| FIFO | First In, First Out | куча по принципу "первым пришел — первым ушел" |  |
| FIM | Feature Interaction Manager | узел реализации функциональных возможностей |  |
| FISU | Fill In Signaling Unit | кадр однонаправленной передачи сигнальных сообщений от получателя о наличии ошибок |  |
| FMC | Fixed Mobile Convergence | конвергенция фиксированной и мобильной связи |  |
| FNUR | Fixed Network User Rate | пользовательская скорость сети фиксированной связи |  |
| FO | forwardingOptions | дополнительные опции при передаче дополнительной услуги |  |
| FPGA | Field Programmable Gate Array | программируемая логическая интегральная схема |  |
| FPH | FreePhone | вызов за счет вызываемого абонента |  |
| FPLMN | Forbidden PLMN | запрещенная PLMN |  |
| FQDN | Fully Qualified Domain Name | полностью уточнённое доменное имя |  |
| FQI | Frame Quality Index | показатель качества кадра |  |
| FQPC | Fully Qualified Partial CDR | полностью уточнённая частичная запись CDR |  |
| FSN | Forward Sequence Number | номер последовательности вперед |  |
| FTAM | File Transfer Access and Management | доступ и управление передачей файлов |  |
| FTN | forwardedToNumber | номер переадресации вызова |  |
| FTP | File Transfer Protocol | протокол передачи файлов |  |
| FTS | Fixed Terminal System | система связи со стационарным терминалом |  |
| FTS | forwardedToSubaddress | субадрес переадресации вызова |  |
| FUA | Fixed Uplink Allocation | фиксированное выделение восходящего канала |  |
| FXO | Foreign Exchange Office | аналоговый интерфейс для подключения к интерфейсу FXS |  |
| FXS | Foreign Exchange Station | голосовой интерфейс для подключения обычного аналогового телефона к мультиплексору и для подключения к АТС в сети оператора другой мини-АТС |  |
| GAD | Geographical Area Description | описание географической области |  |
| GAN | Generative Adversarial Network | генеративно-состязательная сеть |  |
| GAN | Generic Access Network |  | протокол передачи голоса, данных и мультимедиа по IP-сетям |
| GARP | Generic Attribute Registration Protocol | общий протокол регистрации атрибутов |  |
| GBA | Generic Bootstrapping Architecture | общая архитектура начальной загрузки |  |
| GBAS | Ground Based Augmentation System | наземная система контроля и коррекции |  |
| GbE | Gigabit Ethernet | канал Ethernet с гигабитовой пропускной способностью |  |
| GBR QCI | Guaranteed Bit Rate Quality Class Identifier | идентификатор класса качества обслуживания с гарантированной скоростью |  |
| GBR | Guaranteed Bit Rate | гарантированная битовая скорость |  |
| GBR | Guaranteed Bit Rate | гарантированная скорость передачи цифрового потока |  |
| GCI | Global Cable Identifier | глобальный идентификатор кабеля |  |
| GEA | GPRS Encryption Algorithm | алгоритм шифрования в сетях GPRS |  |
| GERAN | GSM/EDGE Radio Access Network | сеть радиодоступа к GSM/EDGE |  |
| GFBR | Guaranteed Flow Bit Rate | гарантированный потоковая скорость передачи информации |  |
| GGSN | Gateway GPRS Support Node | узел поддержки шлюза GPRS |  |
| GLI | Global Line Identifier | глобальный идентификатор линии |  |
| GMLC | Gateway Mobile Location Centre | шлюзовой центр позиционирования |  |
| GMSC | Gateway MSC | шлюз MSC |  |
| GNSS | Global Navigation Satellite System | глобальная навигационная спутниковая система |  |
| GPRS | General Packet Radio Service | система пакетной радиосвязи общего пользования |  |
| GRE | Generic Routing Encapsulation | протокол инкапсуляции сетевых пакетов | для использования инкапсулированных пакетов в других протоколах поверх IP |
| GSI | Geographical Scope Interface | интерфейс услуги геолокации |  |
| GSM | Global System for Mobile Communications | глобальный стандарт цифровой мобильной сотовой связи |  |
| GSMA | GSM Association | Ассоциация GSMA | организация операторов мобильной связи |
| gsmSCF | GSM Service Control Function | служба контроля сервисов GSM |  |
| gsmSRF | GSM Specialized Resource Function | служба специализированных ресурсов GSM |  |
| GSN | GPRS Supporting Node | узел поддержки GPRS |  |
| GSU | Granted-Service-Unit | сервисный блок предоставленных услуг | Diameter-CCA |
| GT | Global Title | глобальный заголовок |  |
| GTI | GT Indicator | индикатор GT |  |
| GTP | GPRS Tunnelling Protocol | протокол туннелирования GPRS | передача данных между узлами поддержки GPRS в пакетной сети |
| GTP-C | GTP Control Plane | уровень управления GTP |  |
| GUAMI | Globally Unique AMF Identifier | международный глобальный идентификатор модуля управления доступом и мобильностью AMF в сетях 5G |  |
| GUI | Graphical User Interface | графический пользовательский интерфейс |  |
| GUID | Globally Unique Identifier | глобально уникальный идентификатор |  |
| GUP | General User Profile | общий профиль пользователя |  |
| GUTI | Globally Unique Temporary UE Identity | глобальный временный идентификатор UE | PLMN + MMEI + M-TMSI |
| GUTS | Generic UDP Transport Service | общая услуга доставки сообщений с помощью UDP |  |
| GW | Gateway | шлюз |  |
| HAC | High-Availability Cluster | кластер высокой доступности |  |
| Handover |  | хэндовер | передача обслуживания абонента в процессе разговора из одной соты в другую без разрыва соединения |
| HARQ | Hybrid Automatic Repeat Request | комбинированный автоматический запрос повторения |  |
| HBM | Host Based Mobility | управление мобильностью на базе хостов |  |
| HDB3 | High Density Bipolar | высокоплотное биполярное кодирование |  |
| HDC | HTTP-to-Diameter Сonverter | преобразователь данных HTTP в формат протокола Diameter |  |
| HDD | Hard Disk Drive | накопитель на жестких дисках |  |
| HDLC | High-Level Data Link Control | высокоуровневый протокол управления каналом связи |  |
| HI | Handover Indication | индикация хэндовера |  |
| HLR | Home Location Register | регистр местоположения абонентов собственной сети |  |
| HMAC | Hash-Based Message Authentication Code | код аутентификации сообщений с помощью хеш-функции |  |
| HMDT | Signaling Message Handling: Message Distribution |  обработка сигнальных сообщений: распределение сообщений |  |
| HMRT | Signaling Message Handling: Message Routing | обработка сигнальных сообщений: маршрутизация сообщений |  |
| HOLD | Call Hold | удержание вызова |  |
| HPLMN | Home PLMN | домашняя сеть PLMN |  |
| HPPLMN | Higher Priority PLMN Search Period | период поиска наиболее приоритетной PLMN |  |
| HRR | Handover Resource Reservation | резервирование ресурсов хендовера |  |
| HS-DSCH | High Speed Downlink Shared Channel | общий высокоскоростной нисходящий канал |  |
| HSCSD | High Speed Circuit Switched Data | высокоскоростная передача данных по коммутируемым каналам |  |
| HSDPA | High Speed Downlink Packet Data Access | высокоскоростной доступ к пакетным данным по нисходящему каналу |  |
| HSL | High-Speed Link | высокоскоростное соединение |  |
| HSM | Hardware Security Module | модуль аппаратной защиты |  |
| HSPA | High-Speed Packet Data Access | высокоскоростная пакетная передача данных |  |
| HSS | Home Subscriber Server | база данных абонентов собственной сети LTE |  |
| HSUPA | High Speed Uplink Packet Data Access | высокоскоростной доступ к пакетным данным по восходящему каналу |  |
| HTS | Head/Tail Sync |  | полностью сериализованный режим синхронизации кольцевого буфера |
| HUP | Handover User Part | абонентская подсистема хэндовера |  |
| I-CSCF | Interrogating CSCF | узел взаимодействия с внешними сетями IMS |  |
| IACS | Immediate Active Codec Set | набор активных кодеков немедленного действия |  |
| IAM | Initial Address Message | начальное адресное сообщение |  |
| IANA | Internet Assigned Numbers Authority | организация по назначению Интернет-номеров |  |
| IBCF | Interconnection Border Control Function | узел управления пограничным взаимодействием сети IMS |  |
| ICC | Integrated Circuit Card | карта со встроенной микросхемой |  |
| ICCID | Integrated Circuit Card Identification | идентификатор смарт-карты |  |
| ICD | Interface Control Document | интерфейсный контрольный документ |  |
| ICE | In Case of Emergency | в случае экстренной ситуации |  |
| ICI | Incoming Call Information | информация о входящем вызове |  |
| ICM | Initial Codec Mode | первоначальный режим кодека |  |
| ICMP | Internet Control Message Protocol | протокол управляющих сообщений в сети Интернет |  |
| ICS | Incident Control System | система реагирования на инциденты |  |
| ICS | IP Multimedia Centralized Services | централизованные службы сети IMS |  |
| ICT | Incoming Call Timer | таймер входящего вызова |  |
| IEC | International Electrotechnical Commission | Международная электротехническая комиссия |  |
| IED | Information Element Data | данные информационного элемента |  |
| IEI | Information Element Identifier | идентификатор информационного элемента |  |
| IETF | International Engineering Task Force | Инженерный совет Интернета | открытое международное сообщество, занимающееся развитием сетевых протоколов и архитектуры Интернета |
| IFC | Initial Filter Criteria | первичные критерии фильтрации |  |
| IFOM | IP Flow Mobility | мобильность потока IP |  |
| IFS | International FreePhone Service | международный бесплатный вызов |  |
| IGP | Interior Gateway Protocol | протокол внутридоменной маршрутизации |  |
| IHOSS | Internet Hosted Octet Stream Service | интернет-служба потока октетов |  |
| IIOP | Internet Inter-ORB Protocol | протокол взаимодействия ORB в интернете |  |
| IKEv2 | Internet Key Exchange v2 | протокол обмена ключами в сети Интернет версии 2 |  |
| IL | Incoming Line | входящее соединение |  |
| iLo | Integrated Lights Out | механизм управления серверами в условиях отсутствия физического доступа к ним |  |
| IM-SSF | IP Multimedia Service Switching Function | мультимедийная функция коммутации услуг на базе протокола IP |  |
| IMEI | International Mobile Equipment Identifier | международный идентификатор оборудования для мобильной связи |  |
| IMEISV | International Mobile Equipment Identifier and Software Version | международный идентификатор оборудования для мобильной связи и версия программного обеспечения |  |
| IMPI | IMS Private User Identity | приватный идентификатор пользователя в IMS-сети |  |
| IMPU | IMS Public User Identity | публичный идентификатор пользователя в IMS-сети |  |
| IMRN | IP Multimedia Routing Number | номер маршрутизации в сети IMS |  |
| IMS | IP Multimedia Subsystem | мультимедийная подсистема на базе протокола IP |  |
| IMSI | International Mobile Subscriber Identifier | международный идентификатор абонента мобильной связи |  |
| IN | Intellectual Networks | интеллектуальные сети |  |
| INAP | Intelligent Network Application Protocol | прикладной протокол интеллектуальной сети |  |
| INAP-R | INAP for Russian Telecommunication Network |  | российская версия протокола INAP |
| INF | Information | сообщение о передачи информации в ISUP |  |
| InitialDP | Initial Detection Point | начальное место обнаружения |  |
| INN | Internal Network Number | внутренний сетевой номер |  |
| INR | Information Request | запрос информации в ISUP |  |
| InRoute | Incoming Route | входящий маршрут |  |
| INS | Instruction | инструкция |  |
| IO | Input/Output | ввод/вывод |  |
| IoT | Internet of Things | интернет вещей |  |
| IP | Intelligent Peripheral | интеллектуальная периферия |  |
| IP | Internet Protocol | межсетевой протокол |  |
| IPCP | Internet Protocol Control Protocol | протокол управления протоколом IP | протокол согласования параметров IP-соединения поверх PPP-соединения |
| IPDL | Idle Period Downlink | период простоя нисходящей линии |  |
| IPE | In Path Equipment | оборудование маршрута передачи |  |
| IPMI | Intelligent Platform Management Interface | интеллектуальный интерфейс управления платформой |  |
| IPSec | Internet Protocol Secure | протокол IP с поддержкой функций безопасности |  |
| IPSP | IP Server Process | сущность процесса приложения, работающего по IP |  |
| IRQ | Interrupt Request | запрос на прерывание процесса |  |
| ISC | Intersystem Communication | межсетевое взаимодействие |  |
| ISD | Insert Subscriber Data | вставить данные абонента | обновления данных абонента |
| ISDN | Integrated Services Digital Network | цифровая сеть с интеграцией услуг |  |
| ISG | IP Source Guard | защита IP-адреса от подделки |  |
| ISO | International Organization for Standardization | Международная организация по стандартизации |  |
| ISRAI | Idle-Mode Signalling Reduction Activation Indication | индикация включения режима уменьшения передачи сигнала в ждущем режиме |  |
| ISRSI | Idle-Mode Signalling Reduction Supported Indication | индикация поддержки уменьшения передачи сигнала в ждущем режиме |  |
| ISS | Intercom Station Selection | система выбора станции внутренней связи |  |
| IST | Immediate Service Termination | немедленное прекращение обслуживания |  |
| ISUP | ISDN User Part | пользовательская часть ISDN |  |
| ISUP-R | ISUP for Russian Telecommunication Network | подсистема ISUP для национальной сети связи России |  |
| ITC | Integrated Terminal Controller | интегрированный терминальный контроллер |  |
| ITG | Internet Telephony Gateway | шлюз интернет-телефонии |  |
| ITU | International Telecommunications Union | Международный союз электросвязи |  |
| IUA | ISDN User Adaptation | протокол пользовательской адаптации ISDN |  |
| IVR | Interactive Voice Response | интерактивное голосовое меню |  |
| IWMSC | Interworking MSC | межсетевой MSC |  |
| JAXB | Java Architecture for XML Binding |  | инструмент для конвертации Java-объектов в XML и обратно |
| JDBC | Java Database Connectivity | соединение с базами данных на Java |  |
| JMX | Java Management Extensions | управленческие расширения Java для контроля и управления приложениями | , объектами, устройствами и сетями |
| JSF | JavaServer Faces |  | Java-спецификация для построения компонентно-ориентированных пользовательских интерфейсов Web-приложений |
| JSON | Java Script Object Notation |  | текстовый формат обмена на базе JavaScript |
| JVM | Java Virtual Machine | виртуальная машина Java |  |
| JWT | JSON Web Token | токен для авторизации в Web-интерфейсе формата JSON |  |
| KASME | Key Access Security Management Entity | модуль управления ключами для безопасности доступа |  |
| KDF | Key Derivation Function | функция формирования ключа |  |
| Ki | Key for Identification | ключ идентификации абонента |  |
| KIc | Key and Algorithm Identifier for Ciphering | идентификатор ключа и алгоритма шифрования |  |
| KID | Key and Algorithm Identifier for RC/CC/DS | идентификатор ключа и алгоритма для проверки избыточности, криптографической хэш-суммы и цифровой подписи |  |
| KIK | Key Identifier for protecting KIc and KID | идентификатор ключа для защиты ключей KIc и KID |  |
| KPI | Key Performance Indicators | ключевые показатели эффективности |  |
| KSI | Key Set Identifier | идентификатор набора ключей |  |
| KVM | Kernel-based Virtual Machine |  | программное обеспечение для виртуализации в среде Linux, состоящее из модуля ядра и компонентов пользовательского режима |
| L2TP | Layer 2 Tunnelling Protocol | протокол туннелирования для уровня 2 |  |
| LA | Level Access | уровень доступа |  |
| LAC | L2TP Access Concentrator | концентратор доступа L2TP |  |
| LAC | Local Area Code | код локальной зоны |  |
| LACS | Local Active Codec Set | набор локальных активных кодеков |  |
| LAI | Location Area Identification | идентификация зоны местоположения |  |
| LAI | Location Area Information | информация о зоне местоположения |  |
| LAPD | Link Access Procedure on the D-Channel | процедура доступа к звену/линии для канала D |  |
| LAU | Location-Area-Update | процедура обновления геоданных |  |
| LBC | Load Balancing Cluster | кластер распределенной нагрузки |  |
| LBS | Location Based Services | служба на основе определения местоположения абонента |  |
| LCAF | Location Client Authorization Function | функция авторизации местоположения клиента |  |
| LCCF | Location Client Control Function | функция управления местоположением клиента |  |
| LCCTF | Location Client Coordinate Transformation Function | функция преобразования координат позиции пользователя |  |
| LCF | Location Client Function | функция определения местоположения клиента |  |
| LCS | Location Service | служба определения местоположения |  |
| LCZTF | Location Client Zone Transformation Function | функция преобразования временной зоны местоположения клиента |  |
| LDAP | Lightweight Directory Access Protocol | упрощённый протокол доступа к каталогам |  |
| LDR | Location Deferred Request | отложенный запрос местоположения |  |
| LEE | Laptop Embedded Equipment | оборудование со встроенным ноутбуком |  |
| LFA | Loss of Frame Alignment | потеря цикловой (фазовой) синхронизации |  |
| LFM | Multi Load Filtering | голосование со множественным выбором |  |
| LFN | longForwardedToNumber | адрес переадресации вызова |  |
| LFS | Single Load Filtering | голосование с единоразовым выбором |  |
| LI | Legal Intervention | законное вмешательство в вызов |  |
| LI | Length Indicator | индикатор длины |  |
| LI | Log Intelligence | обработка лог-файлов |  |
| LIDB | Line Information Database | информационная база данных линии связи |  |
| LIFO | Last in, First out | стек по принципу «последним пришел — первым ушел» |  |
| LIPA | Local IP Access | прямой доступ к домашней локальной IP-сети |  |
| LLC | Logical Link Control | протокол управления логическими связями |  |
| LLC | Logical Link Control | управление логическими связями |  |
| LMA | Local Mobility Anchor | локальный узел управления мобильностью |  |
| LME | Laptop Mounted Equipment | оборудование с вмонтированным ноутбуком |  |
| LMF | Location Management Function | функция управления местоположением |  |
| LMMF | LMU Mobility Management Function | функция управления мобильностью LMU |  |
| LMSI | Local Mobile Subscriber Identity | местный идентификатор абонента мобильной связи |  |
| LMU | Line Measurement Unit | стационарный измерительный модуль |  |
| LMU | Location Measurement Unit | единицы измерения местоположения |  |
| LND | Last Number Dialled | последний набранный номер |  |
| LNR | Last Number Redial | услуга повторного набора номера абонента из последнего вызова |  |
| LOF | Loss of Frame | сигнал потери цикловой синхронизации |  |
| Loop_ST | Loop Signaling Termination | зацикленная отправка сигнальной информации |  |
| LOS | Loss of Signal | сигнал потери линейного сигнала E1 |  |
| LP | Loop Prevention | предотвращение создания бесконечных зацикливаний |  |
| LPB | Local Phone Book | локальная книга контактов |  |
| LRF | Location Retrieval Function | функция поиска местоположения |  |
| LSA | Localized Service Area | локализованная зона обслуживания |  |
| LSAF | Location Subscriber Authorization Function | функция авторизации позиции абонента |  |
| LSB | Least Significant Bit | младший бит |  |
| LSBcF | Location System Broadcast Function | функция широковещания система геопозиционирования |  |
| LSBF | Location System Billing Function | функция биллинга для системы позиционирования |  |
| LSCF | Location System Control Function | функция управления системой позиционирования |  |
| LSCS | Local Supported Codec Set | набор локально поддерживаемых кодеков |  |
| LSCTF | Location System Coordinate Transformation Function | функция преобразования координат системы геопозиционирования |  |
| LSOF | Location Subscriber Operations Function | функция операций геопозиции абонента |  |
| LSPF | Location Subscriber Privacy Function | функция приватности геопозиции абонента |  |
| LSSU | Link Status Signal Unit | кадр передачи статусов сигнальных сообщений |  |
| LSTF | Location Subscriber Translation Function | функция передачи местоположения абонента |  |
| LTE | Long-Term Evolution | стандарт беспроводной высокоскоростной передачи связи для мобильных сетей |  |
| LTO | Line Signal Timeout | истечение допустимой длительности линейного сигнала |  |
| LVDS | Low Voltage Differential Signaling | дифференциальная сигнализация низкого напряжения |  |
| M2PA | MTP-2 User Peer-to-Peer Adaptation Part | протокол адаптации пользовательского Peer-to-Peer уровня MTP-2 стандарта SIGTRAN |  |
| M3UA | MTP-3 User Adaptation Part | протокол адаптации пользовательского уровня MTP-3 стандарта SIGTRAN |  |
| MAC | Media Access Control | уникальный идентификатор оборудования или сетевого интерфейса в сети Ethernet |  |
| MAC | Message Authentication Code | код проверки подлинности сообщения |  |
| MACF | Multiple Association Control Function | функция управления множественными ассоциациями |  |
| MACS | Maximum Number of Codecs Modes in the Active Codec Set |  |  |
| MAG | Mobile Access Gateway | шлюз мобильного доступа |  |
| MAH | Mobile Access Hunting | доступ мобильных пользователей |  |
| MAP | Mobile Application Part | протокол мобильных приложений |  |
| MB | Music Box | автоматическое воспроизведение мелодий |  |
| MBMS | Multimedia Broadcast Multicast Service | услуга трансляции медиаконтента в режиме многоадресной передачи |  |
| MBR | Maximum Bit Rate | максимальная битовая скорость |  |
| MBSFN | Multimedia Broadcast Multicast Service Single Frequency network | одночастотная сеть многоадресной передачи мультимедийных широковещательных услуг |  |
| MC | Multicall | многопользовательский |  |
| MCA | Missed Call Alert | оповещение о пропущенном вызове |  |
| MCA | Mobile Communication on Aircraft | мобильные средства коммуникации на воздушном судне |  |
| MCC | Mobile Country Code | код страны в мобильных сетях |  |
| MCE | Missed Call Emulation | эмуляция пропущенного вызова |  |
| MCE | Multicell/Multicast Coordination Entity | узел координации радиосигналов между сотами |  |
| MCI | Malicious Call Identification | идентификация злонамеренных вызовов |  |
| MCM | Multi-Connection Mode | режим нескольких соединений |  |
| MCN | Missed Call Notification | оповещение о пропущенных вызовах |  |
| MCU | Media Control Unit | сервер обработки медиаданных |  |
| MCU | Multipoint Control Unit | сервер многоточечной конференции |  |
| MD5 | Message Digest 5 |  | алгоритм хеширования |
| MDT | Minimization Drive Test | минимизация выездного тестирования |  |
| MDT | Multicast Distribution Tree | дерево многоадресного распределения |  |
| ME | Mobile Equipment | мобильное оборудование |  |
| MEI | Mobile Equipment Identity | идентификатор мобильного устройства |  |
| MEID | Mobile Equipment Identifier | идентификатор мобильного оборудования сети CDMA |  |
| MExE | Mobile Execution Environment | мобильная среда исполнения |  |
| MF | Master File | файл с основными данными |  |
| MFC | Multi-Frequency Compelled | многочастотный вынужденный |  |
| MFP | Multi Frequency Packet | блок типа "импульсный пакет №2" |  |
| MFS | Multi Frequency Shuttle | блок типа "импульсный челнок" |  |
| MGC | Media Gateway Control | система управления медиашлюзами |  |
| MGCF | Media Gateway Control Function | узел управления медиашлюзами |  |
| MGCP | Media Gateway Control Protocol | протокол управления медиашлюзами |  |
| MGV-F | MTK Generation and Validation Function | функция создания и подтверждения ключей MTK |  |
| MGW | Media Gateway | шлюз передачи медиаданных |  |
| MIB | Management Information Base | база управляющей информации | виртуальная база данных, используемая для управления объектами в сетях связи |
| MID | Message Identifier | идентификатор сообщения |  |
| MiD | Multi-Identity | многопользовательский |  |
| MIKEY | Multimedia Internet Keying | система создания ключей для сетевых мультимедиа |  |
| MIME | Multipurpose Internet Mail Extensions |  | стандарт передачи различных типов данных по электронной почте |
| MIPv4 | Mobile IPv4 | расширение функциональности протокола IPv4 по обеспечению мобильности |  |
| MLC | Mobile Location Centre | центр позиционирования |  |
| MLD | Multicast Listener Discovery | розыск групповых слушателей |  |
| MME | Mobility Management Entity | узел управления мобильностью |  |
| MMEI | MME Identifier | идентификатор MME |  |
| MMS | More Messages to Send | флаг наличия сообщений, готовых к отправке |  |
| MMS | Multimedia Message Service | служба мультимедийных сообщений |  |
| MMSC | Multimedia Message Service Center | центр обработки мультимедийных сообщений |  |
| MMSI | Multimedia Message Service Interface | MMS-интерфейс |  |
| MMSS | MultiMode System Selection | многорежимная система выбора |  |
| MMTel | IMS Multimedia Telephony | мультимедийная телефония в сетях IMS |  |
| MN | Message Number | номер обрабатываемого сообщения |  |
| MNC | Mobile Network Code | код сети мобильной связи |  |
| MNP | Mobile Number Portability | переносимость номеров мобильной сети |  |
| MNRR | Mobile Station Not Reachable Reason | причина недоступности абонента |  |
| MO | Mobile Originated | исходящий |  |
| MO-SMS | Mobile Originated Short Message | исходящее SMS-сообщение |  |
| MOCN | Multi-Operator Core Network | межоператорская базовая сеть |  |
| MOSPF | Multicast Open Shortest Path First | OSPF при групповой многоадресной рассылке |  |
| MPC | Multiple Party Conference | многопользовательская конференция |  |
| MPLS | MultiProtocol Label Switching | многопротокольная коммутация по меткам |  |
| MPS | Multimedia Priority Services |  | служба для повышения приоритета сетей связи экстренных служб |
| MPS | MultiPage Signal | многостраничный сигнал |  |
| MPTY | Multi-Party service | многосторонняя услуга |  |
| MR | Message Reference | идентификатор соответствующего сообщения SMS-SUBMIT |  |
| MRFC | Media Resource Function Controller | контроллер мультимедийных ресурсов |  |
| MRFP | Media Resource Function Processor | процессор мультимедийных ресурсов |  |
| MS | Mobile Subscriber | абонент мобильной сети |  |
| MSAN | Multi-Service Access Node | узел многосервисного доступа |  |
| MSB | Most Significant Bit | старший бит |  |
| MSC | Mobile Switching Center | коммутационный центр мобильной связи |  |
| MSISDN | Mobile Subscriber Integrated Services Digital Number | номер абонента мобильной связи для цифровой сети с интеграцией услуг |  |
| MSK | MBMS Service Key | ключ услуги MBM |  |
| MSL | Minimum Security Level | минимальный уровень безопасности для отправки защищенных пакетов |  |
| MSLD | Minimum Security Level Data | данные минимального уровня безопасности для отправки защищенных пакетов |  |
| MSML | Media Sessions Markup Language | язык разметки сессии канала передачи |  |
| MSRN | Mobile Station Roaming Number | роуминговый номер мобильной станции |  |
| MSS | Maximum Segment Size | максимальный размер полезного блока данных TCP |  |
| MSU | Meaning Signaling Unit | значащая сигнальная единица |  |
| MSU | Message Signal Unit | сигнальная единица сообщения |  |
| MSV | MS Validated | подтверждение аутентификации абонента на новом узле SGSN |  |
| MT | Mobile Terminated | входящий |  |
| MT-SMS | Mobile Terminated Short Message | входящее SMS-сообщение |  |
| MTA | Multilateration Timing Advance | множественное позиционирование на основе времени задержки ответного сигнала |  |
| MTI | Message Type Indicator | индикатор типа сообщения |  |
| MTK | MBMS Traffic Key | ключ обмена MBMS |  |
| MTP | Message Transfer Part | подсистема передачи сообщений |  |
| MTRF | Mobile Terminating Roaming Forwarding | переадресуемый исходящий вызов в роуминговой сети |  |
| MTU | Maximum Transmission Unit | максимальный размер пакета |  |
| MTY | Multi Party | многосторонний |  |
| MuD | Multi-Device | многоточечный |  |
| MUK | MBMS User Key | ключ пользователя MBMS |  |
| MUP | Mobile User Part | абонентская подсистема мобильной связи |  |
| MVNO | Mobile Virtual Network Operator | виртуальный оператор мобильной связи |  |
| MWD | Messages-Waiting-Data | информация о сообщениях в режиме ожидания | список центров, у которых есть сообщения, ожидающие доставки абоненту |
| N3IWF | Non-3GPP InterWorking Function | функция взаимодействия с не-3GPP сетью доступа |  |
| NA | Network Appearance | локальная ссылка на сигнальный шлюз и сервер приложений |  |
| NACC | Network Assisted Cell Change | сетевая смена соты |  |
| NAEA | North American Equal Access |  | закон о предоставлении равных возможностей всем гражданам |
| NAI | Nature of Address Indicator | индикатор типа адреса |  |
| NAI | Network Access Identifier | идентификатор сети доступа |  |
| NAPTR | Name Authority Pointer |  | один из видов записи ресурса на DNS |
| NAS | Network Access Server | сервер сетевого доступа |  |
| NAS | Network Attached Storage | сетевое хранилище данных |  |
| NAS | Non-Access Stratum | слой без доступа | содержит информацию, передаваемую между базовой сетью оператора и пользовательским устройством |
| NAT | Network Address Translation | преобразование сетевых адресов |  |
| NB-IoT | Narrow Band Internet of Things |  | стандарт сотовой связи для стационарных устройств с низкими объемами передаваемых данных и малым потреблением |
| NBM | Network Based Mobility | управление мобильностью на базе сети |  |
| NBNS | NetBIOS Name Service |  | сервис для предоставления услуг сеансового уровня OSI, позволяя приложениям на отдельных машинах взаимодействовать через локальную сеть |
| NBR | Number of Parallel Bearers | количество параллельных медиаканалов передачи |  |
| NCC | Network Colour Code | код раскраски сети |  |
| NCGI | New Radio Cell Global Identity | глобальный идентификатор новой радиосоты |  |
| NCS | Network Communication Server | сервер управления сетью связи |  |
| NDB | Non-Directional Beacon | приводной радиомаяк |  |
| NDP | Neighbour Discovery Protocol | протокол обнаружения соседей |  |
| NEF | Network Exposure Function | функция обеспечения взаимодействия с внешними приложениями |  |
| NFV | Network Function Virtualization | виртуализация сетевых функций |  |
| NGAF | Non-GPRS Alert Flag | флаг оповещения MSC/VLR об активности абонента |  |
| NGN | Next Generation Networks | сети нового поколения | мультисервисные сети связи |
| NGOSS | New Generation Operation Systems and Software | новое поколение систем технико-эксплуатационной поддержки и программных средств |  |
| NI USSI | Network Initiated Unstructured Supplementary Service Data over IMS | инициированный сетью USSD по сетям IMS |  |
| NI | Network Identifier | идентификатор сети |  |
| NI-LR | Network Induced Location Request | запрос местоположения от сети |  |
| NIA | Network Indication of Alerting | сетевая индикация аварий |  |
| NIC | Network Interface Controller | сетевой контроллер |  |
| NIDD | Non-IP Data Delivery | передача данных не по протоколу IP |  |
| NMC | Network Management Controls | функции управления сетью |  |
| NMO | Network Mode of Operation | сетевой режим работы |  |
| NMS | Network Management System | система управления сетью |  |
| NP | Numbering Plan | план нумерации |  |
| NPCI | Network Protocol Control Information | управляющая информация сетевого протокола |  |
| NPI | Numbering Plan Indicator | индикатор плана нумерации |  |
| NPLI | Network Provided Location Information | информация о геолокации, предоставленная сетью |  |
| NPN | Network Protocol Negotiation | согласование сетевого протокола |  |
| NRCT | noReplyConditionTime | таймер ожидания ответа при переадресации в случае отсутствия вызова |  |
| NRF | NF Repository Function | хранилище сетевых функций |  |
| NRSN | Network Request Support Network | сеть поддержки сетевых запросов |  |
| NRSU | Network Request Support UE | пользовательское оборудование, поддерживающее сетевые запросы |  |
| NSAPI | Network Service Access Point Identifier | идентификатор точки доступа к сетевых службам |  |
| NSC | Non-Standard Facilities Command | сообщение о посылке NSF в протоколе T.30 |  |
| NSDU | Network Service Data Unit | блок данных сетевой услуги |  |
| NSE | Named Signaling Event | ключ для обеспечения взаимодействия между протоколами факсимильной передачи |  |
| NSF | Non-Standard Facilities | нестандартное сообщение в протоколе T.30 |  |
| NSLIP | Negative Slip | отрицательное проскальзывание | пропуск фрейма |
| NSS | Non-Standard Facilities Setup | настройка NSF в протоколе T.30 |  |
| NSSF | Network Slice Selection Function | функция выбора сетевого слоя |  |
| NSWO | Non-Seamless WLAN Offload |  | технология разгрузки WiFi сети с прерыванием трафика в случае хэндовера |
| NTE | Named Telephony Event | именное событие телефонии |  |
| NTP | Network Time Protocol | сетевой протокол синхронизации времени |  |
| NWDAF | Network Data Analytics Function | функция анализа сетевых данных |  |
| OA | Originating Address | адрес отправления |  |
| OACS | Optimized Active Codec Set | оптимизированный набор активных кодеков |  |
| OAM | Operation, Administration, and Maintenance | эксплуатация, администрирование и техническое обслуживание |  |
| OBD | Outbound Dialler | абонент, оформивший подписку во время массового оповещения пользователей |  |
| OCI | Outgoing Call Information | информация об исходящем вызове |  |
| OCS | Online Charging System | система учета расходов в реальном времени |  |
| OCT | Outgoing Call Timer | таймер исходящего вызова |  |
| ODB | Operator Determined Barring | запрет, установленный оператором связи |  |
| ODBC | Open Database Connectivity | программный интерфейс подключения к базам данных |  |
| OFCS | Offline Charging System | система учета расходов в режиме offline |  |
| OFDMA | Orthogonal Frequency Division Multiple Access | множественный доступ с ортогональным частотным разделением |  |
| OgPN | Original Party Number | первоначальный номер абонента |  |
| OI | Operation Indication | индикация выполнения действия |  |
| OI | Operator Identifier | идентификатор оператора |  |
| OM | Optimization Mode Supported | поддержка режима оптимизации |  |
| OMA | Open Mobile Alliance | Открытое сообщество операторов мобильной связи |  |
| OMAP | Operation, Maintenance, and Administration Part | подсистема эксплуатации, администрирования и технического обслуживания |  |
| OMC | Operation and Maintenance Centre | центр эксплуатации и технического обслуживания |  |
| OMI | Open Message Interface | интерфейс открытых сообщений |  |
| OMI | Operation and Maintenance Interface | интерфейс эксплуатации и технического обслуживания |  |
| OMS | Outlook Mobile Service |  | сервис для интеграции почтовых служб ПК с сетями мобильных операторов |
| OP | Open Platform | платформа с открытым исходным кодом |  |
| OPC | Origination Point Code | код сигнальной точки отправления |  |
| OPL | Operator PLMN List | список операторских PLMN |  |
| OPLMNwACT | Operator Controlled PLMN Selector with Access Technology | элементарный файл для селектора сети, управляемого оператором, с технологией доступа |  |
| OQoD | Originating Call Query on Digit Analysis | анализ при запросе по цифрам инициируемого вызова |  |
| OR | Optimal Routing | оптимальная маршрутизация |  |
| ORB | Object Request Broker | брокер объектных запросов |  |
| OSA | Open Service Architecture | архитектура открытых сервисов |  |
| OSI | Open System Interconnection | модель протоколов для взаимодействия открытых систем |  |
| OSPF | Open Shortest Path First |  | протокол динамической маршрутизации, отслеживающий наиболее короткий путь |
| OTA | Over-the-Air | беспроводной способ доставки сообщений |  |
| OTDOA | Observed Time Difference Of Arrival | наблюдаемая разница времени получения сигнала |  |
| OTID | Original Transaction Identifier | идентификатор транзакции TCAP на исходящей стороне |  |
| OW | One-Wire Server | однопоточный сервер |  |
| P-CSCF | Proxy CSCF | узел взаимодействия с абонентскими терминалами IMS |  |
| P-LIG | Packet Lawful Interception Gateway | специальный пакетный шлюз законного перехвата |  |
| P-TMSI | Packet Temporary Mobile Subscriber Identity | временный идентификатор абонента пакетной связи |  |
| P2P SMS | Person-to-Person SMS | сообщение, отправленное абоненту от другого абонента |  |
| PA | PlayAnnouncement | проигрыш оповещения |  |
| PAA | PDN Address Allocation | индикатор типа распределения адресов шлюзов PDN |  |
| PAP | Password Authentication Protocol | протокол аутентификации по паролю |  |
| PAPR | Peak-to-Average Power Ratio | отношение пикового уровня мощности сигнала к среднему |  |
| PAR | Precision Approach Radar | радиолокатор точного захода на посадку |  |
| PBID | Phonebook Identifier | идентификатор списка контактов |  |
| PBN | Performance-Based Navigation | навигация на основе эксплуатационных характеристик |  |
| PBX | Private Branch Exchange | учрежденческая телефонная станция |  |
| PC | Protocol Class | класс протокола |  |
| PCAP | Packet Capture | захват данных | программное обеспечение для обработки сетевых данных, поступающих на сетевую карту оборудования |
| PCB | Printed Circuit Board | печатная плата |  |
| PCB | Protocol Control Byte | байт проверки протокола |  |
| PCF | Policy Control Function | функция управления политиками |  |
| PCF | Position Calculation Function | функция вычисления местоположения |  |
| PCI | Peripheral Component Interconnect | взаимодействие периферийных компонентов | шина для подключения устройств к микропроцессору |
| PCI | Peripheral Component Interface | интерфейс взаимодействия периферийных компонентов |  |
| PCI | Preemption Capability Indicator | индикатор преимущественного занятия линии |  |
| PCNTR | Padding Counter | счетчик паддинга |  |
| PCO | Protocol Configuration Options | опции конфигурации протокола |  |
| PCP | Protei Case Processor | обработчик методов разработки «ПРОТЕЙ» |  |
| PCRF | Policy and Charging Rules Function | правила и политики тарификации | узел LTE, управляющий начислением платы за оказанные услуги |
| PCSM | Peer Connection Service Manager | диспетчер соединений протокола Diameter |  |
| PCU | Packet Control Unit | элемент управления пакетами |  |
| PDA | Personal Digital Assistant | персональный цифровой помощник |  |
| PDCH | Packet Data Channel | канал пакетных данных |  |
| PDCP | Packet Data Convergence Protocol | протокол конвергенции пакетных данных |  |
| PDN | Packet Data Network | сеть пакетной передачи данных |  |
| PDP | Packet Data Protocol | протокол пакетной передачи данных |  |
| PDU | Protocol Data Unit | единица данных протокола |  |
| PEI | Permanent Equipment Identifier | постоянный идентификатор оборудования пользователя в сетях 5G |  |
| PER | Packed Encoding Rules | сжатые правила кодирования |  |
| PERL | Practical Extraction and Report Language |  | язык программирования |
| PESQ | Perceptual Evaluation of Speech Quality | оценка восприятия качества передачи речи |  |
| PFD | Packet Flow Description | описание потока пакетов |  |
| PGW | Packet Data Network Gateway | шлюз пакетной передачи данных |  |
| PI | Presentation Indicator | индикатор отображения (адреса) |  |
| PIC | Performance Intelligence Center | центр изучения производительности | узел сети, обрабатывающий информацию о трафике для повышения эффективности |
| PIC | Point In Call |  | текущее состояние вызова |
| PICS | Protocol Implementation Conformance Statement | таблицы соответствия реализации протокола |  |
| PID | Process ID | идентификатор процесса |  |
| PID | Protocol ID | идентификатор протокола |  |
| PIM | Protocol Independent Multicast | независимая от протокола многоадресная передача |  |
| PIM-SM | Protocol Independent Multicast - Sparse Mode | режим независимой от протокола многоадресной передачи "Разреженный режим" |  |
| PIN | Personal Identification Number | личный идентификационный номер |  |
| PL | Priority Level | уровень приоритета |  |
| PLC | Power Line Communication | технология передачи информации по электросетям |  |
| PLMN | Public Landing Mobile Network | наземная сеть мобильной связи общего пользования |  |
| PLMNwACT | User Controlled PLMN Selector with Access Technology | элементарный файл для селектора сети, управляемого пользователем, с технологией доступа |  |
| PMD | Pseudonym Mediation Device | устройство передачи псевдонимов |  |
| PMIPv6 | Proxy Mobile IPv6 | расширение функциональности протокола IPv6 по обеспечению сетевой мобильности |  |
| PMM | Packet Mobility Management | управление мобильностью пакетов |  |
| PNG | Portable Network Graphics |  | один из форматов изображения |
| PNN | PLMN Network Name | сетевое имя PLMN |  |
| PNR | Phone Number Registry | регистр телефонных номеров | сервис определения региона/субъекта РФ и часового пояса по номеру |
| PNR | Premium Rate Number | номер с Premium-тарифом |  |
| PoC | Push-to-Talk over Cellular | режим полудуплексной сотовой связи |  |
| PoR | Proof-of-Receipt | уведомление о получении |  |
| PPF | Paging Proceed Flag | флаг посылки вызова |  |
| PPP | Point-to-Point Protocol | протокол канала связи с непосредственным соединением |  |
| PPR | Privacy Profile Register | регистр приватных профилей |  |
| PRAF | Positioning Radio Assistance Function | вспомогательная функция радиоподсистемы |  |
| PRBT | Private Ring Back Tone | персональный сигнал контроля посылки вызова |  |
| PRCF | Positioning Radio Coordinating Function | функция позиционирования в радиоподсистеме |  |
| PRF | Pseudo Random Function | псевдослучайная функция |  |
| PRI | Primary Rate Interface |  | стандартный интерфейс сети ISDN, определяющий подключение станций ISDN к широкополосным магистралям между АТС или сетевыми коммутаторами |
| ProSe | Proximity-Based Services | услуги при прямом соединении устройств |  |
| PRRM | Positioning Radio Resource Management | управление радиоресурсами позиционирования |  |
| PS | Packet Switch | коммутация пакетов |  |
| PS | Piggybacking Supported | поддержка вложенных подтверждений | передача подтверждения в блоке данных обратного направления, 3GPP TS 23.401 |
| PS | Profile Server | профайл-сервер |  |
| PSBCH | Physical Sidelink Broadcast Channel | физический широковещательный канал прямого соединения устройств |  |
| PSI | Public Service Identity | публичный идентификатор сервиса |  |
| PSLIP | Positive Slip | положительное проскальзывание | повтор фрейма |
| PSM | Power Saving Mode | режим экономии энергии |  |
| PSMF | Positioning Signal Measurement Function | функция измерения параметров сигнала при позиционировании |  |
| PSTN | Public Switched Telephone Network | телефонная сеть общего пользования |  |
| PTI | Procedure Transaction ID |  | идентификатор для отслеживания и соотношения ESM-запросов абонента |
| PTT | Push-to-Talk | полудуплексный режим приема-передачи |  |
| PUAN | Packet Uplink Acknowledgement/Negative Acknowledgement | подтверждение/отсутствие подтверждения приема пакета по восходящему каналу |  |
| PUK | PIN Unblocking Key | личный код разблокирования PIN |  |
| PVC | Permanent Virtual Circuit | постоянный виртуальный канал |  |
| PVI | Preemption Vulnerability Indicator | индикатор возможности завершения текущего вызова при наличии приоритетного входящего вызова |  |
| PWS | Public Warning System | система предупреждения населения |  |
| QCI | QoS Class Identifier | идентификатор класса QoS |  |
| QoHR | Query on HLR Release | запрос при отбое от HLR |  |
| QoP | Quality of Protection | качество предоставления безопасности |  |
| QoS | Quality of Service | качество обслуживания |  |
| QSIG | Q-Point Signaling System | сигнальная система рабочих точек | протокол сигнализации PBX между собой |
| R-APDU | Response-APDU | ответный протокольный блок данных прикладного уровня |  |
| R-TPDU | Response-TPDU | ответный протокольный блок данных транспортного уровня |  |
| RA | Recipient Address | адрес получателя предыдущего сообщения MAP-MO-SM |  |
| RAB | Radio Access Bearer | канал радиодоступа |  |
| RAC | Routing Area Code | код зоны маршрутизации |  |
| RACH | Random Access Channel | канал случайного доступа |  |
| RADIUS | Remote Authentication in Dial-In User Service | услуга удалённой аутентификации абонента телефонной сети | протокол для реализации аутентификации, авторизации и сбора сведений об использованных ресурсах |
| RAI | Remote Alarm Indication | индикация удаленной аварии |  |
| RAI | Routing Area Identity | идентификатор зоны маршрутизации |  |
| RAID | Redundant Array of Independent Disks | избыточный набор независимых дисковых накопителей |  |
| RAM | Random Access Memory | оперативное запоминающее устройство |  |
| RANAP | Radio Access Network Application Protocol | прикладная часть протокола сети радиодоступа |  |
| RAS | Remote Access System | система удаленного доступа |  |
| RAT | Radio Access Technology | технология радиодоступа |  |
| RATSCCH | Robust AMR Traffic Synchronised Control Channel | помехоустойчивый синхронизированный канал управления AMR-трафиком |  |
| RAU | Routing-Area-Update |  | процедура обновления маршрутизации |
| RBT | Ring Back Tone | контроль посылки вызова |  |
| RC | Redundancy Check | проверка избыточности |  |
| RC | Repeat Count | количество повторных попыток |  |
| RC | Routing Context | контекст маршрутизации |  |
| RCAF | RAN Congestion Awareness Function | функция контроля нагрузки на RAN |  |
| RCO | Resource Control Object | объект управления ресурсами |  |
| RCS | Rich Communication Services | система передачи сообщений и контактных данных в сетях мобильной связи |  |
| RD | Reject Duplicates | флаг приема/отбоя сообщений SMS-SUBMIT с одинаковыми MR и DA |  |
| RdPN | Redirected Party Number | номер абонента, перенаправленный в последний раз |  |
| RDS | Route Selection Descriptor | дескриптор выбора маршрута |  |
| REL | Release | освобождение линии ISUP |  |
| RFC | Request for Comments |  | документы, содержащие общепринятые технические спецификации и стандарты |
| RFM | Remote File Management | удаленное управление файлами |  |
| RFSP | RAT/Frequency Selection Priority | приоритет выбора RAT и частоты |  |
| RFU | Reserved for Future Use | зарезервировано для использования в будущем |  |
| RgPN | Redirecting Party Number | номер абонента, на котором последний раз сработала переадресация |  |
| RHCP | Remote Hardware Card Protei | протокол управления аппаратным обеспечением |  |
| RHCS | Red Hat Cluster Suite | программное обеспечение от Red Hat для создания кластера высокой доступности и кластера распределенной нагрузки |  |
| RHEL | Red Hat Enterprise Linux |  | дистрибутив Linux от Red Hat |
| RI | Repeat Interval | время ожидания между попытками |  |
| RI | Routing Indicator | код направления |  |
| RIF | Request Indication Flag | флаг индикации запроса |  |
| RIP | Routing Information Protocol | протокол маршрутизации информации |  |
| RLC | Radio Link Control | управление радиоканалами |  |
| RLC | Release Complete | освобождение канала ISUP завершено |  |
| RLOS | Restricted Local Operator Services | услуги, запрещенные местным оператором |  |
| RN | Routing Number | маршрутный номер |  |
| RnA | Routing and Alerting | маршрутизация и оповещение об ошибке | один из этапов вызова |
| RNC | Radio Network Controller | контроллер радиосети |  |
| RNL | Radio Network Layer | уровень радиосети |  |
| RNS | Radio Network Subsystem | подсистема радиосети |  |
| RNTI | Radio Network Temporary Identity | временный объект радиосети |  |
| ROS | Remote Operations Service | услуга удаленных операций |  |
| RP | Reply Path | адрес для отправки ответа |  |
| RPC | Remote Procedure Call | удаленный вызов процедур |  |
| RPF | Reverse Path Forwarding | переадресация в обратном направлении |  |
| RPH | Resource Priority Header | заголовок приоритета источника |  |
| RPS | Reductant Power Supply | резервный источник питания |  |
| RPS | Requests per Second | количество запросов в секунду |  |
| RRC | Radio Resource Control | управление радиоресурсами |  |
| RRLP | Radio Resource Location Services Protocol | протокол услуг по определению местоположения радиоресурсов |  |
| RRP | Reductant Ring Protocol | ошибкоустойчивый протокол резервирования |  |
| RSA | Rivest-Shamir-Adleman Algorithm | алгоритм RSA | один из несимметричных алгоритмов шифрования и цифровой подписи |
| RSC | Reset Circuit | сброс канала ISUP |  |
| RTCP | Real-Time Transport Control Protocol | протокол, управляющий передачей данных в режиме реального времени | работает совместно с RTP |
| RTD | Round Trip Delay | задержка на подтверждение приема |  |
| RTO | Recovery Time Objective | допустимое время восстановления |  |
| RTO | Retransmission Time | время ожидания до повторной отправки |  |
| RTP | Real-Time Transport Protocol | протокол передачи трафика в режиме реального времени |  |
| RTS | Relaxed Tail Sync |  | режим синхронизации кольцевого буфера, при котором значение tail увеличивается только один раз последним потоком |
| RTSP | Real-Time Streaming Protocol | потоковый протокол реального времени |  |
| RTT | Round-Trip Time | время приема-передачи |  |
| RUCI | RAN User Plane Congestion Information | пользовательская информация о перегрузке в сети радиодоступа |  |
| S-CSCF | Serving CSCF | узел обработки SIP-сообщений IMS |  |
| S1-AP | S1 Application Protocol | прикладной протокол для интерфейса S1 |  |
| SAC | Service Area Code | код зоны обслуживания |  |
| SACF | Single Association Control Function | функция управления одиночной ассоциацией |  |
| SAD | Source Address | адрес отправителя |  |
| SAE | System Architecture Evolution | эволюция архитектуры системы | архитектура сети для стандарта LTE |
| SAI | Service Area Identity | идентификатор зоны обслуживания |  |
| SAM | Subsequent Address Message | последующее адресное сообщение ISUP |  |
| SAPT | Service Access Point Identifier | идентификатор точки доступа к услугам |  |
| SATA | Serial ATA | последовательный ATA |  |
| SB | Service Builder | среда для настраиваемой инфраструктуры разработки приложений |  |
| SBC | Session Border Controller | пограничный контроллер сессий |  |
| SBcAP | SBc Application Part | прикладная часть протокола SBc | протокол взаимодействия CBC и MME |
| SBCF | Session Based Charging Function | функция тарификации на основе сеансов |  |
| SBSC | Serving Base Station Controller | контроллер обслуживающих BSS |  |
| SBSS | Serving BSS | обслуживающая BSS |  |
| SC-FDMA | Single Carrier - Frequency Division Multiple Access | множественный доступ с частотным разделением каналов с одной несущей частотой |  |
| SCA | Service Center Address | адрес сервисного центра |  |
| SCAP | Ericsson Service Charging Application Protocol | прикладной протокол тарификации услуг Ericsson |  |
| SCC-AS | Service Centralization and Continuity Application Server | сервер для централизации и непрерывности предоставления услуг |  |
| SCCP | Signaling Connection Control Part | подсистема управления сигнализацией |  |
| SCEF | Service Capability Exposure Function | технология для взаимодействия с сетью Интернета вещей через единый интерфейс |  |
| SCF | Service Control Function | функциональный объект управления услугами |  |
| SCLC | SCCP Connectionless Control | блок управления услугами SCCP без создания сигнального соединения |  |
| SCM | Single Connection Mode | режим одного соединения |  |
| SCME | SCF Management Entity | управляющий объект SCF |  |
| SCMF | Security Context Management Function | функция управления контекстом безопасности |  |
| SCMG | SCCP Management | средства эксплуатационного управления SCCP |  |
| SCOC | SCCP Connection-Oriented Control | блок управления услугами SCCP с созданием сигнального соединения |  |
| SCON | Signaling Congestion | управление нагрузкой сигнализации |  |
| SCP | Service Control Point | модуль логик услуг, реализованных посредством протокола CAMEL |  |
| SCPT | Service Category Programming Teleservice | услуга настройки категорий услуг |  |
| SCR | Source Controlled Rate | скорость, регулируемая источником |  |
| SCRC | SCCP Routing Control | средства управления маршрутизацией SCCP |  |
| SCS | Service Capabilities Server | сервер обеспечения работоспособности услуг |  |
| SCS | Supported Codec Set | набор поддерживаемых кодеков |  |
| SCSM | SCF Call State Model | модель состояний процесса обслуживания вызова SCF |  |
| SCTP | Stream Control Transmission Protocol | протокол передачи с управлением потока |  |
| SCWS | Smart Card Web Server | Web-сервер смарт-карт |  |
| SDCCH | Standalone Dedicated Control Channel | однополосный выделенный канал управления |  |
| SDL | System Description Language | язык описания систем |  |
| SDN | Service Dialling Number | номер набора сервисов |  |
| SDP | Session Description Protocol | протокол описания сессии |  |
| SDU | Service Data Unit | блок служебных данных |  |
| SE | Security Environment | защищенная среда |  |
| SEAF | Security Anchor Function | якорная функция безопасности | обеспечивает аутентификацию пользователей при их регистрации в сети с любой технологией доступа |
| SEID | Security Environment Identifier | индикатор замкнутой безопасной среды |  |
| SEP | Signaling Endpoint | оконечный сигнальный узел сети |  |
| SEPP | Security Edge Protection Proxy | пограничная сетевая функция |  |
| SES | Service Evaluation System | система сбора данных о работе сервисов |  |
| SFI | Short EF Identifier | короткий идентификатор элементарного файла |  |
| SFP | Small Form-Factor Pluggable | стандарт модульных компактных приёмопередатчиков в телекоммуникациях |  |
| SFTP | SSH File Transfer Protocol | прикладной протокол работы с файлами поверх надежного и безопасного соединения |  |
| SG | Signaling Gateway | сигнальный шлюз |  |
| SGP | Signaling Gateway Process | процесс обработки сигнализации в составе сигнального шлюза |  |
| SGsAP | SGs Application Part | прикладной протокол для интерфейса SGs |  |
| SGSN | Serving GPRS Support Node | узел обслуживания абонентов GPRS |  |
| SGW | Serving Gateway | обслуживающий шлюз |  |
| SGWCI | SGW Change Indication | индикация смены узла SGW |  |
| SHA | Secure Hash Algorithm | криптографический алгоритм хэширования |  |
| SHARC | Super Harvard Architecture Single-Chip Computer | одночиповый компьютер Super Harvard Architecture |  |
| SI | Scope Indication | индикация области видимости |  |
| SI | Service Indicator | индикатор услуги |  |
| SI | Signaling Information | сигнальная информация |  |
| SI | Socket Interface | сокет-интерфейс |  |
| SIC | Service Information Code | код сервисной информации |  |
| SIDF | Subscription Identifier Deconcealing Function | функция извлечения идентификатора пользователя |  |
| SIGTRAN | Signaling Transport | передача сигнальных сообщений телефонных сигнализаций по IP-сети |  |
| SIM | Subscriber Identity Module | модуль идентификации абонента |  |
| SIMD | Single Instruction: Multiple Data | одиночный поток команд, множественный поток данных | принцип, обеспечивающий параллельные вычисления |
| SIO | Service Information Octet | октет служебной информации |  |
| SIO | Status Indication: Out of Alignment | индикация статуса: не синхронизировано |  |
| SIOS | Status Indication: Out of Service | индикация статуса: не обслуживается |  |
| SIP | Session Initiation Protocol | протокол инициирования сеансов связи |  |
| SIPTO | Selected IP Traffic Offload | разгрузка выбранного IP-трафика |  |
| SL | Service Logic | обслуживающая логика |  |
| SLA | Service Level Agreement | соглашение о качестве предоставляемых услуг |  |
| SLC | Signaling Link Code | код сигнального соединения |  |
| SLF | Subscriber Location Function | функция определения местоположения абонента |  |
| SLP | Service Logic Program | программа логики услуги |  |
| SLPP | Subscriber LCS Privacy Profile | профиль приватности услуг LCS абонента |  |
| SLS | Signaling Link Set | набор сигнальных линий |  |
| SLT | Signaling Link Termination | окончание звена сигнализации |  |
| SLTC | Signaling Link Termination Control | контроль окончания звена сигнализации |  |
| SLTM | Signaling Link Termination Message | сообщение об окончании звена сигнализации |  |
| SM | Secure Message | сообщение с повышенными параметрами безопасности |  |
| SM_RP | Short Message Service Relay Sub-Layer Protocol | протокол подуровня ретрансляции службы коротких сообщений |  |
| SMDS | Switched Multimegabit Data Service | услуга коммутируемой многомегабитной передачи данных |  |
| SME | Protocol State Machine Malfunction | сбой конечного автомата протокола |  |
| SMF | Session Management Function | функция управления сессиями |  |
| SMLC | Serving Mobile Location Centre | сервисный центр позиционирования |  |
| SMPP | Short Messages Peer-to-Peer Protocol | протокол передачи сообщений одноранговой сети |  |
| SMS | Short Message Service | служба коротких сообщений |  |
| SMS-CB | Short Message Service - Cell Broadcast | режим SMS-службы "Трансляция по сотам" |  |
| SMS-PP | Short Message Service - Point to Point | режим SMS-службы "Соединение точка-точка" |  |
| SMS-SC | Short Message Service - Service Center | режим SMS-службы "Центр обслуживания" |  |
| SMSC | Short Message Service Center | центр обработки коротких сообщений |  |
| SMSF | SMS Function | функция поддержки обмена короткими текстовыми сообщениями посредством протокола NAS |  |
| SMSP | SMS Header Parameters | параметры заголовков SMS |  |
| SMSP | Storage Management Services Protocol | протокол служб управления запоминающими устройствами |  |
| SMSR | Short Message Status Report | отчет о статусе короткого сообщения |  |
| SMTP | Simple Mail Transfer Protocol | простой протокол передачи почты |  |
| SN | Notification System | система оповещения |  |
| SN | Serial Number | серийный номер |  |
| SNDC | Subnetwork Dependent Convergence | конвергенция зависимых подсетей |  |
| SNDCP | SubNetwork Dependent Convergence Protocol | протокол конвергенции зависимых подсетей |  |
| SNI | Server Name Indication | индикация имени сервера |  |
| SNMM | Signaling Network Management Message | сообщение от системы управления сетью сигнализации |  |
| SNMP | Simple Network Management Protocol | простой протокол управления сетью |  |
| SNOW3G |  | Потоковый алгоритм шифрования 3GPP |  |
| SOAP | Simple Object Access Protocol | простой протокол доступа к объектам | протокол обмена структурированными сообщениями в формате XML в компьютерных сетях |
| SoC | System on a Сhip | интегральная схема |  |
| SoLSA | Support of Localized Service Areas | поддержка локализованный областей обслуживания |  |
| SON | Self-Organizing Network | самоорганизующаяся сеть |  |
| SoR | Steering of Roaming | стиринг при нахождении в роуминговой сети |  |
| SOSM | System of the Operative and Search Measures | Система оперативно-розыскных мероприятий | СОРМ |
| SPC | Signaling Point Code | код сигнального пункта |  |
| SPCF | Security Policy Control Function | функция управления политикой безопасности |  |
| SPCI | SPC Indicator | индикатор SPC |  |
| SPI | Security Parameters Indication | индикация параметров безопасности |  |
| SPI | Serial Peripheral Interface | интерфейс для передачи данных на аппаратном уровне |  |
| SPI | System Packet Interface | системный пакетный интерфейс |  |
| SPN | Service Provider Name | название обслуживающего оператора связи |  |
| SPNE | Signal Processing Network Equipment | сетевое оборудование обработка сигналов |  |
| SPT | Service Point Trigger | условие активации точки триггера |  |
| SPT | Shortest Path Tree | дерево кратчайших маршрутов |  |
| SQL | Structured Query Language | язык структурированных запросов |  |
| SQN | Sequence Number | порядковый номер |  |
| SRF | Signaling Relay Function | функция ретрансляции сигнальных сообщений |  |
| SRI | Status Report Indication | индикация запроса отчета о состоянии |  |
| SRNC | Serving Radio Network Controller | контроллер обслуживающей радиосети |  |
| SRNS | Serving Radio Network Subsystem | обслуживающая подсистема радиосети |  |
| SRQ | Status Report Qualifier | указатель предыдущего сообщения: SMS-SUBMIT или SMS-COMMAND |  |
| SRR | Status Report Request | индикация запроса отчета о состоянии |  |
| SRSM | SRF Call State Model | модель состояний процесса обслуживания вызова в SRF |  |
| SRTP | Secure Real-Time Transport Protocol | безопасный протокол передачи трафика в режиме реального времени |  |
| SRV | Service Record | служебная запись | стандарт для DNS, задающий имена хостов и номера портов для некоторых служб |
| SRVCC | Single Radio Voice Call Continuity | система непрерывной голосовой связи с одним радиомодулем |  |
| SS | Supplementary Service | дополнительная услуга |  |
| SS7 | Signaling System 7 | общий канал сигнализации 7 |  |
| SSCP | Supplementary Service Control Protocol | протокол дополнительного сервисного контроля |  |
| SSD | Solid State Drive | твердотельный накопитель |  |
| SSE2 | Streaming SIMD Extensions 2 | потоковое расширение SIMD-процессора версии 2 |  |
| SSF | Server Switching Functions | сервер функциональной коммутации |  |
| SSH | Secure Shell | безопасная оболочка | прикладной протокол удаленного управления и туннелирования TCP-соединений |
| SSID | Service Set Identifier | идентификатор набора услуг сети |  |
| SSL | Secure Sockets Layer |  | протокол защищенных соединений с применением шифрования |
| SSME | SSF Management Entity | управляющий объект SSF |  |
| SSML | Speech Synthesis Markup Language | язык разметки для синтеза речи |  |
| SSN | Subsystem Number | номер подсистемы |  |
| SSNI | SSN Indicator | индикатор SSN |  |
| SSP | Service Switching Point | узел коммутации услуг |  |
| SSRC | Synchronization Source | источник синхронизации |  |
| SST | SIM service table | таблица услуг SIM |  |
| SSW | Softswitch | гибкий программный коммутатор |  |
| STM | Signaling Termination Manager | узел управления отправкой сигнальной информации |  |
| STM | Synchronous Transport Module | синхронный транспортный модуль |  |
| STN | Session Transfer Number | номер передачи сессии |  |
| STP | Signaling Transfer Point | магистральный шлюз для маршрутизации трафика |  |
| STT | Speech-to-Text | технология распознавания голосовой информации |  |
| SUA | SCCP User Adaptation Layer | уровень адаптации пользователя SCCP к пользователям IP-сети |  |
| SUCI | Subscription Concealed Identifier | скрытый идентификатор пользователя в сетях 5G |  |
| SUERM | Signal Unit Error Rate Monitoring | контроль коэффициента ошибок при передаче сигнальных единиц |  |
| SUME | Set Up Menu Element | элемент меню настройки |  |
| SUPI | Subscription Permanent Identifier | международный постоянный идентификатор подписки абонента в сетях 5G |  |
| SUPL | Secure User Plane Location | надежное определение местонахождения плоскости пользователя |  |
| SUS | Suspend | удержание вызова ISUP |  |
| SWP | Single Wire Protocol | однопроводной протокол |  |
| T-ADS | Terminating Access Domain Selection | выбор домена доступа для входящих вызовов |  |
| T-Bits | Time Alignment Bits | биты сопоставления времени |  |
| TAC | Tracking Area Code | код зоны отслеживания |  |
| TAD | Traffic Aggregate Description |  | параметр, содержащий идентификаторы пакетных фильтров абонентского устройства и информацию о них |
| TAI | Tracking Area Identity | идентификатор зоны отслеживания |  |
| TAP | Transferred Account Procedure | процедура передачи счета за услуги в роуминге |  |
| TAR | Toolkit Application Reference | справочник приложений | уникальный идентификатор приложений верхнего уровня |
| TAS | Telephony Application Server | сервер приложений телефонии |  |
| TAU | Tracking Area Update | обновление зоны обслуживания |  |
| TBF | Temporary Block Flow | временного потока блоков |  |
| TBFH | Time Delay Bad Frame Handling | временная задержка обработки плохого кадра |  |
| TCAP | Transaction Capabilities Application Part | прикладная подсистема возможностей транзакции |  |
| TCM | Toolkit for Conceptual Modeling | пакет программного обеспечения для концептуального моделирования |  |
| TCME | TFO Circuit Multiplication Equipment | оборудование уплотнения каналов TFO |  |
| TCP | Transport Control Protocol | протокол управления передачей данных |  |
| TCS | Terminal Capability Set | функциональные возможности терминала |  |
| TDD | Time Division Duplex | дуплексный режим с временным разделением |  |
| TDF | Traffic Detection Function | функция обнаружения трафика |  |
| TDM | Time Division Multiplexing | мультиплексирование с разделением по времени |  |
| TDMA | Time Division Multiple Access | множественный доступ с временным разделением каналов |  |
| TDP | Trigger Detection Point | триггерная точка обнаружения |  |
| TEI | Terminal Endpoint Identifier | идентификатор оконечной точки терминала |  |
| TEID | Tunnel Endpoint Identifier | идентификатор конечной точки туннеля |  |
| TETRA | Terrestrial Trunked Radio | магистральная наземная радиосвязь |  |
| TFO | Tandem Free Operation | работа без тандемного преобразования |  |
| TFT | Traffic Flow Template | шаблон потока трафика |  |
| TFTP | Trivial File Transfer Protocol | протокол передачи тривиального файла |  |
| THP | Traffic Handling Priority | приоритет обработки трафика |  |
| TICC | Transport Independent Call Control | система управления вызовами, независимая от способов передачи данных |  |
| TID | Thread ID | идентификатор потока |  |
| TID | Transaction Identifier | идентификатор транзакции |  |
| TIF | Translation Information Flag | флаг разрешения использовать короткие FTN в сети CAMEL |  |
| TISPAN | Telecoms and Internet Converged Services and Protocols for Advanced Networks |  | основное подразделение ETSI по стандартизации |
| TLB | Transmission Load Balancing | адаптивная балансировка нагрузки передачи |  |
| TLLI | Temporary Logical Link Identity | временный объект логического канала |  |
| TLS | Transport Layer Security |  | протокол защищенной передачи данных с применением шифрования |
| TLS-PSK | Pre-Shared Key Ciphersuites for Transport Layer Security | наборы шифров с предварительным общим ключом безопасности транспортного уровня |  |
| TLV | Tag-Length-Value | тэг-длина-значение | один из форматов записи данных в файлах |
| TMAG | Trusted Mobile Access Gateway | шлюз мобильного доступа |  |
| TMGI | Temporary Mobile Group Identity | временный идентификатор мобильной группы |  |
| TMR | Transmission Medium Requirement | требование к среде передачи |  |
| TMSI | Temporary Mobile Subscriber Identifier | временный идентификатор абонента мобильной связи |  |
| TMT | Traffic Mode Type | режим передачи трафика |  |
| ToM | Tunnelling of Messages | туннелирование сообщений |  |
| TON | Type of Number | тип нумерации |  |
| ToS | Type of Service | тип обслуживания |  |
| TPDU | Transport Protocol Data Unit | блок данных транспортного протокола |  |
| TPF | Traffic Plane Function | функции плоскости передачи трафика |  |
| TPI | Transport Provider Interface | интерфейс драйверов для взаимодействия с транспортными протоколами |  |
| TRAU | Transcoding Rate Adapter Unit | транскодер преобразования скорости передачи данных |  |
| TrGW | Transition Gateway | транзитный шлюз |  |
| TSCM | Transparent Single Connection Mode | режим одного прозрачного соединения |  |
| TSL | Timeslot | ячейка времени |  |
| TSL | Transaction Sublevel | подуровень транзакций | подуровень системы TCAP |
| TSP | Telecom Specific Peripheral | телекоммуникационное специальное периферийное устройство |  |
| TT | Translation Type | тип трансляции |  |
| TTF | Failure To Train | кадр отказа в настройке |  |
| TTG | Tunnel Terminating Gateway | оконечный шлюз туннеля |  |
| TTI | Transmission Time Interval | время передачи |  |
| TTL | Time-to-Live | время жизни |  |
| TTS | Text-to-Speech | технология синтеза речи |  |
| TTTP | Transfer to Third Party | передача третьей стороне |  |
| TUP | Telephone User Part | абонентская подсистема телефонной связи |  |
| TWAG | TWAN Access Gateway | шлюз сети TWAN |  |
| TWAN | Trusted WLAN Access Network | доверенная беспроводная локальная сеть доступа |  |
| UA | User-Agent | агент пользователя |  |
| UAN | Universal Access Number | номер универсального доступа |  |
| UBA | Unblocking Acknowledgement | подтверждение разблокировки канала ISUP |  |
| UBL | Unblocking | разблокировка канала ISUP |  |
| UBS | Universal Billing Server | универсальная биллинговый сервер |  |
| UCA | UICC Ciphering Algorithm | алгоритм шифрования UICC |  |
| UCI | User CSG Information | информация о закрытых группах абонента |  |
| UCS2 | Universal Coded Character Set 2 | стандарт кодировки букв и символов 2 байтами |  |
| UDH | User Data Header | пользовательские заголовки запросов |  |
| UDHI | User Data Header Indicator | индикатор UDH |  |
| UDHL | User Data Header Length | длина UDH |  |
| UDM | Unified Data Management | модуль управления данными пользователей |  |
| UDM | Unified Data Manager Function | функция управления собранными данными |  |
| UDP | User Datagram Protocol | протокол передачи датаграмм пользователей |  |
| UDR | Unified Data Repository | унифицированная база данных |  |
| UDSF | Unstructured Data Storage Function | система хранения неструктурированных данных |  |
| UDTS | Universal Data Transfer Service | универсальная система передачи данных |  |
| UE | User Equipment | пользовательское оборудование |  |
| UEA | UMTS Encryption Algorithm | алгоритм шифрования в сетях UMTS |  |
| UGW | User Gateway | абонентский шлюз |  |
| UI | User Input | пользовательский ввод |  |
| UI | User Interface | пользовательский интерфейс |  |
| UIA | UMTS Integrity Algorithm | алгоритм проверки целостности в сетях UMTS |  |
| UICC | Universal Integrated Circuit Card | универсальная карта с интегральной схемой |  |
| UIM | UICC Integrity Mechanism | механизм проверки цельности UICC |  |
| UIVR | Universal Interactive Voice Response | универсальный автоматический информатор |  |
| UL | Uplink | восходящая линия |  |
| ULI | User Location Information | информация о местоположении абонента |  |
| ULS | Unexpected Line Signal | непредвиденный линейный сигнал |  |
| UMTS | Universal Mobile Telecommunications System | универсальная мобильная телекоммуникационная система |  |
| UP | User Profile | профиль абонента |  |
| UPF | User Plane Function | функция передачи данных пользователей |  |
| URA | UTRAN Registration Area | зона регистрации в сетях UMTS |  |
| URL | Uniform Resource Locator | унифицированный адрес электронных ресурсов |  |
| URLLC | Ultra-Reliable and Low Latency Communications | ультранадежная связь с низкими задержками |  |
| URN | Uniform Resource Name | универсальное имя источника |  |
| URRP | UE Reachability Request Parameter | параметр запроса доступности в сетях UMTS |  |
| URSP | UE Route Selection Policy | политика выбора маршрута для оборудования пользователя |  |
| USAT | USIM Application Toolkit | набор приложений USIM |  |
| USC | USSD Service Center | центр обработки USSD |  |
| USIM | Universal Subscriber Identity Module | универсальный модуль идентификации абонента |  |
| USSD | Unstructured Supplementary Service Data | неструктурированные дополнительные служебные данные | технология взаимодействия абонента и приложения через обмен короткими сообщениями |
| USSDC | USSD Center | центр обработки USSD-запросов |  |
| UST | USIM Service Table | таблица услуг на USIM |  |
| USU | Used-Service-Unit | сервисный блок использованных услуг | Diameter-CCA |
| UTDOA | Uplink Time Difference of Arrival | разница времени получения сигнала по восходящей линии |  |
| UTF | Unicode Transformation Format | формат кодирования символов |  |
| UTRAN | UMTS Terrestrial Radio Access Network | сеть радиодоступа к UMTS |  |
| UTWAN | Untrusted WLAN | ненадежный беспроводный доступ |  |
| UUID | Universally Unique Identifier | универсальный уникальный идентификатор |  |
| VAD | Voice Activity Detection | функция обнаружения голоса |  |
| VAS | Value Added Service | дополнительная платная услуга |  |
| VASP | Value Added Service Provider | поставщик дополнительных платных услуг |  |
| VBD | Voiceband Data | данные в речевом диапазоне |  |
| VBS | Voice Broadcast Service | услуга голосового вещания |  |
| VFC | Voice Frequency Circuit | канал тональной частоты |  |
| VFS | Virtual File System | виртуальная файловая система |  |
| VGCS | Voice Group Call Service | услуга группового голосового вызова |  |
| VHE | Virtual Home Environment | виртуальное домашнее окружение |  |
| VLAN | Virtual Local Area Network | виртуальная локальная сеть |  |
| VLR | Visitor Location Register | регистр местоположения абонентов в роуминге в своей сети |  |
| VMA | Voice Mail Alerting | голосовые предупреждения |  |
| VMN | Voice Mail Notification | голосовые оповещения |  |
| VMSC | Visited MSC | абонентский MSC |  |
| VO | Virtual Office |  | услуга Виртуальный офис |
| VOC | Voice | голосовое сообщение |  |
| VoIP | Voice over Internet Protocol | технология передачи голосовых сообщений в IP-сетях |  |
| VoLTE | Voice over LTE | голосовой вызов на базе протокола LTE |  |
| VoMS | Voucher Management System | система управления платежными картами |  |
| VP | Validity Period | время прекращения валидности сообщения |  |
| VPF | Validity Period Format | флаг наличия Validity Period |  |
| VPLMN | Visited PLMN | гостевая PLMN |  |
| VPN | Virtual Private Network | виртуальная частная сеть |  |
| VRRP | Virtual Router Redundancy Protocol | протокол избыточности виртуальных маршрутизаторов |  |
| VSAT | Very Small Aperture Terminal | малый терминал спутниковой связи узкой направленности |  |
| WAAS | Wide Area Argumentation System | широкозонная корректирующая система |  |
| WAE | WAN Automation Engine | механизм автоматизации глобальных сетей |  |
| WAG | WLAN Access Gateway | шлюз доступа WLAN |  |
| WAP | Wireless Application Protocol | протокол для приложений беспроводной связи |  |
| WB | Welcome Back | система оповещения вновь зарегистрированных гостевых абонентов |  |
| WCDMA | Wideband Code Division Multiple Access | широкополосный множественный доступ с кодовым разделением каналов |  |
| WEMT | Wireless Enhanced Messaging Teleservice | улучшенная беспроводная услуга передачи сообщений |  |
| WISM | Wireless Services Module | модуль, предоставляющий беспроводные услуги |  |
| WIX | Wireless Information Exchange | беспроводной обмен информацией |  |
| WLAN | Wireless Local Area Network | беспроводная локальная сеть |  |
| WLSS | WebLogic SIP Server |  | программное обеспечение сервера Oracle для запуска SIP-приложений |
| WML | Wireless Markup Language | язык разметки для мобильных устройств |  |
| WMM | Wi-Fi Multimedia | мультимедиа Wi-Fi |  |
| WPS | Wireless Priority Service | служба беспроводной приоритетной связи |  |
| WSID | WLAN Specific Identifier | специальный идентификатор WLAN |  |
| WTP | Wireless Transport Protocol | протокол беспроводной передачи |  |
| XCAP | XML Configuration Access Protocol | протокол доступа к конфигурации XML |  |
| XML | eXtensible Markup Language | расширяемый язык разметки |  |
| XStream | Java library for XML or JSON serialization | библиотека Java для сериализации XML-/JSON-файлов |  |

Operation types description

Link to the GSM standard: <a href="https://www.etsi.org/deliver/etsi_gts/04/0480/05.00.00_60/gsmts_0480v050000p.pdf">GSM 04.80</a>

* RegisterSS (MS --> network);

This operation type is invoked by an MS to register data related to a supplementary service in the network.
When no BasicService parameter is provided, the registration applies to all provisioned and applicable basic services.

* EraseSS (MS --> network);

This operation type is invoked by an MS to erase data related to a supplementary service in the network.
When no BasicService parameter is provided, the erasure applies to all provisioned and applicable basic services.

* ActivateSS (MS --> network);

This operation type is invoked by an MS to request the network for a supplementary service activation.
When no BasicService parameter is provided, the activation applies to all provisioned and applicable basic services.

* DeactivateSS (MS --> network);

This operation type is invoked by an MS to request the network for a supplementary service deactivation.
When no BasicService parameter is provided, the deactivation applies to all provisioned and applicable basic services.

* InterrogateSS (MS --> network);

This operation type is invoked by an MS to request the network for a supplementary service interrogation.
When no BasicService parameter is provided, the interrogation applies to all provisioned and applicable basic services.

* NotifySS (network --> MS);

This operation type is invoked by the network to forward a supplementary service notification towards a mobile subscriber.

* RegisterPassword (MS --> network);

This operation type is invoked by an MS to register a new password related to the management by the subscriber himself of subscription data in the HLR. The operation "Register password" will be successful if the subscriber can provide the old password, the new password and the new password again as results of 3 subsequent operations "Get password".

* GetPassword (network --> MS);

This operation type is invoked by the network to request a password from the mobile subscriber. It may be used to allow the registration of a new password or the management of subscription data by the subscriber himself (e.g. modification of call barring activation status).

* ProcessUnstructuredSS-Data (MS --> network);

This operation type is invoked by an MS to relay unstructured information in order to allow end to end SS operation between the MS and the network following specific rules (e.g. embedding of keypad commands). The operation is used in order to provide backward compatibility (see TS GSM 04.90).

* ProcessUnstructuredSS-Request (MS --> network);

This operation type is invoked by an MS to start an unstructured supplementary service data application in the network.

* UnstructuredSS-Request (network --> MS);

This operation type is invoked by the network to request unstructured information from the MS in order to perform an unstructured supplementary service data application.

* UnstructuredSS-Notify (network --> MS);

This operation type is invoked by the network to give an unstructured supplementary service notification to the mobile user.

* ForwardCheckSSIndication (network --> MS);

This operation type is invoked by the network to indicate to the mobile subscriber that the status of supplementary services may not be correct in the network. The procedures for initiating ForwardCheckSSIndication are specified in TS GSM 09.02.

* ForwardChargeAdvice (network --> MS);

This operation type is invoked by the network to forward Advice of Charge information to the mobile subscriber.

* BuildMPTY (MS --> network);

This operation type is invoked by an MS to request the network to connect calls in a multi party call.

* HoldMPTY (MS --> network);

This operation type is invoked by an MS to put the MS-connection to a multi party call (invoked by that MS) on hold.

* RetrieveMPTY (MS --> network);

This operation type is invoked by an MS to request retrieval of a multi party call held by that MS.

* SplitMPTY (MS --> network);

This operation type is invoked by an MS to request a private communication with one of the remote parties in a multi party call invoked by that MS.

* ForwardCUG-Info (MS --> network);

This operation type is used by an MS to explicitly invoke a CUG call.

* ExplicitCT (MS --> Network);

This operation type is invoked by an MS to request the network to connect the two calls of the subscriber.

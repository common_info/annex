# Diameter AVP Types, [RFC 3588]("https://www.ietf.org/rfc/rfc3588.txt") #

## Generic Types ##

| Name | Description Eng | Description Rus | Length (+V) | Additional Info |
| :--- | :-------------- | :-------------- | :---------- | :-------------- |
| Integer32 | Signed 32-bit integer | Знаковое 32-битное целое число | 12 (16) |  |
| Integer64 | Signed 64-bit integer | Знаковое 64-битное целое число | 16 (20) |  |
| Unsigned32 | Unsigned 32-bit integer | Беззнаковое 32-битное целое число | 12 (16) |  |
| Unsigned64 | Unsigned 64-bit integer | Беззнаковое 64-битное целое число | 16 (20) |  |
| Float32 | 32-bit floating point single precision number | 32-битное число одинарной точности с плавающей точкой | 12 (16) |  |
| Float64 | 64-bit floating point single precision number | 64-битное число одинарной точности с плавающей точкой | 16 (20) |  |
| OctetString | String data of any length | Строка произвольной длины | 8 (12) | padded up to the full block |
| Grouped | Combination of values having different types | Набор значений различных типов | 8 (12) + сумма всех AVP | must be a divisor of 4 |

## Child Types ##
| Name | Parent | Description Eng | Description Rus | Length (+V) | Additional Info |
| :--- | :----- |:--------------- | :-------------- | :---------- | :-------------- |
| UTF8String | OctetString | String encoded with UTF-8 | Строка с кодировкой UTF-8 | Any (in octets) | No flags are recommended, if required, CR LF |
| Enumerated | Integer32 | Set of available options | Перечень допустимых значений | 12 (16) |  |
| Time | OctetString | Timestamp from Jan 1, 1900, with NTP format | Временная метка от 1 января, 1900, в формате NTP | 4 octets |  |
| DiameterIdentity | OctetString | Node Fully Qualified Domain Name (FDQN) | Полностью определенное доменное имя узла | 8 (12) |  |
| Address | OctetString | IPv4/IPv6 address | IPv4-/IPv6-адрес | 8 (12) |  |
| DiameterURI |  | Formatted URI | URI специального формата |  | [Address format](#address-format) |
| IPFilterRule | OctetString | Packet filter | Фильтр пакетов |  | [Filter format](#filter-format) |


### Address Format ###

aaa(s)://{FQDN}:{port=3868};{[transport](#transport)=sctp};{[proto](#proto)=diameter}

#### Transport ####

* udp;
* tcp;
* __sctp__.

#### Proto ####

* radius;
* tacacs+;
* __diameter__.


### Filter Format ###

{[action](#action)} {[dir](#dir)} {proto} [{src} {dest}](#src-and-dest) [{frag} {ipoptions spec} {tcpoptions spec} {established} {setup} {tcpflags spec} {icmptypes types}](#options)

#### Action ####

* permit: allow packets that match the rule;
* deny: drop packets that match the rule.

#### Dir ####

* in: from the terminal;
* out: to the terminal.

#### Src and Dest ####

* ipno: IPv4/IPv6 address in dotted-quad or canonical IPv6 form;
* ipno/bits: IP address with a mask.

#### Options ####

* frag: match if the packet is a fragment and not the first one of the datagram;
* ipoptions spec: match if IP header contains the comma-separated list of options specified in spec;
* tcpoptions spec: match if TCP header contains the comma-separated list of options specified in spec;
* established: match TCP only packets that have the RST or ACK bits set;
* setup: match TCP only packets that have the SYN bit set but no ACK one;
* tcpflags spec: match TCP only packets if the TCP header contains the comma-separated list of flags specified in spec;
* icmptypes types: match ICMP only packets.

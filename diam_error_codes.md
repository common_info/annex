| App ID | Result Code | Name | Description | Reference Point |
| :----- | :---------- | :--- | :---------- | :-------------- |
|  | 1XXX | Informational |  |  | 
| 0 | 1001 | DIAMETER_MULTI_ROUND_AUTH | Informs sender that the authentication mechanism being used requires multiple round trips, and a subsequent requests need to be issued in order for access to be granted. | Base Diameter |
|  | 2XXX | Success |  |  |
| 0 | 2001 | DIAMETER_SUCCESS | Request was successfully completed. (1) | Base Diameter |
| 3GPP (10415) | 2001 | DIAMETER_FIRST_REGISTRATION | User is authorized to register in this server. The user was not registered yet. | Cx/Dx |
| 0 | 2002 | DIAMETER_LIMITED_SUCCESS | Request is processed but additional processing is required to provide access to user. | Base Diameter |
| 3GPP (10415) | 2002 | DIAMETER_SUBSEQUENT_REGISTRATION | HSS informs the I-CSCF that the user is authorized to register this public identity, or AS-CSCF is already assigned and there is no need to select a new one. | Cx/Dx |
| 3GPP (10415) | 2003 | DIAMETER_UNREGISTERED_SERVICE | HSS informs the I-CSCF the public identity is not registered but has services related to unregistered state or the AS-CSCF shall be assigned to the user. | Cx/Dx |
|  | 2003 | DIAMETER_FIRST_REGISTRATION | The user was not previously registered. The Diameter server has now authorized the registration. |  | SIP, RFC4740 |
| 3GPP (10415) | 2004 | DIAMETER_SUCCESS_SERVER_NAME_NOT_STORED | HSS informs the S-CSCF that the de-registration is completed or the S-CSCF name is not stored in the HSS. | Cx/Dx |
|  | 2004 | DIAMETER_SUBSEQUENT_REGISTRATION | The user is already registered. The Diameter server has now authorized the re-registration. | SIP, RFC4740 |
|  | 2005 | DIAMETER_UNREGISTERED_SERVICE | The user is not currently registered, but the requested service can still be granted to the user. | SIP, RFC4740 |
|  | 2006 | DIAMETER_SUCCESS_SERVER_NAME_NOT_STORED | The request operation was successfully processed. The Diameter server does not keep a record of the SIP server address assigned to the user. | SIP, RFC4740 |
|  | 2007 | DIAMETER_SERVER_SELECTION | The Diameter server has authorized the registration. The user has already been assigned a SIP server, but it may be necessary to select a new SIP server for the user. | SIP, RFC4740 |
|  | 2008 | DIAMETER_SUCCESS_AUTH_SENT_SERVER_NOT_STORED | The requested operation was successfully executed. The Diameter server is sending a number of authentication credentials in the answer message. The Diameter server does not keep a record of the SIP server. | SIP, RFC4740 |
|  | 2009 | DIAMETER_SUCCESS_RELOCATE_HA |  | RFC5778 |
| 3GPP (10415) | 2021 | DIAMETER_PDP_CONTEXT_DELETION_INDICATION | (GGSN) – indication to the server that the requested PDP Context or IP-CAN session has been deleted;<br>(PGW) – indication to the server that the requested bearer or IP-CAN session has been deleted. | Gi/SGi |
|  | 3XXX | Protocol Failures |  |  |
| 0 | 3001 | DIAMETER_COMMAND_UNSUPPORTED | Returned if received Diameter command code is not supported/not recognized. | Base Diameter |
| 0 | 3002 | DIAMETER_UNABLE_TO_DELIVER | Message can not be delivered to the destination, because no host within the realm supporting the required application was available to process the request or the Destination-Host AVP was given without the associated Destination-Realm AVP. | Base Diameter |
| 0 | 3003 | DIAMETER_REALM_NOT_SERVED | Intended Realm is not recognized. | Base Diameter |
| 0 | 3004 | DIAMETER_TOO_BUSY | Unable to provide requested service -- while all other prerequisite checks are valid. | Base Diameter |
| 0 | 3005 | DIAMETER_LOOP_DETECTED | A loop was detected while trying to get the message to the intended recipient. | Base Diameter |
| 0 | 3006 | DIAMETER_REDIRECT_INDICATION | Redirect agent determined the request could not be satisfied locally, and the initiator should direct the request directly to the server whose contact information has been added to the response. | Base Diameter |
| 0 | 3007 | DIAMETER_APPLICATION_UNSUPPORTED | Request was received for an application that is not supported. | Base Diameter |
| 0 | 3008 | DIAMETER_INVALID_HDR_BITS | Request was received whose bits in the Diameter header were an invalid combination or a value inconsistent with the Command Code's definition. | Base Diameter |
| 0 | 3009 | DIAMETER_INVALID_AVP_BITS | Request received included an AVP whose flag bits are set to an unrecognized value or that is inconsistent with the AVP’s definition. | Base Diameter |
| 0 | 3010 | DIAMETER_UNKNOWN_PEER | CER message was received from unknown peer. | Base Diameter |
| 0 | 3011 | DIAMETER_REALM_REDIRECT_INDICATION | Indicates that a server has determined that the request within an application supporting realm-based redirection could not be satisfied locally, and the initiator of the request SHOULD direct the request directly to a peer within a realm that has been identified in the response. | Base Diameter |
|  | 4XXX | Transient Failures |  |  |
| 0 | 4001 | DIAMETER_AUTHENTICATION_REJECTED | The authentication process for the user failed, most likely due to an invalid password used by the user. | Base Diameter |
| 0 | 4002 | DIAMETER_OUT_OF_SPACE | Received accounting information but unable to store it because of lack of memory. | Base Diameter |
| 0 | 4003 | ELECTION_LOST | Peer has determined that it has lost the election process and has therefore disconnected the transport connection. | Base Diameter |
| 0 | 4004 |  |  | RFC4004 |
| 0 | 4005 | DIAMETER_ERROR_MIP_REPLY_FAILURE | Used by the home agent when processing of the Registration Request has failed. | RFC4004 |
| 0 | 4006 | DIAMETER_ERROR_HA_NOT_AVAILABLE | Used to inform the foreign agent that the requested Home Agent cannot be assigned to the mobile node at this time. The foreign agent MUST return a Mobile IPv4 Registration. | RFC4004 |
| 0 | 4007 | DIAMETER_ERROR_BAD_KEY | Used by the home agent to indicate to the local Diameter server that the key generated is invalid. | RFC4004 |
| 0 | 4008 | DIAMETER_ERROR_MIP_FILTER_NOT_SUPPORTED | Used by a mobility agent to indicate to the home Diameter server that the requested packet filter Rules cannot be supported. | RFC4004 |
| 0 | 4009 |  |  | RFC4004 |
| 0 | 4010 | DIAMETER_END_USER_SERVICE_DENIED | Request denied due to service restrictions. | Base Diameter |
| 0 | 4011 | DIAMETER_CREDIT_CONTROL_NOT_APPLICABLE | Service can be granted but that further credit-control is needed for the service. | Base Diameter |
| 0 | 4012 | DIAMETER_CREDIT_LIMIT_REACHED | Request denied because the  end user’s account could not cover the requested service. | Base Diameter |
|  | 4013 | DIAMETER_USER_NAME_REQUIRED | The Diameter request did not contain a User-Name AVP, which is required to complete the transaction. The Diameter peer MAY include a User-Name AVP and attempt the request again. | SIP, RFC4740 |
|  | 4014 | RESOURCE_FAILURE | The DNCA Diameter peer within the NAT device indicates that the binding could not be installed or a new session could not be created due to resource shortage. | RFC6736 |
| 3GPP (10415) | 4100 | DIAMETER_USER_DATA_NOT_AVAILABLE | Requested user data is not available at this time to satisfy the requested operation. | Sh/Dh |
| 3GPP (10415) | 4101 | DIAMETER_PRIOR_UPDATE_IN_PROGRESS | Repository data update at the HSS could not be completed because the related repository data is currently being updated by another entity. | Sh/Dh |
| 3GPP (10415) | 4121 | DIAMETER_ERROR_OUT_OF_RESOURCES |  A MBMS Session Start procedure could not be performed due to a temporary resource shortage in the GGSN. The BM-SC may re-try later. | Gmb |
| 3GPP (10415) | 4141 | DIAMETER_PCC_BEARER_EVENT | Used when a PCC rule cannot be enforced or modified successfully in a network initiated procedure. | Gx |
| 3GPP (10415) | 4142 | DIAMETER_BEARER_EVENT | Used when a QoS rule cannot be enforced or modified successfully in a network initiated procedure | Gxx |
| 3GPP (10415) | 4143 | DIAMETER_AN_GW_FAILED | Used when the policy decisions received within a RAR initiated by the PCRF cannot be enforced by the PCEF because the AN-Gateway has failed. | Gx |
| 3GPP (10415) | 4144 | DIAMETER_PENDING_TRANSACTION | Used when a node that supports the PendingTransaction feature receives an incoming request on a session while it has an ongoing transaction on the same session and cannot handle the request. | Gx, S9, Np |
| 3GPP (10415) | 4181 | DIAMETER_AUTHENTICATION_DATA_UNAVAILABLE | Sent by the HSS to indicate that an unexpectedly transient failure occurs. The requesting node can try the request again in the future. | Cx/Dx, S6, S13 |
| 3GPP (10415) | 4182 | DIAMETER_ERROR_CAMEL_SUBSCRIPTION_PRESENT | Sent by the HSS to indicate that the subscriber to be registered has SGSN CAMEL Subscription data. | S6, S13, S7a, S7d |
| 3GPP (10415) | 4201 | DIAMETER_ERROR_ABSENT_USER | Sent by the HSS to indicate that the location of the targeted user is not known at this time to satisfy the requested operation. | SLh |
| 3GPP (10415) | 4221 | DIAMETER_ERROR_UNREACHABLE_USER | Sent by the MME to indicate that the user could not be reached in order to perform positioning procedure. | SLg, T6a/T6b |
| 3GPP (10415) | 4222 | DIAMETER_ERROR_SUSPENDED_USER | Sent by the MME to indicate that the user is suspended in the MME. | SLg |
| 3GPP (10415) | 4223 | DIAMETER_ERROR_DETACHED_USER | Sent by the MME to indicate that the user is detached in the MME. | SLg |
| 3GPP (10415) | 4224 | DIAMETER_ERROR_POSITIONING_DENIED | Sent by the MME to indicate that the positioning procedure was denied. | SLg |
| 3GPP (10415) | 4225 | DIAMETER_ERROR_POSITIONING_FAILED | Sent by the MME to indicate that the positioning procedure failed. | SLg |
| 3GPP (10415) | 4226 | DIAMETER_ERROR_UNKNOWN_UNREACHABLE LCS_CLIENT | Sent by the GMLC to indicate that the LCS Client was not known or could not be reached. | SLg |
| 3GPP (10415) | 4241 | DIAMETER_ERROR_NO_AVAILABLE_POLICY_COUNTERS | Used by the OCS to indicate to the PCRF that the OCS has no available policy counters for the subscriber. | Sy |
| 3GPP (10415) | 4261 | REQUESTED_SERVICE_TEMPORARILY_NOT_AUTHORIZED | The PCRF temporarily rejects new or modified service information because the network is temporarily not able to provide the service delivery that the AF requested, e.g. due to the service information is not consistent with the operator defined policy rules for the congestion status of the user. | Rx |
|  | 5XXX | Permanent Failures |  |  |
| 0 | 5001 | DIAMETER_AVP_UNSUPPORTED | AVP received that is not recognized or supported and was marked with the "M" (Mandatory) bit. | Base Diameter |
| 3GPP (10415) | 5001 | DIAMETER_ERROR_USER_UNKNOWN | Message was received for a user or a wildcarded identity that is unknown. | Cx/Dx, S6, S13, STa, S6b/H2, SWd, SWa, SWm, SWx, SLg, SLh, PC4, T6a/T6b |
| 0 | 5002 | DIAMETER_UNKNOWN_SESSION_ID | Request received with an unknown session ID. | Base Diameter |
| 3GPP (10415) | 5002 | DIAMETER_ERROR_IDENTITIES_DONT_MATCH | Message was received with a public identity and a private identity for a user, and server determines that the public identity does not correspond to the private identity. | Cx/Dx, Sh/Dh |
| 0 | 5003 | DIAMETER_AUTHORIZATION_REJECTED | Request received for which the user could not be authorized. | Base Diameter |
| 3GPP (10415) | 5003 | DIAMETER_ERROR_IDENTITY_NOT_REGISTERED | A query for location information is received for a public identity that has not been registered before. The user to which this identity belongs cannot be given service in this situation. | Cx/Dx, STa, S6b/H2, SWd, SWa, SWm, SWx |
| 0 | 5004 | DIAMETER_INVALID_AVP_VALUE | The request contained an AVP with an invalid value in its portion. | Base Diameter |
| 3GPP (10415) | 5004 | DIAMETER_ERROR_ROAMING_NOT_ALLOWED | Sent by the HSS to indicate that the subscriber is not allowed to roam within the MME or SGSN area. | Cx/Dx, S6, S13, STa, S6b/H2, SWd, SWa, SWm, SWx |
| 0 | 5005 | DIAMETER_MISSING_AVP | Request received does not contain an AVP required by the command code definition. | Base Diameter |
| 3GPP (10415) | 5005 | DIAMETER_ERROR_IDENTITY_ALREADY_REGISTERED | Identity has already a server assigned and the registration status does not allow that it is overwritten. | Cx/Dx, STa, S6b/H2, SWd, SWa, SWm, SWx |
| 0 | 5006 | DIAMETER_RESOURCES_EXCEEDED | Request was received that cannot be authorized because the user has already expended allowed resources. | Base Diameter |
| 3GPP (10415) | 5006 | DIAMETER_ERROR_AUTH_SCHEME_NOT_SUPPORTED | Authentication scheme in an authentication request is not supported. | Cx/Dx
| 0 | 5007 | DIAMETER_CONTRADICTING_AVPS | Request was received with AVPs that are contradictory to each other. | Base Diameter |
| 3GPP (10415) | 5007 | DIAMETER_ERROR_IN_ASSIGNMENT_TYPE | Identity being registered has already the same server assigned and the registration status does not allow the server assignment type or the Public Identity type received in the request is not allowed for the indicated server-assignment-type. | Cx/Dx |
| 0 | 5008 | DIAMETER_AVP_NOT_ALLOWED | Message was received with an AVP that must not be present. | Base Diameter |
| 3GPP (10415) | 5008 | DIAMETER_ERROR_TOO_MUCH_DATA | Volume of the data pushed to the receiving entity exceeds its capacity. | Cx/Dx |
| 3GPP (10415) | 5009 | DIAMETER_ERROR_NOT_SUPPORTED_USER_DATA | The S-CSCF informs HSS that the received subscription data contained information which was not recognised/supported | Cx/Dx |
| 0 | 5010 | DIAMETER_NO_COMMON_APPLICATION | Response of CER if no common application supported between the peers. | Base Diameter |
| 0 | 5011 | DIAMETER_UNSUPPORTED_VERSION | Unsupported version. | Base Diameter |
| 3GPP (10415) | 5011 | DIAMETER_ERROR_FEATURE_UNSUPPORTED | A request application message was received indicating that the origin host requests that the command pair would be handled using a feature which is not supported by the destination host. | Cx/Dx, Sh/Dh |
| 0 | 5012 | DIAMETER_UNABLE_TO_COMPLY | Message rejected because of unspecified reasons. | Base Diameter |
| 3GPP (10415) | 5012 | DIAMETER_ERROR_SERVING_NODE_FEATURE_UNSUPPORTED | Used when the HSS supports the P-CSCF-Restoration-mechanism feature, but none of the user serving node(s) supports it, as described by 3GPP TS 23.380 | Cx/Dx |
| 0 | 5013 | DIAMETER_INVALID_BIT_IN_HEADER | Reserved bit in the Diameter header is set to one (1) or the bits in the Diameter header are set incorrectly. | Base Diameter |
| 0 | 5014 | DIAMETER_INVALID_AVP_LENGTH | Invalid AVP length. | Base Diameter |
| 0 | 5015 | DIAMETER_INVALID_MESSAGE_LENGTH | Invalid Message length. | Base Diameter |
| 0 | 5016 | DIAMETER_INVALID_AVP_BIT_COMBO | Request contained an AVP with which is not allowed to have the given value in the AVP Flags field. | Base Diameter |
| 0 | 5017 | DIAMETER_NO_COMMON_SECURITY | Response to CER if no common security mechanism supported between the peers. | Base Diameter |
| 0 | 5018 | DIAMETER_RADIUS_AVP_UNTRANSLATABLE | Where a Diameter/RADIUS gateway receives a Diameter message containing a NAS-Filter-Rule AVP that is too large to fit into a RADIUS packet. | RFC4849 |
| 0 | 5024 | DIAMETER_ERROR_NO_FOREIGN_HA_SERVICE | Used by the AAAF to inform the AAAH that allocation of a home agent in the foreign domain is not permitted at this time. | SIP, RFC4740 |
| 0 | 5025 | DIAMETER_ERROR_END_TO_END_MIP_KEY_ENCRYPTION | Used by the AAAF to inform the AAAH that allocation of a home agent in the foreign domain is not permitted at this time. | SIP, RFC4740 |
| 0 | 5030 | DIAMETER_USER_UNKNOWN | The server does not recognize the user. | SIP, RFC4740 |
| 0 | 5031 | DIAMETER_RATING_FAILED | OCS was not able to correctly rate the service due to errors in one or more AVPs provided in the CCR. | Base Diameter |
|  | 5032 | DIAMETER_ERROR_USER_UNKNOWN | The SIP-AOR AVP value does not belong to a known user in this realm. | SIP, RFC4740 |
|  | 5033 | DIAMETER_ERROR_IDENTITIES_DONT_MATCH | The value in one of the SIP-AOR AVPs is not allocated to the user specified in the User-Name AVP. | SIP, RFC4740 |
|  | 5034 | DIAMETER_ERROR_IDENTITY_NOT_REGISTERED | A query for location information is received for a SIP AOR that has not been registered before. The user to which this identity belongs cannot be given service in this situation. | SIP, RFC4740 |
|  | 5035 | DIAMETER_ERROR_ROAMING_NOT_ALLOWED | The user is not allowed to roam to the visited network. | SIP, RFC4740 |
|  | 5036 | DIAMETER_ERROR_IDENTITY_ALREADY_REGISTERED | The identity being registered has already been assigned a server and the registration status does not allow that it is overwritten. | SIP, RFC4740 |
|  | 5037 | DIAMETER_ERROR_AUTH_SCHEME_NOT_SUPPORTED | The authentication scheme indicated in an authentication request is not supported. | SIP, RFC4740 |
|  | 5038 | DIAMETER_ERROR_IN_ASSIGNMENT_TYPE | The SIP server address sent in the SIP-Server-URI AVP value of the Diameter Server-Assignment-Request (SAR) command is the same SIP server address that is currently assigned to the user name, but the SIP-Server-Assignment-Type AVP is not allowed. | SIP, RFC4740 |
|  | 5039 | DIAMETER_ERROR_TOO_MUCH_DATA | The Diameter peer in the SIP server receives more data than it can accept. The SIP server cannot overwrite the already stored data. | SIP, RFC4740 |
|  | 5040 | DIAMETER_ERROR_NOT SUPPORTED_USER_DATA | The SIP server informs the Diameter server that the received subscription data contained information that was not recognized or supported. | SIP, RFC4740 |
| 3GPP (10415) | 5041 | DIAMETER_ERROR_USER_NO_WLAN_SUBSCRIPTION | Message was received for a user with no WLAN subscription. | Wa, Wd, Wx, Wm, Wd |
| 3GPP (10415) | 5042 | DIAMETER_ERROR_W-APN_UNUSED_BY_USER | Message was received for a user who has no subscription for a specified W-APN. | Wa, Wd, Wx, Wm, Wd |
| 3GPP (10415) | 5043 | DIAMETER_ERROR_NO_ACCESS_INDEPENDENT_SUBSCRIPTION | Message was received requesting WLAN 3GPP IP access for a user whose subscription does not allow it if it was not previously authenticated by WLAN Direct IP Access. | Wa, Wd, Wx, Wm, Wd |
| 3GPP (10415) | 5044 | DIAMETER_ERROR_USER_NO_W-APN_SUBSCRIPTION | Message was received requesting WLAN 3GPP IP access for a user whose subscription does not allow it if it was not previously authenticated by WLAN 3GPP direct access. | Wa, Wd, Wx, Wm, Wd |
| 3GPP (10415) | 5045 | DIAMETER_ERROR_UNSUITABLE_NETWORK |  | Wa, Wd, Wx, Wm, Wd |
| 3GPP (10415) | 5048 | DIAMETER_ERROR_EAP_CODE_UNKNOWN | Used by the Diameter server to inform the peer that the received EAP-Payload AVP contains an EAP packet with an unknown EAP code. | STa, S6b, SWd, SWm, SWx
| 3GPP (10415) | 5061 | INVALID_SERVICE_INFORMATION | PCRF rejects new or modified service information the service information provided by the AF is invalid /insufficient for the server to perform the requested action. | Rx/Gq
| 3GPP (10415) | 5062 | FILTER_RESTRICTIONS | PCRF rejects new or modified service information because the Flow-Description AVPs cannot be handled by the server. | Rx/Gq
| 3GPP (10415) | 5063 | REQUESTED_SERVICE_NOT_AUTHORIZED | PCRF rejects new or modified service information because the requested service is not consistent with the related subscription information /operator defined policy rules and/or the supported features in the IP-CAN network. | Rx |
| 3GPP (10415) | 5064 | DUPLICATED_AF_SESSION | PCRF rejects a new Rx session setup because the new Rx session relates to an AF session with another related active Rx session. | Rx |
| 3GPP (10415) | 5065 | IP-CAN_SESSION_NOT_AVAILABLE | PCRF rejects a new Rx session setup when it fails to associate the described service IP flows within the session information received from the AF to an existing IP-CAN session. | IP-CAN Session Not Available (PCRF) Transaction Failed (Client) | Rx |
| 3GPP (10415) | 5066 | UNAUTHORIZED_NON_EMERGENCY_SESSION | PCRF rejects new Rx session setup because the session binding function associated a non-Emergency IMS session to an IP-CAN session established to an Emergency APN. | Rx |
| 3GPP (10415) | 5067 | UNAUTHORIZED_SPONSORED_DATA_CONNECTIVITY | The PCRF rejects a new Rx session setup because the PCRF can’t authorize the sponsored data connectivity based on the sponsored data connectivity profile or the operator policy. | Rx |
| 3GPP (10415) | 5068 | TEMPORARY_NETWORK_FAILURE | The PCRF rejects new or modified service information because there is a temporary failure in the access network (e.g. the SGW has failed). | Rx |
| 3GPP (10415) | 5100 | DIAMETER_ERROR_USER_DATA_NOT_RECOGNIZED | The data received by the AS is not supported or recognized. | Sh/Dh |
| 3GPP (10415) | 5101 | DIAMETER_ERROR_OPERATION_NOT_ALLOWED | The requested operation is not allowed for the user. | Sh/Dh, T6a/T6b |
| 3GPP (10415) | 5102 | DIAMETER_ERROR_USER_DATA_CANNOT_BE_READ | Requested user data is not allowed to be read. | Sh/Dh |
| 3GPP (10415) | 5103 | DIAMETER_ERROR_USER_DATA_CANNOT_BE_MODIFIED | Requested user data is not allowed to be modified. | Sh/Dh |
| 3GPP (10415) | 5104 | DIAMETER_ERROR_USER_DATA_CANNOT_BE_NOTIFIED | Requested user data is not allowed to be notified on changes. | Sh/Dh |
| 3GPP (10415) | 5105 | DIAMETER_ERROR_TRANSPARENT_DATA_OUT_OF_SYNC | Repository data update at the HSS could not be completed because the requested update is based on an out-of-date version of the repository data. | Sh/Dh |
| 3GPP (10415) | 5106 | DIAMETER_ERROR_SUBS_DATA_ABSENT | AS requested to subscribe to changes to Repository Data that is not present in the HSS. | Sh/Dh |
| 3GPP (10415) | 5107 | DIAMETER_ERROR_NO_SUBSCRIPTION_TO_DATA | AS received a notification of changes of some information to which it is not subscribed. | Sh/Dh |
| 3GPP (10415) | 5108 | DIAMETER_ERROR_DSAI_NOT_AVAILABLE | AS addressed a DSAI not configured in the HSS. | Sh/Dh |
| 3GPP (10415) | 5120 | DIAMETER_ERROR_START_INDICATION | A MBMS Session Start procedure could not be performed due to some of the required session attributes that are necessary to activate the bearer resources are missing. | Gmb |
| 3GPP (10415) | 5121 | DIAMETER_ERROR_STOP_INDICATION |  Session stop has been received with no session start procedure running. | Gmb |
| 3GPP (10415) | 5122 | DIAMETER_ERROR_UNKNOWN_MBMS_BEARER_SERVICE | The requested MBMS service is unknown at the BM-SC. | Gmb |
| 3GPP (10415) | 5123 | DIAMETER_ERROR_SERVICE_AREA | The MBMS service area indicated for a specific MBMS Bearer Service is unknown or not available. | Gmb |
| 3GPP (10415) | 5140 | DIAMETER_ERROR_INITIAL_PARAMETERS | Used when the set of bearer or session or subscriber information needed by the PCRF for rule selection is incomplete/ erroneous/not available for the decision to be made. | Gx |
| 3GPP (10415) | 5141 | DIAMETER_ERROR_TRIGGER_EVENT | Used when the set of bearer/session information sent in a CCR originated due to a trigger event been met is inconsistent with the previous set of bearer/session information for the same bearer/session. | Gx |
| 3GPP (10415) | 5142 | DIAMETER_PCC_RULE_EVENT | The PCC rules cannot be installed/activated. Affected PCC-Rules will be provided in the Charging-Rule-Report AVP including the reason and status. Absence of the Charging-Rule-Report means that all provided PCC rules for that specific bearer/session are affected. | Gx |
| 3GPP (10415) | 5143 | DIAMETER_ERROR_BEARER_NOT_AUTHORIZED | Used when the PCRF cannot authorize an IP-CAN bearer upon the reception of an IP-CAN bearer authorization request coming from the PCEF. | Gx |
| 3GPP (10415) | 5144 | DIAMETER_ERROR_TRAFFIC_MAPPING_INFO_REJECTED | Used when the PCRF does not accept one or more of the traffic mapping filters. | Gx |
| 3GPP (10415) | 5145 | DIAMETER_QOS_RULE_EVENT | Used when the QoS rules cannot be installed/activated. | Gxx |
| 3GPP (10415) | 5147 | DIAMETER_ERROR_CONFLICTING_REQUEST | Used when the PCRF cannot accept the UE-initiated resource request as a network-initiated resource allocation is already in progress. | Gx |
| 3GPP (10415) | 5148 | DIAMETER_ADC_RULE_EVENT | Used when the ADC rules cannot be installed/activated. | Gx, Sd |
| 3GPP (10415) | 5149 | DIAMETER_ERROR_NBIFOM_NOT_AUTHORIZED | Invalid combination of IP-CAN and RAT Types or not allowed by the subscription | Gx, Sd |
| 3GPP (10415) | 5401 | DIAMETER_ERROR_IMPI_UNKNOWN | A message was received by the HSS for an IMPI that is unknown. | Zn/Zh/Zpn |
| 3GPP (10415) | 5402 | DIAMETER_ERROR_NOT_AUTHORIZED | A message was received by the BSF which the BSF can not authorize. | Zn/Zh/Zpn |
| 3GPP (10415) | 5403 | DIAMETER_ERROR_TRANSACTION_IDENTIFIER_INVALID | A message was received by the BSF for an invalid (e.g. unknown or expired) Bootstrapping Transaction Identifier (BTID). | Zn/Zh/Zpn |
| 3GPP (10415) | 5420 | DIAMETER_ERROR_UNKNOWN_EPS_SUBSCRIPTION | Sent by the HSS to indicate that no EPS subscription is associated with the IMSI. | Cx/Dx, S6, S13 |
| 3GPP (10415) | 5421 | DIAMETER_ERROR_RAT_NOT_ALLOWED | This result code shall be sent by the HSS to indicate the RAT type the UE is using is not allowed for the IMSI. | Cx/Dx, S6, S13 |
| 3GPP (10415) | 5422 | DIAMETER_ERROR_EQUIPMENT_UNKNOWN | This result code shall be sent by the EIR to indicate that the mobile equipment is not known in the EIR. | Cx/Dx, S6, S13 |
| 3GPP (10415) | 5423 | DIAMETER_ERROR_UNKNOWN_SERVING_NODE | Sent by the HSS to indicate that a Notify command has been received from a serving node which is not registered in HSS as the node currently serving the user. | S6, S13 |
| 3GPP (10415) | 5450 | DIAMETER_ERROR_USER_NO_NON_3GPP_SUBSCRIPTION | This result code shall be sent by the HSS to indicate that no non-3GPP subscription is associated with the IMSI. | STa, S6b, SWd, SWa, SWm, SWx |
| 3GPP (10415) | 5451 | DIAMETER_ERROR_USER_NO_APN_SUBSCRIPTION | Sent by the 3GPP AAA Server to indicate that the requested APN is not included in the user’s profile, and therefore is not authorized for that user. | STa, S6b/H2, SWd, SWa, SWm, SWx, T6a/T6b, S6t |
| 3GPP (10415) | 5452 | DIAMETER_ERROR_RAT_TYPE_NOT_ALLOWED | Sent by the HSS to indicate the RAT type the UE is using is not allowed for the IMSI. | STa, S6b/H2, SWd, SWa, SWm, SWx |
| 3GPP (10415) | 5453 | DIAMETER_ERROR_LATE_OVERLAPPING_REQUEST | Sent by the 3GPP AAA Server to indicate that the incoming request collides with an existing session which has a more recent time stamp than the time stamp of the new request. | STa, S6b/H2, SWd, SWa, SWm, SWx |
| 3GPP (10415) | 5454 | DIAMETER_ERROR_TIMED_OUT_REQUEST | Sent by the 3GPP AAA Server to indicate that the incoming request is known to have already timed out at the originating entity. | STa, S6b/H2, SWd, SWa, SWm, SWx |
| 3GPP (10415) | 5470 | DIAMETER_ERROR_SUBSESSION | Used when one or more S9 subsessions within the answer contains an unsuccessful Result-Code or Experimental-Result-Code value. | S9 |
| 3GPP (10415) | 5471 | DIAMETER_ERROR_ONGOING_SESSION_ESTABLISHMENT | Used within the TEA command by the V-PCRF when the V-PCRF receives a TER command from the H-PCRF to trigger an S9 session establishment for the EPC-routed traffic while the S9 session is being established for the NSWO traffic by the V-PCRF initiated by the BPCF. | S9 |
| 3GPP (10415) | 5490 | DIAMETER_ERROR_UNAUTHORIZED_REQUESTING_NETWORK | Sent by the MME to indicate that the requesting GMLC’s network is not authorized to request UE location information. | SLg, SLh |
| 3GPP (10415) | 5510 | DIAMETER_ERROR_UNAUTHORIZED_REQUESTING_ENTITY | Sent by the HSS to indicate that the SCS is not allowed to request control plane services for an UE, to the MTC-IWF. | S6m, S6n, S6t, T6a/T6b |
| 3GPP (10415) | 5511 | DIAMETER_ERROR_UNAUTHORIZED_SERVICE | Sent by the HSS to indicate that the specific service requested by the SCS is not allowed for an UE, or that it cannot be delivered according to the current subscribed services of the UE. | S6m, S6n, S6t, T6a/T6b |
| 3GPP (10415) | 5512 | DIAMETER_ERROR_REQUESTED_RANGE_IS_NOT ALLOWED | Sent by the HSS to indicate that the specific service requested by the SCEF is not allowed for an UE, or that it cannot be delivered according to the current subscribed services of the UE. | S6t |
| 3GPP (10415) | 5513 | DIAMETER_ERROR_CONFIGURATION_EVENT_STORAGE_NOT_ SUCCESSFUL | Sent by the MME/SGSN to indicate that the specific service requested by the SCEF could not be stored. | T6a/T6b, S6t |
| 3GPP (10415) | 5514 | DIAMETER_ERROR_CONFIGURATION_EVENT_NON_EXISTANT | Sent by the IWK-SCEF to indicate that the requested deletion by the MME/SGSN could not be performed because the event does not exist. | T6a/T6b, S6t |
| 3GPP (10415) | 5515 | DIAMETER_ERROR _SCEF_REFERENCE_ID_UNKNOWN | Sent by the SCEF to indicate that the SCEF reference ID is not known by the SCEF. | T6a/T6b |
| 3GPP (10415) | 5530 | DIAMETER_ERROR_INVALID_SME_ADDRESS | Sent by the SMS-SC to indicate that the SME address is invalid. | T4 |
| 3GPP (10415) | 5531 | DIAMETER_ERROR_SC_CONGESTION | Sent by the SMS-SC to indicate that SC is congested and unable to deliver the device trigger request. | T4 |
| 3GPP (10415) | 5532 | DIAMETER_ERROR_SM_PROTOCOL | Sent by the SMS-SC to indicate that there is an error with the protocol contained in the short message transfer protocol data unit. | T4 |
| 3GPP (10415) | 5533 | DIAMETER_ERROR_TRIGGER_REPLACE_FAILURE | Sent by the SMS-SC to indicate that trigger replace has failed to delete the old message and/or to store the new message. | T4 |
| 3GPP (10415) | 5534 | DIAMETER_ERROR_TRIGGER_RECALL_FAILURE | Sent by the SMS-SC to indicate that trigger recall has failed. | T4 |
| 3GPP (10415) | 5535 | DIAMETER_ERROR_ORIGINAL_MESSAGE_NOT_PENDING | Sent by the SMS-SC to indicate that trigger recall or replace has failed because the original message to be recalled or replaced is not stored any more in the SMS-SC. | T4 |
| 3GPP (10415) | 5550 | DIAMETER_ERROR_ABSENT_USER | Sent by the MME over the SGd interface or by the SGSN over the Gdd interface to indicate that the UE is not reachable. | S6c, SGd/Gdd |
| 3GPP (10415) | 5551 | DIAMETER_ERROR_USER_BUSY_FOR_MT_SMS | Sent by the MME or the SGSN when the user is busy for MT SMS. | S6c, SGd/Gdd |
| 3GPP (10415) | 5552 | DIAMETER_ERROR_FACILITY_NOT_SUPPORTED | Sent to indicate a requested facility is not supported. | S6c, SGd/Gdd |
| 3GPP (10415) | 5553 | DIAMETER_ERROR_ILLEGAL_USER | Sent by the MME or the SGSN to indicate that the delivery of the mobile terminated short message failed because the mobile station failed authentication. | S6c,SGd/Gdd |
| 3GPP (10415) | 5554 | DIAMETER_ERROR_ILLEGAL_EQUIPMENT | Sent by the MME or the SGSN to indicate that the delivery of the mobile terminated short message failed because an IMEI check failed, i.e. the IMEI was blacklisted or not white-listed. | S6c, SGd/Gdd |
| 3GPP (10415) | 5555 | DIAMETER_ERROR_SM_DELIVERY_FAILURE | Sent by the MME or the SGSN or the SMS-IWMSC to indicate that the delivery of the mobile terminated short message failed. | S6c, SGd/Gdd |
| 3GPP (10415) | 5556 | DIAMETER_ERROR_SERVICE_NOT_SUBSCRIBED | Sent by the HSS or the SMS Router over the S6c interface to indicate that the MT SMS Teleservice is not part of the subscription. | S6c, SGd/Gdd |
| 3GPP (10415) | 5557 | DIAMETER_ERROR_SERVICE_BARRED | Sent by the HSS or the SMS Router over the S6c interface to indicate that the MT SMS Teleservice is barred.Sent by the MME to indicate that the delivery of the mobile terminated short message failed because of the barring of the SMS service. | S6c, SGd/Gdd |
| 3GPP (10415) | 5558 | DIAMETER_ERROR_MWD_LIST_FULL | Sent by the HSS over the S6c interface to indicate that the Message Waiting List is full. | S6c, SGd/Gdd |
| 3GPP (10415) | 5570 | DIAMETER_ERROR_UNKNOWN_POLICY_COUNTERS | Used by the OCS to indicate to the PCRF that the OCS does not recognize one or more Policy Counters specified in the request, when the OCS is configured to reject the request provided with unknown policy counter identifier(s). | Sy |
| 3GPP (10415) | 5590 | DIAMETER_ERROR__ORIGIN_ALUID_UNKNOWN | Indicates that there is no valid context associated to the origin ALUID received in the request. | PC2 |
| 3GPP (10415) | 5591 | DIAMETER_ERROR_TARGET_ALUID_UNKNOWN | Indicates that there is no valid context associated to the target ALUID received in the request. | PC2 |
| 3GPP (10415) | 5592 | DIAMETER_ERROR_PFID_UNKNOWN | Indicates that there is no valid ProSe Function associate to the PFID received in the request. | PC2 |
| 3GPP (10415) | 5593 | DIAMETER_ERROR_APP_REGISTER_REJECT | Indicates that the ProSe Application Server cannot accept the application registration request for an unspecific reason. | PC2 |
| 3GPP (10415) | 5594 | DIAMETER_ERROR_PROSE_MAP_REQUEST_DISALLOWED | Indicates that the ProSe Application Server cannot accept the map request because the targeted application user is not allowed to be discovered by the originating application user. | PC2 |
| 3GPP (10415) | 5595 | DIAMETER_ERROR_MAP_REQUEST_REJECT | Indicates that the ProSe Application Server cannot accept the map request for an unspecific reason. | PC2 |
| 3GPP (10415) | 5596 | DIAMETER_ERROR_REQUESTING_RPAUID_UNKNOWN | Indicates that the ProSe Application Server cannot find the requesting RPAUID included in the restricted discovery authorization request. | PC2 |
| 3GPP (10415) | 5597 | DIAMETER_ERROR_UNKNOWN_OR_INVALID_TARGET_SET | Indicates that the ProSe Application Server cannot authorize the request because all target RPAUID(s) provided in the request are either unknown to the ProSe Application Server, or not eligible to be discovered by the requesting RPAUID. | PC2 |
| 3GPP (10415) | 5598 | DIAMETER_ERROR_MISSING_APPLICATION_DATA | Indicates that there is no application data provided so the ProSe Application Server cannot process the corresponding discovery request. | PC2 |
| 3GPP (10415) | 5599 | DIAMETER_ERROR_AUTHORIZATION_REJECT | Indicates that for the particular requesting type of restricted discovery, the ProSe Application Server found that the requesting RPAUID is not authorized. | PC2 |
| 3GPP (10415) | 5600 | DIAMETER_ERROR_DISCOVERY_NOT_PERMITTED | Indicates that the requesting RPAUID is not allowed to discover the target RPAUID. | PC2 |
| 3GPP (10415) | 5601 | DIAMETER_ERROR_TARGET_RPAUID_UNKNOWN | Indicates that there is no valid target RPAUID received in the request. | PC2 |
| 3GPP (10415) | 5602 | DIAMETER_ERROR_INVALID_APPLICATION_DATA | Indicates that there is invalid application data provided so the ProSe Application Server cannot process the corresponding discovery request. | PC2 |
| 3GPP (10415) | 5610 | DIAMETER_ERROR_UNKNOWN_PROSE_SUBSCRIPTION |  | PC4 |
| 3GPP (10415) | 5611 | DIAMETER_ERROR_PROSE_NOT_ALLOWED | Sent by the HSS to indicate that ProSe is not allowed to be used in the specific PLMN where the UE is registered. | PC4 |
| 3GPP (10415) | 5612 | DIAMETER_ERROR_UE_LOCATION_UNKNOWN | Sent by the HSS to indicate that the initial location of the UE is unknown. | PC4 |
| 3GPP (10415) | 5630 | DIAMETER_ERROR_NO_ASSOCIATED_DISCOVERY_FILTER | Sent by the ProSe Function in the local/visited PLMN to indicate that there is no valid Discovery Filter associated to the ProSe Application ID name received in the request. | PC6/PC7 |
| 3GPP (10415) | 5631 | DIAMETER_ERROR_ANNOUNCING_UNAUTHORIZED_IN_PLMN | Sent by the ProSe Function in the local/visited PLMN to indicate that the UE is not authorized to announce in this PLMN. | PC6/PC7 |
| 3GPP (10415) | 5632 | DIAMETER_ERROR_INVALID_APPLICATION_CODE | Sent by the ProSe Function in the local PLMN to indicate that none of the ProSe Application Code(s) received in the request is valid. | PC6/PC7 |
| 3GPP (10415) | 5633 | DIAMETER_ERROR_PROXIMITY_UNAUTHORIZED | Sent by the ProSe Function in the serving PLMN to indicate that the Proximity request is not authorized by the user. | PC6/PC7 |
| 3GPP (10415) | 5634 | DIAMETER_ERROR_PROXIMITY_REJECTED | Sent by the ProSe Function in the serving PLMN to indicate that it is unlikely that UEs enter into proximity for the received time window. | PC6/PC7 |
| 3GPP (10415) | 5635 | DIAMETER_ERROR_NO_PROXIMITY_REQUEST | Sent by the ProSe Function in the serving PLMN to indicate that there is no context associated with EPC ProSe User Identities included in the request. | PC6/PC7 |
| 3GPP (10415) | 5636 | DIAMETER_ERROR_UNAUTHORIZED_SERVICE_IN_THIS_PLMN | Sent by the ProSe Function HPLMN to indicate that ProSe is not authorized to announce in this PLMN. | PC6/PC7 |
| 3GPP (10415) | 5637 | DIAMETER_ERROR_PROXIMITY_CANCELLED | Sent by the ProSe Function triggering the Proximity Request to indicate that the cancellation of the Proximity Request procedure as it determines that the UEs are unlikely to enter proximity within the requested time window. | PC6/PC7 |
| 3GPP (10415) | 5638 | DIAMETER_ERROR_INVALID_TARGET_PDUID | Sent by the ProSe Function to indicate that the PDUID is not belonging to this ProSe Function. | PC6/PC7 |
| 3GPP (10415) | 5639 | DIAMETER_ERROR_INVALID_TARGET_RPAUID | Sent by the ProSe Function to indicate that the target RPAUID is not allowed to be discovered. | PC6/PC7 |
| 3GPP (10415) | 5640 | DIAMETER_ERROR_NO_ASSOCIATED_RESTRICTED_CODE | Sent by the ProSe Function to indicate that the there is no allocated ProSe Restricted Code for the requested target. | PC6/PC7 |
| 3GPP (10415) | 5641 | DIAMETER_ERROR_INVALID_DISCOVERY_TYPE | Sent by the ProSe Function to indicate that the Discovery Tpe in the request message is not supported. | PC6/PC7 |
| 3GPP (10415) | 56×1 | DIAMETER_ERROR_REVOCATION_FAILURE | Indicates that the ProSe Function was unable to successfully complete the revocation procedure (in an operator-configured time period). | PC6/PC7 |
| 3GPP (10415) | 56×2 | DIAMETER_ERROR_ALREADY_BANNED | Indicates that the ProSe Function has determined that the revocation is not necessary because the target RPAUID has already been banned for discovery. | PC6/PC7 |
| 3GPP (10415) | 5650 | DIAMETER_ERROR_REQUESTED_LOCATION_NOT_SERVED | Sent sent by the MME/SGSN to indicate that the location for which a related monitoring event is configured (e.g. Number of UEs at a given geographical location) by the SCEF, is not served by the MME/SGSN. | T6a/T6b |
| 3GPP (10415) | 5651 | DIAMETER_ERROR_INVALID_EPS_BEARER | Sent by the SCEF or the MME to indicate that there is no existing EPS bearer context for the user. | T6a/T6b |
| 3GPP (10415) | 5652 | DIAMETER_ERROR_NIDD_CONFIGURATION_NOT_AVAILABLE | Sent by the SCEF to indicate that there is no valid NIDD configuration available. | T6a/T6b |
| 3GPP (10415) | 5653 | DIAMETER_ERROR_USER_TEMPORARILY_UNREACHABLE | Sent by the MME or SGSN to indicate that the UE is temporarily not reachable due to a power saving function, and that the MME or SGSN will update the SCEF when it detects that the UE is reachable or about to become reachable. | T6a/T6b |
| 3GPP (10415) | 5670 | DIAMETER_ERROR_UNKNKOWN_DATA | The requested data received by the MC Service User Database does not exist | TS 29.283 |
| 3GPP (10415) | 5671 | DIAMETER_ERROR_REQUIRED_KEY_NOT_PROVIDED | One or more access keys are missing in the request to be able to update the requested data | TS 29.283 |
| 3GPP (10415) | 5690 | DIAMETER_ERROR_UNKNOWN_V2X_SUBSCRIPTION |  | TS 29.368 |
| 3GPP (10415) | 5691 | DIAMETER_ERROR_V2X_NOT_ALLOWED |  | TS 29.368 |

Note. For stateful interfaces, counted as a successful transaction upon successful termination (result Code 2001 received in termination response) of session. For stateless interfaces, counted as a successful transaction when response received with 2001 result code.

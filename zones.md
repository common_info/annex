| Zone | Products |
| :--- | :------- |
| 192.168.126.186 | Protei_IPSMGW, Protei_SMPP_BS, Protei_OTA_SMS, Protei_SMPP_PROXY, Protei_RBC, Protei_SMSC, Protei_RG_REG, Protei_SMSC_2, Protei_SCL, Protei_SMSFW, Protei_SCL_GIS, Protei_STP, Protei_SCP, Protei_CALLME, Protei_DRA |
| 192.168.126.3 | GLR_API, Protei_ITG, Protei_STP2, HLR_Load, Protei_MV_RADIUS, Protei_UDM, HSS_API, Protei_OTA_SMS, SB, Protei_RDS, Protei_RG, MI_API, Protei_SCL, Protei_CAPL, Protei_SC_Lite, Protei_DRA, Protei_SRF, Protei_GMSC, Protei_SS7FW, Protei_HSS |
| 149.255.118.70 | PCSCF, ICSCF, SCSCF, MKD, SBC, SSW4, TAS |
| 192.168.110.189 | Protei_SB, Protei_EIR |
| 192.168.126.153 | Protei_CBC, Protei_SRF, Protei_GLR, Protei_GLR_BS, Protei_HLR, Protei_HLR_API, Protei_HLR_BS, Protei_SigtranTester |
| 192.168.77.54 | PROTEI SS7FW |
| 192.168.110.19:8080/ss7fw | PROTEI SS7FW Web |
| 192.168.110.19 | PROTEI SMSFW |
| 192.168.126.76 | PROTEI_MIMSI_DB, mysql -u mimsi -p sql |
| 192.168.110.116 | SCL, Protei_CPE_SB, Protei_DIAMETER_BS, Sender_XVLR, Protei-SB.core, SigMonitor, Protei_SMPP_PROXY, Protei_SN.core |
| 192.168.108.253 | VO |
| 192.168.110.234 | SN2 |
| 192.168.108.168 | Protei_SPL_SIP, Protei_TRACER, Protei_CAPL, data_proc.sb, Protei_CPE_SB, SigTester_SG, Protei_CPE_SB_HTTP2.0, Protei_SB_CORE, Protei_SG, Protei_SG_DNS, Protei_SG_GTP, Protei_SIGMON, root/elephant |
| 192.168.126.245 | Protei_GGSN_TESTER, Protei_SGW, Protei_UDSF |
| 192.168.110.140 | PROTEI_HLR, PROTEI_HLR_API |
| 192.168.0.131 | git.protei.ru |
| 192.168.100.156 | svn.protei.ru |
| 192.168.108.253 | PROTEI_VO_EQUANT |
| 192.168.126.21 | Billing OCS |
| 192.168.99.181 | mGate.ITG.cli |
| 192.168.126.158/GTOm/login.jsp | ЕЦОВ |
| 192.168.126.37:8080/sb | Protei_SB Web |
| 192.168.126.7:8080/SB_safari | Protei_SB Web |
| 192.168.125.148 | Protei_RBT.CORE, Protei_RBT_WEB, Protei_SMPP_PROXY, Protei_SPL_SIP, Protei_CPE.PRBT, Protei_DPDP.CORE, Protei_DPDP_WEB, Protei_MEDIA_SERVER, Protei_MIG.TOOL, root/elephant |
| 192.168.125.148:8081 | DPDP Web |
| 192.168.125.148:8080/rbt | RBT Web |
| 192.168.125.26 | Protei_GGSN_TESTER, Protei_SGW, Protei_UDSF1, Protei_UDSF2, Protei_UDSF3 |
| 192.168.44.116 | PROTEI_SMSFW Web, support/elephant |
| 172.30.137.234:10018 | PROTEI DPDP Web, admin/sql |

Data Types in English

| Type | Description |
| :--- | :---------- |
| bool | Boolean type. It sets a flag and takes on values false and true. |
| bool_int | Boolean type. It sets a flag and takes on values 0 and 1. |
| datetime | It sets a date and time. Used notations:<br>YY/YYYY: year, spelt out in two/four digits, respectively;<br>MM: month, spelt out in two digits;<br>DD: day, spelt out in two digits;<br>hh: hours, spelt out in two digits;<br>mm: minutes, spelt out in two digits;<br>ss: seconds, spelt out in two digits;<br>mss: milliseconds, spelt out in three digits.<br>Time has a 24-hour format. |
| hex | Numeric type. It sets an integer in a hexadecimal format spelt out in the 0–9 digits and the A–F Latin letters. The number may have the prefix of 0x. It is specified as a string if the designation is absent. |
| int | Numeric type. It sets a 32-bit integer spelt out in the 0-9 digits and the "–" minus sign. Range: from –2 ** 31 to 2 ** 31 – 1. |
| list | It sets a sequence that contains several values with the same type or structure. |
| object | It sets a tuple that has a fixed number of parameters with the different types. |
| string | String type. It may contain the Latin letters, the 0–9 digits, special symbols, and punctuation marks. |
| ip | String type. It sets an IP  a format of the IPv4 address: xxx.xxx.xxx.xxx |
| regex | String type. It sets a regular expression/mask/template for the data values. |
| double | Numeric type. It sets a 64-bit real number spelt out in the 0–9 digits, the "–" minus sign, and the "." dot separator sign. Range: from –308 to 308 exponent part. |

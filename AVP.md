# Diameter AVP Codes #

Link to the 3GPP standard: <a-href="https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.03.00_60/ts_129272v170300p.pdf">3GPP TS 29.272</a>

User-Identity 700 6.3.1 Grouped M, V No
MSISDN 701 6.3.2 OctetString M, V No
User-Data 702 6.3.3 OctetString M, V No
Data-Reference 703 6.3.4 Enumerated M, V No
Service-Indication 704 6.3.5 OctetString M, V No
Subs-Req-Type 705 6.3.6 Enumerated M, V No
Requested-Domain 706 6.3.7 Enumerated M, V No
Current-Location 707 6.3.8 Enumerated M, V No
Identity-Set 708 6.3.10 Enumerated V M No
Expiry-Time 709 6.3.16 Time V M No
Send-Data-Indication 710 6.3.17 Enumerated V M No
Server-Name 602 6.3.9 UTF8String M, V No
Supported-Features 628 6.3.11 Grouped V M No
Feature-List-ID 629 6.3.12 Unsigned32 V M No
Feature-List 630 6.3.13 Unsigned32 V M No
Supported-Applications 631 6.3.14 Grouped V M No
Public-Identity 601 6.3.15 UTF8String M, V No
DSAI-Tag 711 6.3.18 OctetString M, V No
Wildcarded-Public-Identity 634 6.3.19 UTF8String V M No
Wildcarded-IMPU 636 6.3.20 UTF8String V M No
Session-Priority 650 6.3.21 Enumerated V M No
One-Time-Notification 712 6.3.22 Enumerated V M No
Requested-Nodes 713 6.3.7A Unsigned32 V M No
Serving-Node-Indication 714 6.3.23 Enumerated V M No
Repository-Data-ID 715 6.3.24 Grouped V M No
Sequence-Number 716 6.3.25 Unsigned32 V M No
Pre-paging-Supported 717 6.3.26 Enumerated V M No
Local-Time-Zone-Indication 718 6.3.27 Enumerated V M No
UDR-Flags 719 6.3.28 Unsigned32 V M No
Call-Reference-Info 720 6.3.29 Grouped V M No
Call-Reference-Number 721 6.3.30 OctetString V M No
AS-Number 722 6.3.31 OctetString V M No
OC-Supported-Features 621
NOTE
3
6.3.32 Grouped M, V No
OC-OLR 623
NOTE
3
6.3.33 Grouped M, V No
DRMP 301
NOTE
4
6.3.34 Enumerated M, V No
Load NOTE
5
6.3.35 Grouped M, V No
NOTE 1: The AVP header bit denoted as 'M', indicates whether support of the AVP is required. The AVP header
bit denoted as 'V', indicates whether the optional Vendor-ID field is present in the AVP header.
For further details, see 3GPP TS 29.229 [6].
NOTE 2: If the M-bit is set for an AVP and the receiver does not understand the AVP, it shall return a rejection. If
the M-bit is not set for an AVP, the receiver shall not return a rejection, whether or not it understands the
AVP. If the receiver understands the AVP but the M-bit value does not match with the definition in this
table, the receiver shall ignore the M-bit.
NOTE 3: The value of these attributes is defined in IETF RFC 7683 [12].
NOTE 4: The value of this attribute is defined in IETF RFC 7944 [13].
NOTE 5: The value of this attribute is defined in IETF RFC 8583 [14].


User-Identity AVP
The User-Identity AVP is of type Grouped. This AVP contains either a Public- Identity AVP or an MSISDN AVP or an External-Identifier AVP.
AVP format
User-Identity ::= <AVP header: 700 10415>
[Public-Identity]
[MSISDN]
[External-Identifier]
*[AVP]
MSISDN AVP
The MSISDN AVP is of type OctetString. This AVP contains an MSISDN, in international number format as described in ITU-T Rec E.164 [8], encoded as a TBCD-string, i.e. digits from 0 through 9 are encoded 0000 to 1001; 1111 is used
as a filler when there is an odd number of digits; bits 8 to 5 of octet n encode digit 2n; bits 4 to 1 of octet n encode digit 2(n-1)+1.
User-Data AVP
The User-Data AVP is of type OctetString. This AVP contains the user data requested in the UDR/UDA, SNR/SNA and PNR/PNA operations and the data to be modified in the PUR/PUA operation. The exact content and format of this
AVP is described in 3GPP TS 29.328 [1] Annex C as Sh-Data.
Data-Reference AVP
The Data-Reference AVP is of type Enumerated, and indicates the type of the requested user data in the operation UDR and SNR. Its exact values and meaning is defined in 3GPP TS 29.328 [1]. The following values are defined (more
details are given in 3GPP TS 29.328 [1]):
RepositoryData (0)
IMSPublicIdentity (10)
IMSUserState (11)
S-CSCFName (12)
InitialFilterCriteria (13)
This value is used to request initial filter criteria relevant to the requesting AS
LocationInformation (14)
UserState (15)
ChargingInformation (16)
MSISDN (17)
PSIActivation (18)
DSAI (19)
ServiceLevelTraceInfo (21)
IPAddressSecureBindingInformation (22)
ServicePriorityLevel (23)
SMSRegistrationInfo (24)
UEReachabilityForIP (25)
TADSinformation (26)
STN-SR (27)
UE-SRVCC-Capability (28)
ExtendedPriority (29)
CSRN (30)
ReferenceLocationInformation (31)
IMSI (32)
IMSPrivateUserIdentity (33)
IMEISV (34)
UE-5G-SRVCC-Capability (35)
NOTE: Value 20 is reserved.

Service-Indication AVP
The Service-Indication AVP is of type OctetString. This AVP contains the Service Indication that identifies a service or a set of services in an AS and the related repository data in the HSS. Standardized values of Service-Indication identifying a standardized service or set of services in the AS and standardized format of the related repository data are defined in 3GPP TS 29.364 [10].
Subs-Req-Type AVP
The Subs-Req-Type AVP is of type Enumerated, and indicates the type of the subscription-to-notifications request. The following values are defined:
Subscribe (0)
This value is used by an AS to subscribe to notifications of changes in data.
Unsubscribe (1)
This value is used by an AS to unsubscribe to notifications of changes in data.
Requested-Domain AVP
The Requested-Domain AVP is of type Enumerated, and indicates the access domain for which certain data (e.g. user state) are requested. The following values are defined:
CS-Domain (0)
The requested data apply to the CS domain.
PS-Domain (1)
The requested data apply to the PS domain.
6.3.7A Requested-Nodes AVP
The Requested-Nodes AVP is of type Unsigned32 and it shall contain a bit mask. The meaning of the bits shall be as defined in table 6.3.7A/1:
Table 6.3.7A/1: Requested-Nodes
Bit Name Description
0 MME The requested data apply to the MME
1 SGSN The requested data apply to the SGSN
2 3GPP-AAA-SERVER-TWAN
The requested data apply to the 3GPP AAA Server for TWAN
3 AMF The requested data apply to the AMF (for 3GPP access)
Current-Location AVP
The Current-Location AVP is of type Enumerated, and indicates whether an active location retrieval has to be initiated or not:
DoNotNeedInitiateActiveLocationRetrieval (0)
The request indicates that the initiation of an active location retrieval is not required.
InitiateActiveLocationRetrieval (1)
It is requested that an active location retrieval is initiated.
Server-Name AVP
The Server-Name contains a SIP-URL used to identify an AS. See 3GPP TS 29.229 [6] for further description of this AVP.
Identity-Set AVP
The Identity-Set AVP is of type Enumerated and indicates the requested set of IMS Public Identities. The following values are defined:
ALL_IDENTITIES (0)
REGISTERED_IDENTITIES (1)
IMPLICIT_IDENTITIES (2)
ALIAS_IDENTITIES (3)
Supported-Features AVP
See 3GPP TS 29.229 [6] clause 6.3.29.
Feature-List-ID AVP
See 3GPP TS 29.229 [6] clause 6.3.30.
Feature-List AVP
See 3GPP TS 29.229 [6] clause 6.3.31.
Supported-Applications AVP
See 3GPP TS 29.229 [6] clause 6.3.32.
Public-Identity AVP
The Public-Identity AVP contains a Public User Identity. See 3GPP TS 29.229 [6] for the definition of this AVP.
Expiry-Time AVP
The Expiry-Time AVP is of type Time. This AVP contains the expiry time of subscriptions to notifications in the HSS.
Send-Data-Indication AVP
The Send-Data-Indication AVP is of type Enumerated. If present it indicates that the sender requests the User-Data. The following values are defined:
USER_DATA_NOT_REQUESTED (0)
USER_DATA_REQUESTED (1)
DSAI-Tag AVP
The DSAI-Tag AVP is of type OctetString. This AVP contains the DSAI-Tag identifying the instance of the Dynamic Service Activation Information being accessed for the Public Identity.
Wildcarded-Public-Identity AVP
See 3GPP TS 29.229 [6] clause 6.3.35 for the definition of theWildcarded-Public-Identity AVP. This AVP only contains a Wildcarded PSI over Sh interface.
Wildcarded-IMPU AVP
See 3GPP TS 29.229 [6] clause 6.3.43.
Session-Priority AVP
See 3GPP TS 29.229 [6] clause 6.3.56.
One-Time-Notification AVP
The One-Time-Notification AVP is of type Enumerated. If present it indicates that the sender requests to be notified only one time. The following values are defined:
ONE_TIME_NOTIFICATION_REQUESTED (0)
This AVP is only applicable to UE reachability for IP (25)

Serving-Node-Indication AVP
The Serving-Node-Indication AVP is of type Enumerated. If present it indicates that the sender does not require any location information other than the Serving Node Addresses/Identities requested (e.g. MME name, VLR number). Other location information (e.g. Global Cell ID, Tracking Area ID) may be absent. The following values are defined:
ONLY_SERVING_NODES_REQUIRED (0)
Repository-Data-ID AVP
The Repository-Data-ID AVP is of type Grouped. This AVP shall contain a Service-Indication AVP and a Sequence-Number AVP.
AVP format
Repository-Data-ID ::= <AVP header: 715 10415>
{Service-Indication}
{Sequence-Number}
*[AVP]
Sequence-Number AVP
The Sequence-Number AVP is of type Unsigned32. This AVP contains a number associated to a repository data.
Pre-paging-Supported AVP
The Pre-paging-Supported AVP is of type Enumerated. It indicates whether the sender supports pre-paging or not. The following values are defined:
PREPAGING_NOT_SUPPORTED (0)
PREPAGING_SUPPORTED (1)
If this AVP is not present in the command, the default value is PREPAGING_NOT_SUPPORTED (0).
Local-Time-Zone-Indication AVP
The Local-Time-Zone-Indication AVP is of type Enumerated. If present it indicates that the Local Time Zone information (time zone and daylight saving time) of the visited network where the UE is attached is requested with or without other location information. The following values are defined:
ONLY_LOCAL_TIME_ZONE_REQUESTED (0)
LOCAL_TIME_ZONE_WITH_LOCATION_INFO_REQUESTED (1)
UDR-Flags
The UDR-Flags AVP is of type Unsigned32 and it shall contain a bit mask. The meaning of the bits shall be as defined in 3GPP TS 29.328 [1].
Table 6.3.28/1: UDR-Flags
Bit Name
0 Location-Information-EPS-Supported
1 RAT-Type-Requested
NOTE: Bits not defined in this table shall be cleared by the sender of the request and discarded by the receiver of the request.

Call-Reference-Info AVP
The Call-Reference-Info AVP is of type Grouped. This AVP shall contain a Call-Reference-Number AVP and an AS-Number AVP.
AVP format
Call-Reference-Info ::= <AVP header: 720 10415>
{Call-Reference-Number}
{AS-Number}
*[AVP]
Call-Reference-Number AVP
The Call-Reference-Number AVP is of type OctetString. The exact content and format of this AVP is described in 3GPP TS 29.002 [11].
AS-Number AVP
The AS-Number AVP is of type OctetString. The exact content and format of this AVP corresponds to the gmsc-address parameter described in 3GPP TS 29.002 [11].
OC-Supported-Features
The OC-Supported-Features AVP is of type Grouped and it is defined in IETF RFC 7683 [12]. This AVP is used to support Diameter overload control mechanism.
OC-OLR
The OC-OLR AVP is of type Grouped and it is defined in IETF RFC 7683 [12]. This AVP is used to support Diameter overload control mechanism.
DRMP AVP
The DRMP AVP is of type Enumerated and it is defined in IETF RFC 7944 [13]. This AVP allows the HSS/SLF and the AS/OSA SCS to indicate the relative priority of Diameter messages. The DRMP AVP may be used to set the DSCP marking for transport of the associated Diameter message.
Load
The Load AVP is of type Grouped and it is defined in IETF RFC 8583 [14]. This AVP is used to support the Diameter load control mechanism.

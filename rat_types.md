# RAT Types #

Source: [3GPP TS 29.212](https://www.etsi.org/deliver/etsi_ts/129200_129299/129212/17.02.00_60/ts_129212v170200p.pdf).

AVP: 1032.

* 1 - UTRAN;
* 2 - GERAN;
* 3 - WLAN;
* 4 - GAN;
* 5 - HSPA Evolution;
* 6 - EUTRAN (WB\-E\-UTRAN);
* 7 - Virtual;
* 8 - E\-UTRAN\-NB\-IoT;


Values `0-999` are used for generic radio access technologies that can apply to different IP\-CAN types and are not IP\-CAN specific.
Values `1000-1999` are used for 3GPP specific radio access technology types.
Values `2000-2999` are used for 3GPP2 specific radio access technology types.
NOTE 4: The informative Annex C presents a mapping between the code values for different access network types.

The following values are defined:
* WLAN (0) --- This value shall be used to indicate that the RAT is WLAN.
* VIRTUAL (1) --- This value shall be used to indicate that the RAT is unknown. For further details refer to 3GPP TS 29.274 [22].
* TRUSTED- N3GA (2) --- This value shall be used to indicate that the RAT is a trusted non-3GPP access, different than Trusted Wireless LAN (IEEE 802.11) access.
This value is not used in the present specification.
* WIRELINE (3) --- This value shall be used to indicate that the transmission technology is wireline access. It is used when it is not
possible to differentiate between wireline cable and wireline BBF.
This value is not used in the present specification.
* WIRELINE-CABLE (4) --- This value shall be used to indicate that the transmission technology is wireline cable.
This value is not used in the present specification.
* WIRELINE-BBF (5) --- This value shall be used to indicate that the transmission technology is wireline BBF.
This value is not used in the present specification.
* UTRAN (1000) --- This value shall be used to indicate that the RAT is UTRAN. For further details refer to 3GPP TS 29.060 [18].
* GERAN (1001) --- This value shall be used to indicate that the RAT is GERAN. For further details refer to 3GPP TS 29.060 [18].
* GAN (1002) --- This value shall be used to indicate that the RAT is GAN. For further details refer to 3GPP TS 29.060 [18] and 3GPP TS 43.318 [29].
* HSPA_EVOLUTION (1003) --- This value shall be used to indicate that the RAT is HSPA Evolution. For further details refer to 3GPP TS 29.060 [18].
* EUTRAN (1004) --- This value shall be used to indicate that the RAT is EUTRAN (WB-EUTRAN) terrestrial RAT type. For further details refer to 3GPP TS 29.274 [22].
* EUTRAN-NB-IoT (1005) --- This value shall be used to indicate that the RAT is NB-IoT. For further details refer to 3GPP TS 29.274 [22].
* NR (1006) --- This value shall be used to indicate that the RAT is NR.
This value is not used in the present specification.
* LTE-M (1007) --- This value shall be used to indicate that the RAT is LTE-M. For further details refer to 3GPP TS 29.274 [22].
* NR-U (1008) --- This value shall be used to indicate that the RAT is NR in unlicensed bands.
This value is not used in the present specification.
* EUTRAN(LEO) (1011) --- This value shall be used to indicate that the RAT is WB-EUTRAN(LEO). For further details refer to 3GPP TS 29.274 [22].
* EUTRAN(MEO) (1012) --- This value shall be used to indicate that the RAT is WB-EUTRAN(MEO). For further details refer to 3GPP TS 29.274 [22].
* EUTRAN(GEO) (1013) --- This value shall be used to indicate that the RAT is WB-EUTRAN(GEO). For further details refer to 3GPP TS 29.274 [22].
* EUTRAN(OTHERSAT) (1014) --- This value shall be used to indicate that the RAT is WB-EUTRAN(OTHERSAT). For further details refer to 3GPP TS 29.274 [22].
* EUTRAN-NB-IoT(LEO) (1021) --- This value shall be used to indicate that the RAT is NB-IoT(LEO). For further details refer to 3GPP TS 29.274 [22].
* EUTRAN-NB-IoT(MEO) (1022) --- This value shall be used to indicate that the RAT is NB-IoT(MEO). For further details refer to 3GPP TS 29.274 [22].
* EUTRAN-NB-IoT(GEO) (1023) --- This value shall be used to indicate that the RAT is NB-IoT(GEO). For further details refer to
3GPP TS 29.274 [22].
* EUTRAN-NB-IoT(OTHERSAT) (1024) --- This value shall be used to indicate that the RAT is NB-IoT(OTHERSAT). For further details refer to 3GPP TS 29.274 [22].
* LTE-M(LEO) (1031) --- This value shall be used to indicate that the RAT is LTE-M(LEO). For further details refer to 3GPP TS 29.274 [22].
* LTE-M(MEO) (1032) --- This value shall be used to indicate that the RAT is LTE-M(MEO). For further details refer to 3GPP TS 29.274 [22].
* LTE-M(GEO) (1033) --- This value shall be used to indicate that the RAT is LTE-M(GEO). For further details refer to 3GPP TS 29.274 [22].
* LTE-M(OTHERSAT) (1034) --- This value shall be used to indicate that the RAT is LTE-M(OTHERSAT). For further details refer to 3GPP TS 29.274 [22].
* CDMA2000_1X (2000) --- This value shall be used to indicate that the RAT is CDMA2000 1X. For further details refer to 3GPP2 X.S0011 [20].
* HRPD (2001) --- This value shall be used to indicate that the RAT is HRPD. For further details refer to 3GPP2 X.S0011 [20].
* UMB (2002) --- This value shall be used to indicate that the RAT is UMB. For further details refer to 3GPP2 X.S0011 [20].
* EHRPD (2003) --- This value shall be used to indicate that the RAT is eHRPD. For further details refer to 3GPP2 X.S0057 [24].


Access Technology Type
registered with IANA, see
TS 29.275 [28]
PCC related RAT-Type, see
clause 5.3.31
RAT-Type specified for
GTPv2, see
TS 29.274 [22]
IP-CAN-Type, see
clause 5.3.27
(NOTE 1 and 2)
Value Description Value Description Value Description Value Description
0 Reserved 0 <reserved>
1 Virtual 1 VIRTUAL 7 Virtual 6 Non-3GPP-EPS
2 PPP
3 IEEE 802.3
4 IEEE 802.11a/b/g 0 WLAN 3 WLAN 6 Non-3GPP-EPS
5 IEEE 802.16e 6
3
Non-3GPP-EPS
WiMAX
6 3GPP GERAN 1001 GERAN 2 GERAN 0 3GPP-GPRS
5 3GPP-EPS
7 3GPP UTRAN 1000 UTRAN 1 UTRAN 0 3GPP-GPRS
5 3GPP-EPS
8 3GPP E-UTRAN 1004 EUTRAN (NOTE 3) 6 EUTRAN (WB-E-
UTRAN)
5 3GPP-EPS
9 3GPP2 eHRPD 2003 EHRPD 6 Non-3GPP-EPS
10 3GPP2 HRPD 2001 HRPD 4 3GPP2
11 3GPP2 1xRTT 2000 CDMA2000_1X 4 3GPP2
12 3GPP2 UMB 2002 UMB 4 3GPP2
13 3GPP NB-IOT 1005 EUTRAN-NB-IoT 8 EUTRAN-NB-IoT 5 3GPP-EPS
14-255 Unassigned
1002 GAN 4 GAN 0 3GPP-GPRS
5 3GPP-EPS
1003 HSPA_EVOLUTION 5 HSPA Evolution 0 3GPP-GPRS
5 3GPP-EPS
1 DOCSIS

![RAT types](./images/rat_types.png)

# Сетевые функции 5G #

## Названия сетевых функций ##

| NF name | Full name | Standard |
| :------ | :-------- | :------- |
| nnrf-nfm | Nnrf_NFManagement | 3GPP TS 29.510 |
| nnrf-disc | Nnrf_NFDiscovery | 3GPP TS 29.510 |
| nudm-sdm | Nudm_SubscriberDataManagement | 3GPP TS 29.503 |
| nudm-uecm | Nudm_UEContextManagement | 3GPP TS 29.503 |
| nudm-ueau | Nudm_UEAuthentication | 3GPP TS 29.503 |
| nudm-ee | Nudm_EventExposure | 3GPP TS 29.503 |
| nudm-pp | Nudm_ParameterProvision | 3GPP TS 29.503 |
| namf-comm | Namf_Communication | 3GPP TS 29.518 |
| namf-evts | Namf_EventExposure | 3GPP TS 29.518 |
| namf-mt | Namf_MT | 3GPP TS 29.518 |
| namf-loc | Namf_Location | 3GPP TS 29.518 |
| nsmf-pdusession | Nsmf_PDUSession | 3GPP TS 29.502 |
| nsmf-event-exposure | Nsmf_EventExposure | 3GPP TS 29.502 |
| nausf-auth | Nausf_UEAuthentication | 3GPP TS 29.509 |
| nausf-sorprotection | Nausf_SoRProtection | 3GPP TS 29.509 |
| nausf-upuprotection | Nausf_UPUProtection | 3GPP TS 29.509 |
| nnef-pfdmanagement | Nnef_PFDManagement | 3GPP TS 29.551 |
| npcf-am-policy-control | Npcf_AMPolicyControl | 3GPP TS 29.507 |
| npcf-smpolicycontrol | Npcf_SMPolicyControl | 3GPP TS 29.507 |
| npcf-policyauthorization | Npcf_PolicyAuthorization | 3GPP TS 29.507 |
| npcf-bdtpolicycontrol | Npcf_BDTPolicyControl | 3GPP TS 29.507 |
| npcf-eventexposure | Npcf_EventExposure | 3GPP TS 29.507 |
| npcf-ue-policy-control | Npcf_UEPolicyControl | 3GPP TS 29.507 |
| nsmsf-sms | Nsmsf_SMService | 3GPP TS 29.540 |
| nnssf-nsselection | Nnssf_NSSelection | 3GPP TS 29.531 |
| nnssf-nssaiavailability | Nnssf_NSSAIAvailability | 3GPP TS 29.531 |
| nudr-dr | Nudr_DataRepository | 3GPP TS 29.504 |
| nlmf-loc | Nlmf_Location | 3GPP TS 29.572 |
| n5g-eir-eic | N5g-eir_EquipmentIdentityCheck | 3GPP TS 29.511 |
| nbsf-management | Nbsf_Management | 3GPP TS 29.521 |
| nchf-spendinglimitcontrol | Nchf_SpendingLimitControl | 3GPP TS 29.594 |
| nchf-convergedcharging | Nchf_Converged_Charging | 3GPP TS 29.594 |
| nnwdaf-eventssubscription | Nnwdaf_EventsSubscription | 3GPP TS 29.520 |
| nnwdaf-analyticsinfo | Nnwdaf_AnalyticsInfo | 3GPP TS 29.520 |

## Типы сетевых функций ##

| Тип NF | Полное название | Описание |
| :----- | :-------------- | :------- |
| NRF | NF Repository Function | хранилище сетевых функций |
| UDM | Unified Data Manager Function | функция управления собранными данными |
| AMF | Access and Mobility Management Function | функция управления доступом и мобильностью |
| SMF | Session Management Function | функция управления сессиями |
| AUSF | Authentication Server Function | функция сервера аутентификации |
| NEF | Network Exposure Function | функция взаимодействия с внешними приложениями |
| PCF | Policy Control Function | функция управления политиками |
| SMSF | SMS Function | функция поддержки обмена короткими текстовыми сообщениями посредством протокола NAS |
| NSSF | Network Slice Selection Function | функция выбора сетевого слоя |
| UDR | Unified Data Repository | унифицированная база данных |
| LMF | Location Management Function | функция управления местоположением |
| GMLC | Gateway Mobile Location Centre | шлюзовой центр позиционирования |
| 5G_EIR | Equipment Identity Register | регистр идентификаторов оборудования |
| SEPP | Security Edge Protection Proxy | пограничная сетевая функция |
| UPF | User Plane Function | функция передачи данных пользователей |
| N3IWF | Non-3GPP InterWorking Function | функция взаимодействия с не-3GPP сетью |
| AF | Application Function | прикладная функция |
| UDSF | Unstructured Data Storage Function | функция хранения неструктурированных данных |
| BSF | Binding Support Function | функция поддержки привязки |
| CHF | Charging Function | функция тарификации |
| NWDAF | Network Data Analytics Function | функция анализа сетевых данных |

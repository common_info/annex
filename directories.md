# Директории внутри папки проекта#

| Директория | Описание |
| :--------- | :------- |
| `/usr/protei/{project_name}/archive/` | Директория для архивных файлов журналов |
| `/usr/protei/{project_name}/bin/` | Директория для исполняемых файлов |
| `/usr/protei/{project_name}/cdr/` | Директория для журналов CDR |
| `/usr/protei/{project_name}/conf/` | Директория для общих файлов настройки сервера Tomcat |
| `/usr/protei/{project_name}/config/` | Директория для конфигурационных файлов |
| `/usr/protei/{project_name}/data/` | Директория для записей разговоров |
| `/usr/protei/{project_name}/db/` | Директория для настройки параметров базы данных |
| `/usr/protei/{project_name}/history/` | Директория для архивных лог–файлов |
| `/usr/protei/{project_name}/jar/` | Директория для архивированных файлов с классами Java |
| `/usr/protei/{project_name}/lib/` | Директория для сторонних подключаемых библиотек |
| `/usr/protei/{project_name}/logs/` | Директория для журналов |
| `/usr/protei/{project_name}/scripts/` | Директория для новых подсказок до обработки к требуемому формату файлов голосовых подсказок |
| `/usr/protei/{project_name}/stat/` | Директория для файлов статистики |
| `/usr/protei/{project_name}/system/` | Директория для конфигураций по умолчанию |
| `/usr/protei/{project_name}/temp/` | Директория для временных файлов |
| `/usr/protei/{project_name}/utils/` | Директория для запускаемых скриптов, реализующих основной функционал скриптовой оболочки |
| `/usr/protei/{project_name}/webapps/` | Директория для файлов Web–интерфейса |

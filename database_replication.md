# Репликация базы данных #

## Репликация Master-Master ##

1. Добавить следующие настройки в `my.cnf` (`/etc/my.cnf`) на первом сервере:

        [mariadb]
        server_id = 1
        log_bin = /var/lib/mysql/mysql-bin.log
        binlog_do_db = smsfw_ui
        bind-address = 10.200.110.137

2. Перезапустить MySQL:

        systemctl restart mariadb

3. Добавить пользователя и его привилегии:

        create user 'replicator'@'%' identified by 'sql';
        grant replication slave on *.* to 'replicator'@'%'; 

4. Проверить статус репликации:

        show master status;

        +------------------+----------+--------------+------------------+
        | File             | Position | Binlog_Do_DB | Binlog_Ignore_DB |
        +------------------+----------+--------------+------------------+
        | mysql-bin.000001 |      328 | smsfw_ui     |                  |
        +------------------+----------+--------------+------------------+

5. Повторить шаги 1–3 на втором сервере:

        [mariadb]
        server_id = 2
        log_bin = /var/lib/mysql/mysql-bin.log
        binlog_do_db = smsfw_ui
        bind-address = 10.200.110.138

        systemctl restart mariadb

        create user 'replicator'@'%' identified by 'sql';
        grant replication slave on *.* to 'replicator'@'%';

        show master status;

        +------------------+----------+--------------+------------------+
        | File             | Position | Binlog_Do_DB | Binlog_Ignore_DB |
        +------------------+----------+--------------+------------------+
        | mysql-bin.000001 |      328 | smsfw_ui     |                  |
        +------------------+----------+--------------+------------------+

6. Настроить репликацию на втором сервере:

        stop slave;

        CHANGE MASTER TO MASTER_HOST = '10.200.110.137', MASTER_USER = 'replicator', MASTER_PASSWORD = 'sql', MASTER_LOG_FILE = 'mysql-bin.000001', MASTER_LOG_POS = 328;

        start slave;

7. Повторить шаг 6 на первом сервере:

        stop slave;

        CHANGE MASTER TO MASTER_HOST = '10.200.110.138', MASTER_USER = 'replicator', MASTER_PASSWORD = 'sql', MASTER_LOG_FILE = 'mysql-bin.000001', MASTER_LOG_POS = 328;

        start slave;

8. Проверить, что всё работает:
* Значение Slave_IO_Running должно быть Yes.

Если это не так, то нужно остановить и деактивировать `firewalld`.

        show slave status \G

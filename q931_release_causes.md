| Код | Причина | Описание |
| :-- | :------ | :------- |
| 1 | Unallocated (unassigned) number | Не распределённый или не назначенный номер |
| 6 | Channel unacceptable | Канал не приемлем |
| 16 | Normal call clearing | Нормальное завершение вызова |
| 17 | User busy (engaged) | Абонент занят |
| 18 | No user responding | Абонент не отвечает |
| 21 | Call rejected | Вызов сброшен |
| 22 | Number changed | Номер изменён на номер в диагностическом поле |
| 26 | Non-selected user clearing | Не выбранная абонентом очистка |
| 27 | Destination out of order | Направление вне предписанного |
| 28 | Invalid number format | Неверный формат номера или неполный номер |
| 29 | Facility rejected | Функциональная возможность отклонена (сетью) |
| 30 | Response to STATUS ENQUIRY | Ответ на сообщение ЗАПРОС СОСТОЯНИЯ |
| 31 | Normal, unspecified | Нормальное состояние, не уточнено |
| 34 | No circuit/channel available | Нет схемы доступа/канала |
| 38 | Network out of order | Сеть вышла из строя |
| 41 | Temporary failure | Временная неудача |
| 42 | Switching equipment congestion | Коммутационное оборудование перегружено |
| 43 | Access information discarded | Доступ к информации отброшен |
| 44 | Requested circuit/channel not available | Запрашиваемая схема/канал недоступны |
| 47 | Resources unavailable, unspecified | Ресурс недоступен, не уточнено |
| 50 | Requested facility not subscribed | Нет подписки на запрошенную услугу |
| 57 | Bearer capability not authorized | Возможности переноса информации не санкционированы |
| 58 | Bearer capability not presently available | Возможности переноса информации в данный момент недоступны |
| 63 | Service or option not available, unspecified | Сервис или опция недоступны, не уточнено |
| 65 | Bearer capability not implemented | Возможности переноса информации не реализованы |
| 66 | Channel type not implemented | Типа канала не поддерживается |
| 70 | Only restricted digital information bearer capability is available | Доступны только ограниченные возможности переноса цифровой информации |
| 79 | Service or option not implemented, unspecified | Сервис или опция не применимы, не определено |
| 81 | Invalid call reference value | Неверное значение идентификатора вызова |
| 82 | Identified channel does not exist | Указанный канал не существует |
| 83 | A suspended call exists, but this call identity does not | Присутствует приостановленный звонок, но этот вызов не идентифицирован |
| 84 | Call identity in use | Идентификатор вызова уже используется |
| 85 | No call suspended | Нет приостановленных вызовов |
| 86 | Call having the requested call identity has been cleared | Вызов, имеющий затребованный идентификатор звонка, был очищен |
| 88 | Incompatible destination | Несовместимый адресат |
| 91 | Invalid transit network selection | Выбрана неверная транзитная сеть (использование в пределах страны) |
| 95 | Invalid message, unspecified | Неверное сообщение, не уточнено |
| 96 | Mandatory information element is missing | Отсутствует обязательный информационный элемент |
| 97 | Message type non-existent or not implemented | Не существующий тип сообщения или не применим |
| 98 | Message not compatible with call state or message type non-existent or not implemented | Сообщение не совместимо со статусом звонка, или не существующий тип сообщения, или не применим |
| 99 | Information element non-existent or not implemented | Не существующий или не применимый информационный элемент |
| 100 | Invalid information element contents | Неверное содержимое информационного элемента |
| 101 | Message not compatible with call state | Сообщение не совместимо со статусом вызова |
| 102 | Recovery on timer expiry | Восстановлено по истечении таймера |
| 111 | Protocol error, unspecified | Ошибка протокола, не уточнено |
| 127 | Interworking, unspecified | Межсетевое взаимодействие, не уточнено |

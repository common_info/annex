## Тестовое задание ##

1. Укажите тип каждого из параметров ниже.

```json
{
    "param_1": "89001234123",
    "param_2": 89001234123,
    "param_3": {89001234123},
    "param_4": {"89001234123"},
    "param_5": [89001234123],
    "param_6": ["89001234123"],
}
```

2. Укажите результат выполнения данной команды в bash Linux. Достаточно назвать полученные объекты, а также охарактеризовать их одним-двумя предложением.

```bash
while read -r line; do echo "$line" && cat $line && echo $'\n'; done <<< $(cd /path/to/directory/ && ls -a | grep -E '\.cfg|\.conf');
```

PS. Набор команд, приведенный ниже, выполняет те же самые действия, что и представленная выше, но без цикла. Возможно, в таком виде более привычно видеть действия в консоли.

```bash
cd /path/to/directory/;
VAR="cat `ls -a | grep -E '\.cfg|\.conf'`";
eval $VAR;
```

3. Опишите заголовки и параметры, передаваемые в API-запросе, HTTP REST JSON:

```http
POST SubscriberService/AddSubscriber/ HTTP/2.0
Host: http://www.sample.protei.ru
Accept: application/json
Authorization: Basic dXNlcjpwYXNz
Content-Type: application/json
Content-Length: 3398
```

```json
{
  "imsi": "250010000001",
  "msisdn": "79000000001",
  "status": 2,
  "category": 10,
  "networkAccessMode": 0,
  "DefaultForwardingNumber": "867349752",
  "DefaultForwardingStatus": 7,
  "ipSmGwNumber": "23525252",
  "algorithm": 4,
  "ki": "12345678900987654321123456789009",
  "opc": "09876543211234567890098765432112",
  "activeTime": 1000,
  "edrxCycleLength": [
    {
      "ratType": 1005,
      "lengthValue": 8
    }
  ],
  "ssData": [
    {
      "ss_Code": 17,
      "ss_Status": 5,
      "sub_option_type": 1,
      "sub_option": 1,
      "tele_service": [ 0, 16, 32 ]
    }
  ],
  "ssForw": [
    {
      "ss_Code": 42,
      "ss_Status": 7,
      "forwardedToNumber": "42513466754",
      "tele_service": [ 0, 16, 32 ]
    }
  ],
  "EpsData": {
    "defContextId": 1,
    "ueMaxDl": 10000,
    "ueMaxUl": 10000
  },
  "link-eps-data": [
    {
      "context-id": 2,
      "ipv4": "192.168.1.22",
      "plmnId": "25001"
    }
  ],
  "pdpData": [
    {
      "context-id": 1,
      "type": "0080"
    }
  ],
  "ODB": {
    "generalSetList": [ 1, 6, 19 ],
    "SubscriberStatus": 1
  },
  "imsProfile": {
    "impi": "250010000001@sample.ims.protei.ru",
    "authScheme": 5,
    "sipDigest": { "password": "superpass" },
    "impus": [
      {
        "Identity": "sip:79000000001@sample.ims.protei.ru",
        "BarringIndication": 0,
        "Type": 0,
        "CanRegister": 1,
        "ServiceProfileName": "sp"
      },
      {
        "Identity": "tel:+79000000001@sample.ims.protei.ru",
        "BarringIndication": 0,
        "Type": 1,
        "CanRegister": 1,
        "WildcardPsi": "tel:+79000000001!.*!",
        "PsiActivation": 1,
        "ServiceProfileName": "sp"
      }
    ],
    "implicitlySets": [ { "name": "implSet1" } ]
  },
  "imsSubscription": {
    "name": "250010000001",
    "capabilitySetId": 1,
    "prefferedScscfSetId": 1,
    "chargingInformationName": "ci",
    "serviceProfiles": [
      {
        "Name": "sp",
        "CoreNetworkServiceAuthorization": 1,
        "Ifcs": [
          {
            "Name": "ifc1",
            "Priority": 1,
            "ApplicationServerName": "as",
            "ProfilePartIndicator": 1,
            "TriggerPoint": {
              "ConditionTypeCNF": 1,
              "Spt": [
                {
                  "Group": 1,
                  "Method": "INVITE",
                  "SessionCase": 1,
                  "ConditionNegated": 2,
                  "Type": 3,
                  "RequestUri": "http://protei.sample.ru/spt1",
                  "Header": "header",
                  "Content": "headerContent",
                  "SdpLine": "sdpLine",
                  "SdpLineContent": "sdpLineContent",
                  "RegistrationType": 1
                }
              ]
            }
          }
        ]
      }
    ],
    "implicitlyRegisteredSet": [
      {
        "name": "implSet1",
        "impus": [
          {
            "Identity": "sip:protei@sample.ims.protei.ru",
            "BarringIndication": 0,
            "Type": 0,
            "CanRegister": 1,
            "ServiceProfileName": "sp",
            "Default": true
          },
          {
            "Identity": "sip:sample.ntc_protei@ims.protei.ru",
            "BarringIndication": 0,
            "Type": 0,
            "CanRegister": 1,
            "ServiceProfileName": "sp"
          }
        ]
      }
    ]
  }
}
```
---

## Старые задания ##



2. XML

```xml
<drawing>
    <inline distB="0" distL="0" distR="0" distT="0">
        <extent cx="1483200" cy="540000"/>
        <effectExtent b="0" l="0" r="3175" t="0"/>
        <docPr id="2" name="Name 2"/>
        <GraphicFramePr>
            <graphicFrameLocks noChangeAspect="true"/>
        </GraphicFramePr>
        <graphic>
            <graphicData uri="uri">
                <pic>
                    <PicPr>
                        <cNvPr id="2" name="Pic 2"/>
                        <cNvPicPr/>
                    </PicPr>
                    <blipFill>
                        <blip embed="rId1">
                            <extLst>
                                <ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}">
                                    <useLocalDpi val="false"/>
                                </ext>
                            </extLst>
                        </blip>
                        <stretch>
                            <fillRect/>
                        </stretch>
                    </blipFill>
                    <spPr>
                        <xfrm>
                            <off x="0" y="0"/>
                            <ext cx="1483200" cy="540000"/>
                        </xfrm>
                        <prstGeom prst="rect">
                            <avLst/>
                        </prstGeom>
                    </spPr>
                </pic>
            </graphicData>
        </graphic>
    </inline>
</drawing>
```

3. Linux:

* перейти в `/path/to/dir/`;
* вывести все содержимое папки;
* вывести текст файла `file.cfg` от имени суперпользователя;
* удалить директорию `dir` со всеми файлами;

```bash
# ~: cd /path/to/dir/
# /path/to/dir/: ls -a
# /path/to/dir/: sudo cat/vim/less/more file.cfg
# /path/to/dir/: cd ../
# /path/to/: rm -rf "./dir/"
```

4. API HTTP REST JSON:

* запрос POST;
* заголовки:
    * `Accept: application/json`;
    * `Content-Type: application/json`;
    * `X-Session-Id = 66b50bdfefb03c06c9d2adce93bbbf65b5607775`;
* URI `sample.protei.ru`;
* webhook `get_info_user`;
* query:
    * id --- \<int\>;
* body:
    - user:
        - name: "John";
        - surname: "Doe";

```bash
curl -X POST http://www.sample.protei.ru/get_info_user?id=1 
-H "Accept: application/json" 
-H "Content-Type: application/json" 
-H "X-Session-Id: 66b50bdfefb03c06c9d2adce93bbbf65b5607775" 
-d "{\"user\":{\"name\":\"John\",\"surname\":\"Doe\"}}"
```

```http
POST /get_info_user?id=1 HTTP/1.1
Host: www.sample.protei.ru
Accept: application/json
Content-Type: application/json
Content-Length: 40
X-Session-Id: 66b50bdfefb03c06c9d2adce93bbbf65b5607775

{"user":{"name":"John","surname":"Doe"}}
```

5. ASN.1

```
InsertSubscriberDataArg ::= SEQUENCE {
  imsi [0] OCTET STRING (SIZE (3..8)) OPTIONAL,
  msisdn [1] OCTET STRING (SIZE (1..maxISDN-AddressLength)) OPTIONAL,
  category [2] OCTET STRING (SIZE (1)) OPTIONAL,
  subscriberStatus [3] ENUMERATED {
    serviceGranted (0),
    operatorDeterminedBarring (1)} OPTIONAL,
  bearerServiceList [4] SEQUENCE SIZE (1..maxNumOfBearerServices) OF OCTET STRING (SIZE (1..5)) OPTIONAL,
  teleserviceList [6] SEQUENCE SIZE (1..maxNumOfTeleservices) OF OCTET STRING (SIZE (1..5)) OPTIONAL,
  extensionContainer [14] ExtensionContainer OPTIONAL,
  istAlertTimer [26] INTEGER (15..255) OPTIONAL,
  superChargerSupportedInHLR [27] OCTET STRING (SIZE (1..6)) OPTIONAL,
  mc-SS-Info [28] SEQUENCE {
    ss-Code [0] OCTET STRING (SIZE (1)),
    ss-Status [1] OCTET STRING (SIZE (1..5)),
    nbrSB [2] INTEGER (2..maxNumOfMC-Bearers),
    nbrUser [3] INTEGER (1..maxNumOfMC-Bearers),
    extensionContainer [4] ExtensionContainer OPTIONAL,
    ...} OPTIONAL,
  cs-AllocationRetentionPriority [29] OCTET STRING (SIZE (1)) OPTIONAL,
  sgsn-CAMEL-SubscriptionInfo [17] SEQUENCE {
    gprs-CSI [0] SEQUENCE {
      gprs-CamelTDPDataList [0] SEQUENCE SIZE (1..maxNumOfCamelTDPData) OF SEQUENCE {
          gprs-TriggerDetectionPoint [0] ENUMERATED {
            attach (1),
            attachChangeOfPosition (2),
            pdp-ContextEstablishment (11),
            pdp-ContextEstablishmentAcknowledgement (12),
            pdp-ContextChangeOfPosition (14),
            ...},
          extensionContainer [4] ExtensionContainer OPTIONAL,
          ...} OPTIONAL,
      camelCapabilityHandling [1] INTEGER (1..16) OPTIONAL,
      extensionContainer [2] ExtensionContainer OPTIONAL,
      notificationToCSE [3] NULL OPTIONAL,
      csi-Active [4] NULL OPTIONAL,
      ...} OPTIONAL,
    mo-sms-CSI [1] SEQUENCE {
      sms-CAMEL-TDP-DataList [0] SEQUENCE SIZE (1..maxNumOfCamelTDPData) OF SEQUENCE {
          sms-TriggerDetectionPoint [0] ENUMERATED {
            sms-CollectedInfo (1),
            sms-DeliveryRequest (2),
            ...},
          serviceKey [1] INTEGER (0..2147483647),
          gsmSCF-Address [2] OCTET STRING (SIZE (1..maxISDN-AddressLength)),
          extensionContainer [4] ExtensionContainer OPTIONAL,
          ...} OPTIONAL,
      camelCapabilityHandling [1] INTEGER (1..16) OPTIONAL,
      extensionContainer [2] ExtensionContainer OPTIONAL,
      notificationToCSE [3] NULL OPTIONAL,
      csi-Active [4] NULL OPTIONAL,
      ...} OPTIONAL,
    extensionContainer [2] ExtensionContainer OPTIONAL,
    mt-sms-CSI [3] SEQUENCE {
      sms-CAMEL-TDP-DataList [0] SEQUENCE SIZE (1..maxNumOfCamelTDPData) OF SEQUENCE {
          sms-TriggerDetectionPoint [0] ENUMERATED {
            sms-CollectedInfo (1),
            sms-DeliveryRequest (2),
            ...},
          serviceKey [1] INTEGER (0..2147483647),
          gsmSCF-Address [2] OCTET STRING (SIZE (1..maxISDN-AddressLength)),
          defaultSMS-Handling [3] ENUMERATED {
            continueTransaction (0),
            releaseTransaction (1),
            ...},
          extensionContainer [4] ExtensionContainer OPTIONAL,
          ...} OPTIONAL,
      camelCapabilityHandling [1] INTEGER (1..16) OPTIONAL,
      extensionContainer [2] ExtensionContainer OPTIONAL,
      notificationToCSE [3] NULL OPTIONAL,
      csi-Active [4] NULL OPTIONAL,
      ...} OPTIONAL,
    mt-smsCAMELTDP-CriteriaList [4] SEQUENCE SIZE (1.. maxNumOfCamelTDPData) OF SEQUENCE {
        sms-TriggerDetectionPoint ENUMERATED {
          sms-CollectedInfo (1),
          sms-DeliveryRequest (2),
          ...},
        tpdu-TypeCriterion [0] SEQUENCE SIZE (1..maxNumOfTPDUTypes) OF ENUMERATED {
            sms-DELIVER (0),
            sms-SUBMIT-REPORT (1),
            sms-STATUS-REPORT (2),
            ...} OPTIONAL,
        ...} OPTIONAL,
    mg-csi [5] SEQUENCE {
      mobilityTriggers SEQUENCE SIZE (1..maxNumOfMobilityTriggers) OF OCTET STRING (SIZE (1)),
      serviceKey INTEGER (0..2147483647),
      gsmSCF-Address [0] OCTET STRING (SIZE (1..maxISDN-AddressLength)),
      extensionContainer [1] ExtensionContainer OPTIONAL,
      notificationToCSE [2] NULL OPTIONAL,
      csi-Active [3] NULL OPTIONAL,
      ...} OPTIONAL,
    ...} OPTIONAL,
  chargingCharacteristics [18] OCTET STRING (SIZE (2)) OPTIONAL,
  ics-Indicator [20] BOOLEAN OPTIONAL,
  csg-SubscriptionDataList [32] SEQUENCE SIZE (1..50) OF SEQUENCE {
      csg-Id BIT STRING (SIZE (27)),
      expirationDate OCTET STRING (SIZE (4)) OPTIONAL,
      extensionContainer ExtensionContainer OPTIONAL,
      lipa-AllowedAPNList [0] SEQUENCE SIZE (1..maxNumOfLIPAAllowedAPN) OF OCTET STRING (SIZE (2..63)) OPTIONAL,
      plmn-Id [1] OCTET STRING (SIZE (3)) OPTIONAL,
      ...} OPTIONAL,
  ue-ReachabilityRequestIndicator [33] NULL OPTIONAL,
  sgsn-Number [34] OCTET STRING (SIZE (1..maxISDN-AddressLength)) OPTIONAL,
  mme-Name [35] OCTET STRING (SIZE(9..255)) OPTIONAL,
  subscribedPeriodicRAUTAUtimer [36] INTEGER (0..4294967295) OPTIONAL,
  vplmnLIPAAllowed [37] NULL OPTIONAL,
  mdtUserConsent [38] BOOLEAN OPTIONAL,
  subscribedPeriodicLAUtimer [39] INTEGER (0..4294967295) OPTIONAL,
  vplmn-Csg-SubscriptionDataList [40] SEQUENCE SIZE (1..50) OF SEQUENCE {
      csg-Id BIT STRING (SIZE (27)),
      expirationDate OCTET STRING (SIZE (4)) OPTIONAL,
      extensionContainer ExtensionContainer OPTIONAL,
      lipa-AllowedAPNList [0] SEQUENCE SIZE (1..maxNumOfLIPAAllowedAPN) OF OCTET STRING (SIZE (2..63)) OPTIONAL,
      plmn-Id [1] OCTET STRING (SIZE (3)) OPTIONAL,
      ...} OPTIONAL,
  additionalMSISDN [41] OCTET STRING (SIZE (1..maxISDN-AddressLength)) OPTIONAL,
  psAndSMS-OnlyServiceProvision [42] NULL OPTIONAL,
  smsInSGSNAllowed [43] NULL OPTIONAL,
  cs-to-ps-SRVCC-Allowed-Indicator [44] NULL OPTIONAL,
  pcscf-Restoration-Request [45] NULL OPTIONAL,
  ueUsageType [48] OCTET STRING (SIZE (4)) OPTIONAL,
  userPlaneIntegrityProtectionIndicator [49] NULL OPTIONAL,
}
```

```
InsertSubscriberDataArg = [
	imsi: hex
	msisdn: hex
	category: hex
	subscriber: enum <int>
	bearerServiceList: list<hex> = []
	teleserviceList: list<hex> = []
	extensionContainer: ExtensionContainer
	istAlertTimer: int
	superChargerSupportedInHLR: hex
	mc-SS-Info: {
		ss-Code: hex
		ss-Status: hex
		nbrSB: int
		nbrUser: int
		extensionContainer: ExtensionContainer
	}
	cs-AllocationRetentionPriority: hex
	sgsn-CAMEL-SubscriptionInfo: object = {
		gprs-CSI: object = {
			gprs-CamelTDPDataList: list<object> = [
				{
					gprs-TriggerDetectionPoint: enum <int>
					extensionContainer: ExtensionContainer
				}
			]
			camelCapabilityHandling: int
			extensionContainer: ExtensionContainer
			notificationToCSE: None
			csi-Active: None
		}
		mo-sms-CSI: object = {
			sms-CAMEL-TDP-DataList: list<object> = [
				{
					sms-TriggerDetectionPoint: enum <int>
					serviceKey: int
					gsmSCF-Address: hex
					extensionContainer: ExtensionContainer
				}
			]
			camelCapabilityHandling: int
			extensionContainer: ExtensionContainer
			notificationToCSE: None
			csi-Active: None
		}
		extensionContainer: ExtensionContainer
		mt-sms-CSI: object = {
			sms-CAMEL-TDP-DataList: list<object> = [
				{
					sms-TriggerDetectionPoint: enum <int>
					serviceKey: int
					gsmSCF-Address: hex
					extensionContainer: ExtensionContainer
				}
			]
			camelCapabilityHandling: int
			extensionContainer: ExtensionContainer
			notificationToCSE: None
			csi-Active: None
		}
		mt-smsCAMELTDP-CriteriaList: list<object> = [
			{
				sms-TriggerDetectionPoint: enum <int>
				tpdu-TypeCriterion: list<enum <int>>
			}
		]
		mg-csi: object = {
			mobilityTriggers: list<hex> = []
		    serviceKey: int
		    gsmSCF-Address: hex
		    extensionContainer: ExtensionContainer
			notificationToCSE: None
			csi-Active: None
		}
	}
	chargingCharacteristics: hex
	ics-Indicator bool
	csg-SubscriptionDataList: list<object> = [
		{
			csg-Id: binary
      		expirationDate: hex
      		extensionContainer: ExtensionContainer
      		lipa-AllowedAPNList: list<hex> = []
      		plmn-Id: hex
		}
	]
	ue-ReachabilityRequestIndicator: None
  	sgsn-Number: hex
  	mme-Name: hex
  	subscribedPeriodicRAUTAUtimer: int
  	vplmnLIPAAllowed: None
  	mdtUserConsent: bool
  	subscribedPeriodicLAUtimer: int
  	vplmn-Csg-SubscriptionDataList: list<object> = [
  		{
  			csg-Id: binary
		    expirationDate: hex
		    extensionContainer: ExtensionContainer
		    lipa-AllowedAPNList: list<hex> = []
		    plmn-Id: hex
  		}
  	]
	additionalMSISDN: hex
	psAndSMS-OnlyServiceProvision: None
	smsInSGSNAllowed: None
	cs-to-ps-SRVCC-Allowed-Indicator: None
	pcscf-Restoration-Request: None
	ueUsageType: hex
	userPlaneIntegrityProtectionIndicator: None
]
```




AddSubscriber
Создание абонента
endpoint: /SubscriberService/AddSubscriber
method: POST
Request
Element/Attribute   Type    Mandatory   Description     Values  DB  Version
imsi    <String>    Mandatory   IMSI        TM_SUBSCRIBER_PROFILE.STRIMSI   
msisdn  <String>    Mandatory   MSISDN      TM_SUBSCRIBER_PROFILE.STRMSISDN     
status  <int>   Mandatory   The subscriber administrative state     0 ‒ not provisioned (the card is not assigned to MSISDN); 1 ‒ provisioned/locked (the card is assigned to MSISDN and locked); 2 ‒ provisioned/unlocked (in-service); 3 ‒ provisioned/suspended (service is stopped, for example, if the phone was stolen); 4 – terminated (the subscriber is deleted)   TM_SUBSCRIBER_PROFILE.NADMINISTRATIVE_STATE     
category    <int>   Optional            TM_SUBSCRIBER_PROFILE.NCATEGORY     
networkAccessMode   <int>   Optional            TM_SUBSCRIBER_PROFILE.NNETWORKACCESSMODE    
DefaultForwardingNumber     <String>    Optional            TM_SUBSCRIBER_PROFILE.STRDEFAULTFORWARDINGNUMBER    
DefaultForwardingStatus     <int>   Optional            TM_SUBSCRIBER_PROFILE.NDEFAULTFORWARDINGSTATUS  
groupId     <int>   Optional            TM_SUBSCRIBER_PROFILE.NGROUPID  
IpSmGwNumber    <String>    Optional            TM_SUBSCRIBER_PROFILE.STRIP_SM_GW_NUMBER    
IpSmGwHost  <String>    Optional            TM_SUBSCRIBER_PROFILE.STRIP_SM_GW_HOST  
IpSmGwRealm     <String>    Optional            TM_SUBSCRIBER_PROFILE.STRIP_SM_GW_REALM     
deactivatePsi   <int>   Optional            TM_SUBSCRIBER_PROFILE.NDEACTIVATEPSI    
baocWithoutCamel    <int>   Optional            TM_SUBSCRIBER_PROFILE.NBAOC_WITHOUTCAMEL    
ForbidReg_WithoutCamel  <int>   Optional            TM_SUBSCRIBER_PROFILE.LFORBIDREG_WITHOUTCAMEL   
qosGprsId   <int>   Optional    id профиля QoS для Gprs         TM_SUBSCRIBER_PROFILE.NQOS_GPRS_ID  
qosEpsId    <int>   Optional    id профиля QoS для Eps      TM_SUBSCRIBER_PROFILE.NQOS_EPS_ID   
roamingNotAllowed   <int>   Optional    запрет роаминга         TM_SUBSCRIBER_PROFILE.NROAMINGNOTALLOWED    
accessRestrictionData   <AccessRestrictionData>     Optional    Access restrition data      TM_SUBSCRIBER_PROFILE.NACCESSRESTRICTIONDATA    
periodicLauTimer    <int>   Optional            TM_SUBSCRIBER_PROFILE.NPERIODICLAU_TIMER    
periodicRauTauTimer     <int>   Optional            TM_SUBSCRIBER_PROFILE.NPERIODICRAU_TAU_TIMER    
SCA     <String>    Optional            TM_SUBSCRIBER_PROFILE.STRSCA    
ueUsageType     <int>   Optional            TM_SUBSCRIBER_PROFILE.NUE_USAGE_TYPE    
imrn    <String>    Optional            TM_SUBSCRIBER_PROFILE.STRIMRN   
voLte   <int>   Optional            TM_SUBSCRIBER_PROFILE.NVOLTE    
icsIndicator    <int>   Optional            TM_SUBSCRIBER_PROFILE.NICS_INDICATOR    
hlr_profile_id  <int>   Optional    Identifier of ProfileTemplate from config           
algorithm   <int>   Mandatory   Algorithm of authentification   1 - COMP1; 2 - COMP2; 3 - COMP3; 4 - MILLENAGE; 6 - XOR; 7 - TUAK; 8 - S3G128; 9 - S3G256;  TM_AUC.NALGORITM    
ki  <String>    Mandatory   Key     for TUAK length 32/64, for other length 32  TM_AUC.STRKI    
opc     <String>    Optional    OPC         TM_AUC.STROPC   
HSM_ID  <int>   Optional            TM_AUC.NHSM_ID  
aucData     <AucData>   Optional            TM_AUC  
ssData  [<SsData>]  Optional            TM_PROVISIONED_SS, TM_PROVISIONED_SS_BS     
ssForw  [<SsForw>]  Optional            TM_PROVISIONED_SS_FORW  
ssBarring   [<SsBarring>]   Optional            TM_PROVISIONED_SS_CALL_BARR     
ssCugFeat   [<SsCugFeat>]   Optional            TM_PROVISIONED_SS_CUG_FEATURE   
ssCugSub    [<SsCugSub>]    Optional            TM_PROVISIONED_SS_CUG_SUB, TM_PROVISIONED_SS_CUG_SUB_BS     
epsData     <EpsData>   Optional            TM_DM_SUBSCRIBER_EPS    
lcsId   <int>   Optional            TM_LCS_SUBSCRIBER.NLCS_ID   
PDP_DATA    <String>    Optional    DEPRECATED      TM_SUBSCRIBER_PDP   
EPS_CONTEXTS    <String>    Optional    DEPRECATED      TM_DM_SUBSCRIBER_EPS_CONTEXT    
pdpData     [<PdpData>]     Optional            TM_SUBSCRIBER_PDP   
link-eps-data   [<LinkEpsData>]     Optional            TM_DM_SUBSCRIBER_EPS_CONTEXT    
O_CSI   [<int>]     Optional    DEPRECATED      TM_CAMEL_SUBSCRIBER (NTYPECSI = 0; NSUB_TYPE = 0;)  
T_CSI   [<int>]     Optional    DEPRECATED      TM_CAMEL_SUBSCRIBER (NTYPECSI = 1; NSUB_TYPE = 0;)  
SMS_CSI     [<int>]     Optional    DEPRECATED      TM_CAMEL_SUBSCRIBER (NTYPECSI = 2;)     
GPRS_CSI    [<int>]     Optional    DEPRECATED      TM_CAMEL_SUBSCRIBER (NTYPECSI = 3;)     
M_CSI   [<int>]     Optional    DEPRECATED      TM_CAMEL_SUBSCRIBER (NTYPECSI = 4;)     
D_CSI   [<int>]     Optional    DEPRECATED      TM_CAMEL_SUBSCRIBER (NTYPECSI = 5; NSUB_TYPE = 0;)  
USSD_CSI    [<int>]     Optional    DEPRECATED      TM_CAMEL_SUBSCRIBER (NTYPECSI = 8;)     
O_IM_CSI    [<int>]     Optional    DEPRECATED      TM_CAMEL_SUBSCRIBER (NTYPECSI = 0; NSUB_TYPE = 2;)  
VT_IM_CSI   [<int>]     Optional    DEPRECATED      TM_CAMEL_SUBSCRIBER (NTYPECSI = 1; NSUB_TYPE = 3;)  
D_IM_CSI    [<int>]     Optional    DEPRECATED      TM_CAMEL_SUBSCRIBER (NTYPECSI = 5; NSUB_TYPE = 2;)  
csi-list    [<CsiLink>]     Optional            TM_CAMEL_SUBSCRIBER     
TeleServices    [<int>]     Optional            TM_TELESERVICE  
BearerServices  [<int>]     Optional            TM_BEARERSERVICE    
WL_DATA     [<int>]     Optional            TM_SUB_2_WL     
BL_DATA     [<int>]     Optional            TM_SUB_2_BL     
WL_NAMES    [<String>]  Optional            TM_SUB_WL   
BL_NAMES    [<String>]  Optional            TM_SUB_BL   
ODB     <ODB>   Optional            TM_ODB  
odb-param   <OdbParam>  Optional    DEPRECATED      TM_ODB  
chargingCharacteristics     <String>    Optional            TM_GPRS_DATA.STRCHARGINGCHARACTERISTICS     
aMsisdn     [<String>]  Optional    List of additional MSISDN(SRI,SRIFSM)       TM_AMSISDN  
imsProfile  <ImsProfile>    Optional                
imsSubscription     <ImsSubscription>   Optional                
regionalZoneCodes   [<RegionalZoneCodeLink>]    Optional            TM_SUB_2_REGIONAL   
csToPsSrvccAllowedIndicator     <Boolean>   Optional            TM_SUBSCRIBER_PROFILE.NCS_PS_SRVCC_INDICATOR    2.0.35.0
activeTime  <Long>  Optional            TM_SUBSCRIBER_PROFILE.NACTIVE_TIME  2.0.36.0
edrxCycleLength     [<EdrxCycleLength>]     Optional            TM_EDRX_CYCLE_LENGTH    2.0.36.0
Reply
Element/Attribute   Type    Mandatory   Description
status  <int>   Mandatory   The status of the request
Example

```
{
    "imsi":"250010000001",
    "msisdn":"79000000001",
    "status":2,
    "category":10,
    "networkAccessMode":0,
    "DefaultForwardingNumber":"867349752",
    "DefaultForwardingStatus":7,
    "ipSmGwNumber":"23525252",
    "algorithm":4,
    "ki":"12345678900987654321123456789009",
    "opc":"09876543211234567890098765432112",
    "activeTime":1000,
    "edrxCycleLength":[
        {
            "ratType":1005,
            "lengthValue":8
        }
    ],
    "ssData":
    [
        {
            "ss_Code":17,
            "ss_Status":5,
            "sub_option_type":1,
            "sub_option":1,
            "tele_service":[0,16,32]
        }
    ],
    "ssForw":
    [
        {
            "ss_Code":42,
            "ss_Status":7,
            "forwardedToNumber":"42513466754",
            "tele_service":[0,16,32]
        }
    ],
    "EpsData":
    {
        "defContextId":1,
        "ueMaxDl":10000,
        "ueMaxUl":10000
    },
    "link-eps-data":
    [
        {
            "context-id":2,
            "ipv4":"192.168.1.22",
            "plmnId":"25001"
        }
    ],
    "pdpData":
    [
        {
            "context-id":1,
            "type":"0080"
        }
    ],
    "ODB":
    {
        "generalSetList":[1,6,19],
        "SubscriberStatus":1
    },
    "imsProfile":
    {
        "impi":"250010000001@ims.protei.ru",
        "authScheme":5,
        "sipDigest":
        {
            "password":"elephant"
        },
        "impus":
        [
            {
                "Identity":"sip:79000000001@ims.protei.ru",
                "BarringIndication":0,
                "Type":0,
                "CanRegister":1,
                "ServiceProfileName":"sp"
            },
            {
                "Identity":"tel:+79000000001@ims.protei.ru",
                "BarringIndication":0,
                "Type":1,
                "CanRegister":1,
                "WildcardPsi":"tel:+79000000001!.*!",
                "PsiActivation":1,
                "ServiceProfileName":"sp"
            }
        ],
        "implicitlySets":
        [
            {
                "name":"implSet1"
            }
        ]
    },
    "imsSubscription":
    {
        "name":"250010000001",
        "capabilitySetId":1,
        "prefferedScscfSetId":1,
        "chargingInformationName":"ci",
        "serviceProfiles":
        [
            {
                "Name":"sp",
                "CoreNetworkServiceAuthorization":1,
                "Ifcs":
                [
                    {
                        "Name":"ifc1",
                        "Priority":1,
                        "ApplicationServerName":"as",
                        "ProfilePartIndicator":1,
                        "TriggerPoint":
                        {
                            "ConditionTypeCNF":1,
                            "Spt":
                            [
                                {
                                    "Group":1,
                                    "Method":"INVITE",
                                    "SessionCase":1,
                                    "ConditionNegated":2,
                                    "Type":3,
                                    "RequestUri":"http://ims.protei.ru/spt1",
                                    "Header":"header",
                                    "Content":"headerContent",
                                    "SdpLine":"sdpLine",
                                    "SdpLineContent":"sdpLineContent",
                                    "RegistrationType":1
                                }  
                            ]
                        }
                    }
                ]
            }
        ],
        "implicitlyRegisteredSet":
        [
            {
                "name":"implSet1",
                "impus":
                [
                    {
                        "Identity":"sip:protei@ims.protei.ru",
                        "BarringIndication":0,
                        "Type":0,
                        "CanRegister":1,
                        "ServiceProfileName":"sp",
                        "Default":true
                    },
                    {
                        "Identity":"sip:ntc_protei@ims.protei.ru",
                        "BarringIndication":0,
                        "Type":0,
                        "CanRegister":1,
                        "ServiceProfileName":"sp"
                    }
                ]
            }
        ]
    }
}
```

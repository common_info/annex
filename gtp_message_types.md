# Типы сообщений GTP #

## GTP v1 ##

| Dec | Hex | Сообщение |
| :-- | :-- | :-------- |
| 1 | 1 | Echo Request |
| 2 | 2 | Echo Response |
| 3 | 3 | Version Not Supported |
| 4 | 4 | Node Alive Request |
| 5 | 5 | Node Alive Response |
| 6 | 6 | Redirection Request |
| 7 | 7 | Redirection Response |
| 16 | 10 | Create PDP Context Request |
| 17 | 11 | Create PDP Context Response |
| 18 | 12 | Update PDP Context Request |
| 19 | 13 | Update PDP Context Response |
| 20 | 14 | Delete PDP Context Request |
| 21 | 15 | Delete PDP Context Response |
| 22 | 16 | Initiate PDP Context Activation Request |
| 23 | 17 | Initiate PDP Context Activation Response |
| 26 | 1A | Error Indication |
| 27 | 1B | PDU Notification Request |
| 28 | 1C | PDU Notification Response |
| 29 | 1D | PDU Notification Reject Request |
| 30 | 1E | PDU Notification Reject Response |
| 31 | 1F | Supported Extensions Header Notification |
| 32 | 20 | Send Routing for GPRS Request |
| 33 | 21 | Send Routing for GPRS Response |
| 34 | 22 | Failure Report Request |
| 35 | 23 | Failure Report Response |
| 36 | 24 | Note MS Present Request |
| 37 | 25 | Note MS Present Response |
| 38 | 26 | Identification Request |
| 39 | 27 | Identification Response |
| 50 | 32 | SGSN Context Request |
| 51 | 33 | SGSN Context Response |
| 52 | 34 | SGSN Context Acknowledge |
| 53 | 35 | Forward Relocation Request |
| 54 | 36 | Forward Relocation Response |
| 55 | 37 | Forward Relocation Complete |
| 56 | 38 | Relocation Cancel Request |
| 57 | 39 | Relocation Cancel Response |
| 58 | 3A | Forward SRNS Context |
| 59 | 3B | Forward Relocation Complete Acknowledge |
| 60 | 3C | Forward SRNS Context Acknowledge |
| 61 | 3D | UE Registration Request |
| 62 | 3E | UE Registration Response |
| 70 | 46 | RAN Information Relay |
| 96 | 60 | MBMS Notification Request |
| 97 | 61 | MBMS Notification Response |
| 98 | 62 | MBMS Notification Reject Request |
| 99 | 63 | MBMS Notification Reject Response |
| 100 | 64 | Create MBMS Notification Request |
| 101 | 65 | Create MBMS Notification Response |
| 102 | 66 | Update MBMS Notification Request |
| 103 | 67 | Update MBMS Notification Response |
| 104 | 68 | Delete MBMS Notification Request |
| 105 | 69 | Delete MBMS Notification Response |
| 112 | 70 | MBMS Registration Request |
| 113 | 71 | MBMS Registration Response |
| 114 | 72 | MBMS De-Registration Request |
| 115 | 73 | MBMS De-Registration Response |
| 116 | 74 | MBMS Session Start Request |
| 117 | 75 | MBMS Session Start Response |
| 118 | 76 | MBMS Session Stop Request |
| 119 | 77 | MBMS Session Stop Response |
| 120 | 78 | MBMS Session Update Request |
| 121 | 79 | MBMS Session Update Response |
| 128 | 80 | MS Info Change Request |
| 129 | 81 | MS Info Change Response |
| 240 | F0 | Data Record Transfer Request |
| 241 | F1 | Data Record Transfer Response |
| 254 | FE | End Marker |
| 255 | FF | G-PDU |

## GTP v2 ##

| Dec | Hex | Сообщение |
| :-- | :-- | :-------- |
| 1 | 1 | Echo Request |
| 2 | 2 | Echo Response |
| 3 | 3 | Version Not Supported |
| 32 | 20 | Create Session Request |
| 33 | 21 | Create Session Response |
| 34 | 22 | Modify Bearer Request |
| 35 | 23 | Modify Bearer Response |
| 36 | 24 | Delete Session Request |
| 37 | 25 | Delete Session Response |
| 38 | 26 | Change Notification Request |
| 39 | 27 | Change Notification Response |
| 40 | 28 | Remote UE Report Notifications |
| 41 | 29 | Remote UE Report Acknowledge |
| 64 | 40 | Modify Bearer Command |
| 65 | 41 | Modify Bearer Failure |
| 66 | 42 | Delete Bearer Command |
| 67 | 43 | Delete Bearer Failure |
| 68 | 44 | Bearer Resource Command |
| 69 | 45 | Bearer Resource Failure |
| 70 | 46 | Downlink Data Notification Failure |
| 71 | 47 | Trace Session Activation |
| 72 | 48 | Trace Session Deactivation |
| 73 | 49 | Stop Paging Indication |
| 95 | 5F | Create Bearer Request |
| 96 | 60 | Create Bearer Response |
| 97 | 61 | Update Bearer Request |
| 98 | 62 | Update Bearer Response |
| 99 | 63 | Delete Bearer Request |
| 100 | 64 | Delete Bearer Response |
| 101 | 65 | Delete PDN Request |
| 102 | 66 | Delete PDN Response |
| 103 | 67 | PGW Downlink Notification |
| 104 | 68 | PGW Downlink Acknowledge |
| 128 | 80 | Identification Request |
| 129 | 81 | Identification Response |
| 130 | 82 | Context Request |
| 131 | 83 | Context Response |
| 132 | 84 | Context Acknowledge |
| 133 | 85 | Forward Relocation Request |
| 134 | 86 | Forward Relocation Response |
| 135 | 87 | Forward Relocation Notification |
| 136 | 88 | Forward Relocation Acknowledge |
| 137 | 89 | Forward Access Notification |
| 138 | 8A | Forward Access Acknowledge |
| 139 | 8B | Relocation Cancel Request |
| 140 | 8C | Relocation Cancel Response |
| 141 | 8D | Configuration Transfer Tunnel |
| 149 | 95 | Detach Notification |
| 150 | 96 | Detach Acknowledge |
| 151 | 97 | CS Paging |
| 152 | 98 | RAN Information Relay |
| 153 | 99 | Alert MME Notification |
| 154 | 9A | Alert MME Acknowledge |
| 155 | 9B | UE Activity Notification |
| 156 | 9C | UE Activity Acknowledge |
| 157 | 9D | ISR Status |
| 160 | A0 | Create Forwarding Request |
| 161 | A1 | Create Forwarding Response |
| 162 | A2 | Suspend Notification |
| 163 | A3 | Suspend Acknowledge |
| 164 | A4 | Resume Notification |
| 165 | A5 | Resume Acknowledge |
| 166 | A6 | Create Indirect Data Tunnel Request |
| 167 | A7 | Create Indirect Data Tunnel Response |
| 168 | A8 | Delete Indirect Data Tunnel Request |
| 169 | A9 | Delete Indirect Data Tunnel Response |
| 170 | AA | Release Access Bearers Request |
| 171 | AB | Release Access Bearers Response |
| 176 | B0 | Downlink Data Notification |
| 177 | B1 | Downlink Data Acknowledge |
| 179 | B3 | PGW Restart Notification |
| 180 | B4 | PGW Restart Acknowledge |
| 200 | C8 | Update PDN Connection Request |
| 201 | C9 | Update PDN Connection Response |
| 211 | D3 | Modify Access Bearers Request |
| 212 | D4 | Modify Access Bearers Response |
| 231 | E7 | MMBS Session Start Request |
| 232 | E8 | MMBS Session Start Response |
| 233 | E9 | MMBS Session Update Request |
| 234 | EA | MMBS Session Update Response |
| 235 | EB | MMBS Session Stop Request |
| 236 | EC | MMBS Session Stop Response |

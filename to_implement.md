TODO:
* Change History;
* Changes between document issues are cumulative. The latest document issue contains all the changes made in earlier issues;
* Structure:

| Issue | Date | Description |
| :---- | :--- | :---------- |
| 01 | 2019-09-06 | First official release |

* Related Terms;
* Overview;

| Parameter ID | Meaning | Mandatory | Type | Range | Default Value | Remarks |
| :----------- | :------ | :-------- | :--- | :---- | :------------ | :------ |
| username | The username of this entry. | Yes | String | Length: 1 to 32 digits | - | - |
| password | The password of this entry. | Yes | String | Length: 6 to 32 digits | - | - |


* Troubleshooting:
  * Symptom;
  * Possible Cause;
  * Solution;

| Doc Type in English | Doc Type in Russian |
| :------------------ | :------------------ |
| AUTHENTIC_TEXT_HOLDERS_LIST | Ведомость держателей подлинников |
| SPTA_SET_LIST | Ведомость ЗИП |
| LIST_BOUGHT_IN_PAPERS | Ведомость покупных изделий |
| LIST_BOUGHT_IN_PERMISSION_PAPERS | Ведомость разрешения применения покупных изделий |
| LIST_SPECIFICATIONS | Ведомость спецификаций |
| LIST_REFERENCE_PAPERS | Ведомость ссылочных документов |
| LIST_ASSEMBLY_PAPERS | Ведомость сборки изделия |
| BASIC_DESIGN_LIST | Ведомость технического проекта |
| LIST_OPERATION_PAPERS | Ведомость эксплуатационных документов |
| OPERATION_DOCUMENTATION_TYPES | Виды для ЭД |
| OUTLINE_DRAWING | Габаритный чертеж |
| INSTRUCTIONS | Инструкции |
| SPTA_USE_INSTRUCTION | Инструкция по использованию ЗИП-О |
| MOUNTING_LAUNCH_CONFIGURATION_TRIAL_INSTRUCTION | Инструкция по монтажу, пуску, регулированию и обкатке изделия |
| MOUNTING_COMMISSIONING_INSTRUCTION | Инструкция по монтажу и вводу в эксплуатацию |
| ANTIVIRUS_PROTECTION_MANAGEMENT_INSTRUCTION | Инструкция по организации антивирусной защиты информации |
| DATABASE_MANAGEMENT_INSTRUCTION | Инструкция по формированию и ведению базы данных |
| OPERATION_INSTRUCTION | Инструкция по эксплуатации |
| PROCESS_FLOW_CHART | Карта технологического процесса |
| DATABASE_CATALOG | Каталог базы данных |
| CONFIGURATION_TEST_CASE | Контрольный пример по настройке |
| PROCESS_CHART | Маршрутная карта |
| MOUNTING_DRAWING | Монтажный чертеж |
| GENERAL_SYSTEM_DESCRIPTION | Общее описание системы |
| DATABASE_DESCRIPTION | Описание базы данных |
| PRODUCT_DESCRIPTION | Описание изделия |
| PROGRAM_DESCRIPTION | Описание программы |
| USE_DESCRIPTION | Описание применения |
| PROCESS_AND_TECHNOLOGY_DESCRIPTION | Описание технологического процесса |
| PRODUCT_CERTIFICATE | Паспорт |
| EXPLANATORY_NOTE | Пояснительная записка |
| TESTING_PROGRAMME_AND_PROCEDURE | Программа и методика испытаний |
| OTHERS | Прочие документы |
| CALCULATION | Расчеты |
| SERVICE_CHARGE_SETTLEMENT | Расчет затрат на техническое обслуживание |
| INSPECTION_AND_MAINTENANCE_GUIDE | Регламент технического обслуживания |
| ADMINISTRATOR_GUIDE | Руководство администратора |
| SECURITY_ADMINISTRATOR_GUIDE | Руководство администратора безопасности |
| SECURITY_SYSTEM_ADMINISTRATOR_GUIDE | Руководство администратора СЗИ |
| OPERATOR_GUIDE | Руководство оператора |
| SECURITY_TOOL_OPERATOR_GUIDE | Руководство оператора КСЗ |
| USER_GUIDE | Руководство пользователя |
| SECURITY_TOOL_SYSTEM_GUIDE | Руководство по КСЗ |
| CONFIGURATION_GUIDE | Руководство по настройке |
| SECURITY_SYSTEM_GUIDE | Руководство по СЗИ |
| INSTALLATION_GUIDE | Руководство по установке |
| OPERATION_GUIDE | Руководство по эксплуатации |
| INFORMATION_SECURITY_OFFICER_GUIDE | Руководство пользователя СЗИ |
| PROGRAMMER_GUIDE | Руководство программиста |
| SYSTEM_PROGRAMMER_GUIDE | Руководство системного программиста |
| ASSEMBLY_DRAWING | Сборочный чертеж |
| JOINT_RESOLUTION | Совместное решение |
| MESSAGES_OUTPUT | Состав выходных данных (сообщений) |
| SPECIFICATION | Спецификация |
| PRODUCT_COMPONENT_STRUCTURE | Схема деления структурная |
| ELECTRO_CONNECTIONS_DIAGRAM | Схема электрическая подключения |
| ELECTRO_SCHEMATIC_DIAGRAM | Схема электрическая принципиальная |
| ELECTRO_CIRCUIT_DIAGRAM | Схема электрическая соединений |
| SOURCE_CODE | Текст программы |
| TECHNICAL_DESCRIPTION | Техническое описание |
| TECHNICAL_SPECIFICATION | Технические условия |
| PROCESSING_INSTRUCTION | Технологическая инструкция |
| PACKING_DRAWING | Упаковочный чертеж |
| PRODUCT_STATUS_RECORD | Формуляр |
| DETAIL_DRAWING | Чертеж детали |
| GENERAL_ARRANGEMENT_DRAWING | Чертеж общего вида |
| LABEL | Этикетка |


| Doc Type | Short |
| :------- | :---- |
| AUTHENTIC_TEXT_HOLDERS_LIST_SHORT | 05 |
| SOURCE_CODE_SHORT | 12 |
| PROGRAM_DESCRIPTION_SHORT | 13 |
| LIST_OPERATION_PAPERS_20_SHORT | 20 |
| PRODUCT_STATUS_RECORD_30_SHORT | 30 |
| USE_DESCRIPTION_SHORT | 31 |
| SYSTEM_PROGRAMMER_GUIDE_SHORT | 32 |
| PROGRAMMER_GUIDE_SHORT | 33 |
| OPERATOR_GUIDE_SHORT | 34 |
| TESTING_PROGRAMME_AND_PROCEDURE_51_SHORT | 51 |
| EXPLANATORY_NOTE_81_SHORT | 81 |
| OTHERS_SHORT | 90-99 |
| DATABASE_CATALOG_SHORT | В7 |
| MESSAGES_OUTPUT_SHORT | В8 |
| LIST_REFERENCE_PAPERS_SHORT | ВД |
| OPERATION_DOCUMENTATION_TYPES_SHORT | ВдЭД |
| LIST_BOUGHT_IN_PERMISSION_PAPERS_SHORT | ВИ |
| GENERAL_ARRANGEMENT_DRAWING_SHORT | ВО |
| LIST_BOUGHT_IN_PAPERS_SHORT | ВП |
| LIST_ASSEMBLY_PAPERS_SHORT | ВП/ВСИ |
| LIST_SPECIFICATIONS_SHORT | ВС |
| LIST_OPERATION_PAPERS_PROGRAMMING_SHORT | ВЭ |
| OUTLINE_DRAWING_SHORT | ГЧ |
| SECURITY_SYSTEM_ADMINISTRATOR_GUIDE_SHORT | Д3 |
| INFORMATION_SECURITY_OFFICER_GUIDE_SHORT | Д4 |
| SECURITY_TOOL_SYSTEM_GUIDE_D5_SHORT | Д5 |
| SECURITY_TOOL_SYSTEM_GUIDE_D6_SHORT | Д6 |
| SECURITY_TOOL_OPERATOR_GUIDE_SHORT | Д7 |
| CONFIGURATION_TEST_CASE_SHORT | Д8 |
| JOINT_RESOLUTION_SHORT | Д9 |
| ANTIVIRUS_PROTECTION_MANAGEMENT_INSTRUCTION_SHORT | Д11 |
| PRODUCT_COMPONENT_STRUCTURE_SHORT | Е1 |
| SPTA_SET_LIST_SHORT | ЗИ |
| INSTRUCTIONS_SHORT | И |
| SPTA_USE_INSTRUCTION_SHORT | И1 |
| USER_GUIDE_SHORT | И3 |
| DATABASE_MANAGEMENT_INSTRUCTION_SHORT | И4 |
| SECURITY_ADMINISTRATOR_GUIDE_SHORT | И5 |
| MOUNTING_COMMISSIONING_INSTRUCTION_SHORT | ИМ |
| INSPECTION_AND_MAINTENANCE_GUIDE_SHORT | ИС |
| OPERATION_INSTRUCTION_SHORT | ИЭ |
| PROCESS_FLOW_CHART_SHORT | КТП |
| MOUNTING_DRAWING_SHORT | МЧ |
| PROCESS_CHART_SHORT | МК |
| PRODUCT_DESCRIPTION_SHORT | ОИ |
| DATABASE_DESCRIPTION_SHORT | ОБД |
| PROCESS_AND_TECHNOLOGY_DESCRIPTION_SHORT | ПГ |
| GENERAL_SYSTEM_DESCRIPTION_SHORT | ПД |
| EXPLANATORY_NOTE_PZ_SHORT | ПЗ |
| TESTING_PROGRAMME_AND_PROCEDURE_PM_SHORT | ПМ |
| PRODUCT_CERTIFICATE_SHORT | ПС |
| ADMINISTRATOR_GUIDE_SHORT | РА |
| INSTALLATION_GUIDE_SHORT | РИ |
| CONFIGURATION_GUIDE_SHORT | РН |
| CALCULATION_SHORT | РР |
| SERVICE_CHARGE_SETTLEMENT_SHORT | РТО |
| OPERATION_GUIDE_SHORT | РЭ |
| ASSEMBLY_DRAWING_SHORT | СБ |
| SECURITY_SYSTEM_GUIDE_SHORT | СЗИ |
| PROCESSING_INSTRUCTION_SHORT | ТИ |
| TECHNICAL_DESCRIPTION_SHORT | ТО |
| BASIC_DESIGN_LIST_SHORT | ТП |
| TECHNICAL_SPECIFICATION_SHORT | ТУ |
| PACKING_DRAWING_SHORT | УЧ |
| PRODUCT_STATUS_RECORD_FO_SHORT | ФО |
| ELECTRO_SCHEMATIC_DIAGRAM_SHORT | Э3 |
| ELECTRO_CIRCUIT_DIAGRAM_SHORT | Э4 |
| ELECTRO_CONNECTIONS_DIAGRAM_SHORT | Э5 |
| LIST_OPERATION_PAPERS_ED_SHORT | ЭД |
| LABEL_SHORT | ЭТ |

| Doc Category in English | Doc Category in Russian |
| :---------------------- | :---------------------- |
| BASIC_DESIGN | Технический проект |
| DESIGN_DOC | Конструкторская документация |
| DEVELOPMENT_DOC | Программная документация |
| OPERATION_DOC | Эксплуатационная документация |
| PRODUCTION_DOC | Технологическая документация |
| FOREIGN | Зарубежье |

RAT Types

1 — UTRAN;
2 — GERAN;
3 — WLAN;
4 — GAN;
5 — HSPA Evolution;
6 — EUTRAN (WB-E-U;TRAN);
7 — Virtual;
8 — EUTRAN-NB-IoT;


 ()
Protocol Configuration Options (PCO)
 ()
Traffic Aggregate Description (TAD)
 ()
 ()
User CSG Information (UCI)
PL (Priority Level)
PVI (Pre-emption Vulnerability)
 ()
DAF (Dual Address Bearer Flag)
DTF (Direct Tunnel Flag)
DFI (Direct Forwarding Indication)
HI (Handover Indication)
OI (Operation Indication)
ISRSI (Idle mode Signalling Reduction Supported Indication)
ISRAI (Idle mode Signalling Reduction Activation Indication)
SGWCI (SGW Change Indication)
UIMSI (Unauthenticated IMSI)
CFSI (Change F-TEID support indication)
CRSI (Change Reporting support indication)
SI (Scope Indication)
PS (Piggybacking Supported)
MSV (MS Validated):
QCI, Maximum bit rate for uplink, Maximum bit rate for downlink, Guaranteed bit rate for uplink and Guaranteed bit
rate for downlink are specified in 3GPP TS 36.413 [10]

PPC (Prohibit Payload Compression)
- NRF
- UDM
- AMF
- SMF
- AUSF
- NEF
- PCF
- SMSF
- NSSF
- UDR
- LMF
- GMLC
- 5G_EIR
- SEPP
- UPF
- N3IWF
- AF
- UDSF
- BSF
- CHF
- NWDAF

AMF, CHF, PCF, and UDM

| NBNS | NetBIOS Name Service |  | сервис для предоставления услуг сеансового уровня модели OSI, позволяя приложениям на отдельных машинах взаимодействовать через локальную сеть |
| GRE | Generic Routing Encapsulation

0 Geographic Location field included and it holds the Cell Global Figure Identification (CGI) of where the user currently is registered. 3GPP TS 23.003
1 Geographic Location field included and it holds the Service Area Identity (SAI) of where the user currently is registered. 3GPP TS 25.413
2 Geographic Location field included and it holds the Routing Area Identification (RAI) of where the user currently is registered. 3GPP TS 23.003

3GPP TS 29.274

0 — интерфейс S1-U eNodeB GTP-U;
1 — интерфейс S1-U SGW GTP-U;
2 — интерфейс S12 RNC GTP-U;
3 — интерфейс S12 SGW GTP-U;
4 — интерфейс S5/S8 SGW GTP-U;
5 — интерфейс S5/S8 PGW GTP-U;
6 — интерфейс S5/S8 SGW GTP-C;
7 — интерфейс S5/S8 PGW GTP-C;
8 — интерфейс S5/S8 SGW PMIPv6 (32-битный GRE key is encoded in 32 bit TEID field)
9 — интерфейс S5/S8 PGW PMIPv6 interface (the 32 bit GRE key is encoded in the 32 bit TEID field, see clause 6.3 in
3GPP TS 29.275 [26])
10 — интерфейс S11 MME GTP-C;
11 — интерфейс S11/S4 SGW GTP-C;
12 — интерфейс S10/N26 MME GTP-C;
13 — интерфейс S3 MME GTP-C;
14 — интерфейс S3 SGSN GTP-C;
15 — интерфейс S4 SGSN GTP-U;
16 — интерфейс S4 SGW GTP-U;
17 — интерфейс S4 SGSN GTP-C;
18 — интерфейс S16 SGSN GTP-C;
19 — интерфейс eNodeB/gNodeB GTP-U interface for DL data forwarding
20 — интерфейс eNodeB GTP-U interface for UL data forwarding
21 — интерфейс RNC GTP-U interface for data forwarding
22 — интерфейс SGSN GTP-U interface for data forwarding
23 — интерфейс SGW/UPF GTP-U interface for DL data forwarding
24 — интерфейс Sm MBMS GW GTP-C;
25 — интерфейс Sn MBMS GW GTP-C;
26 — интерфейс Sm MME GTP-C;
27 — интерфейс Sn SGSN GTP-C;
28 — интерфейс SGW GTP-U interface for UL data forwarding
29 — интерфейс Sn SGSN GTP-U;
30 — интерфейс S2b ePDG GTP-C;
31 — интерфейс S2b-U ePDG GTP-U;
32 — интерфейс S2b PGW GTP-C;
33 — интерфейс S2b-U PGW GTP-U;
34 — интерфейс S2a TWAN GTP-U;
35 — интерфейс S2a TWAN GTP-C;
36 — интерфейс S2a PGW GTP-C;
37 — интерфейс S2a PGW GTP-U;
38 — интерфейс S11 MME GTP-U;
39 — интерфейс S11 SGW GTP-U;
40 — интерфейс N26 AMF GTP-C;
41 — интерфейс N19mb UPF GTP-U;




IPCP -> Type --- такие значения:
3 — IP-адрес
4 — Mobile-IPv4
129 — Primary DNS Server Address
130 — Primary NBNS Server Address
131 — Secondary DNS Server Address
132 — Secondary NBNS Server Address



NRF
UDM
AMF
SMF
AUSF
NEF
PCF
SMSF
NSSF
UDR
LMF
GMLC
5G_EIR
SEPP
UPF
N3IWF
AF
UDSF
BSF
CHF
NWDAF


nnrf-nfm
nnrf-disc
nudm-sdm
nudm-uecm
nudm-ueau
nudm-ee
nudm-pp
namf-comm
namf-evts
namf-mt
namf-loc
nsmf-pdusession
nsmf-event-exposure
nausf-auth
nausf-sorprotection
nausf-upuprotection
nnef-pfdmanagement
npcf-am-policy-control
npcf-smpolicycontrol
npcf-policyauthorization
npcf-bdtpolicycontrol
npcf-eventexposure
npcf-ue-policy-control
nsmsf-sms
nnssf-nsselection
nnssf-nssaiavailability
nudr-dr
nlmf-loc
n5g-eir-eic
nbsf-management
nchf-spendinglimitcontrol
nchf-convergedcharging
nnwdaf-eventssubscription
nnwdaf-analyticsinfo


Nnrf_NFManagement 3GPP TS 29.510
Nnrf_NFDiscovery 3GPP TS 29.510
Nudm_SubscriberDataManagement 3GPP TS 29.503
Nudm_UEContextManagement 3GPP TS 29.503
Nudm_UEAuthentication 3GPP TS 29.503
Nudm_EventExposure 3GPP TS 29.503
Nudm_ParameterProvision 3GPP TS 29.503
Namf_Communication 3GPP TS 29.518
Namf_EventExposure 3GPP TS 29.518
Namf_MT 3GPP TS 29.518
Namf_Location 3GPP TS 29.518
Nsmf_PDUSession 3GPP TS 29.502
Nsmf_EventExposure 3GPP TS 29.502
Nausf_UEAuthentication 3GPP TS 29.509
Nausf_SoRProtection 3GPP TS 29.509
Nnef_PFDManagement 3GPP TS 29.551
Npcf_AMPolicyControl 3GPP TS 29.507
Npcf_SMPolicyControl 3GPP TS 29.507
Npcf_PolicyAuthorization 3GPP TS 29.507
Npcf_BDTPolicyControl 3GPP TS 29.507
Nsmsf_SMService 3GPP TS 29.540
Nnssf_NSSelection 3GPP TS 29.531
Nnssf_NSSAIAvailability 3GPP TS 29.531
Nudr_DataRepository 3GPP TS 29.504
Nlmf_Location 3GPP TS 29.572
N5g-eir_EquipmentIdentityCheck 3GPP TS 29.511
Nbsf_Management 3GPP TS 29.521
Nchf_SpendingLimitControl 3GPP TS 29.594
Nnwdaf_EventsSubscription 3GPP TS 29.520
Nnwdaf_AnalyticsInfo 3GPP TS 29.520

SelMode := ChosenBySGSN: Network-provided APN, subscription not verified.
SelMode := SentByMS: MS-provided APN, subscription not verified.
SelMode := Subscribed: MS or Network-provided APN, subscription verified

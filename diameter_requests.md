# Сообщения Diameter для различных интерфейсов #

## Diameter: S6b, Auth-Application-Id = 16777272 ##

| Сообщение | Сокр. | Описание | Код сообщения | бит R, Флаг | Направление передачи |
| :-------- | :---- | :------- | :------------ | :---------- | :------------------- |
| Diameter-EAP-Request | DER | Запрос EAP-сессии | 268 | 1 | PGW --> 3GPP AAA-сервер |
| Diameter-EAP-Answer | DEA | Запрос EAP-сессии | 268 | очищен | 3GPP AAA-сервер --> PGW |
| AA-Request | AAR | Информация о сессии | 265 | 1 | PGW --> 3GPP AAA-сервер |
| АА-Answer | AAA | Информация о сессии | 265 | очищен | 3GPP AAA-сервер --> PGW |
| Session-Termination-Request | STR | Окончание сессии | 275 | 1 | 3GPP AAA-сервер <--> PGW |
| Session-Termination-Answer | STA | Окончание сессии | 275 | очищен | 3GPP AAA-сервер <--> PGW |
| Abort-Session-Request | ASR | Аварийное прекращение сессии | 274 | 1 | 3GPP AAA-сервер --> PGW |
| Abort-Session-Answer | ASA | Аварийное прекращение сессии | 274 | очищен | PGW --> 3GPP AAA-сервер |
| Re-Auth-Request | RAR | Обновление данных авторизации | 258 | 1 | 3GPP AAA-сервер/прокси --> PGW |
| Re-Auth-Answer | RAA | Обновление данных авторизации | 258 | очищен | PGW --> 3GPP AAA-сервер/прокси |

## Diameter: SWx, Auth-Application-Id = 16777265 ##

| Сообщение | Сокр. | Описание | Код сообщения | бит R, Флаг | Направление передачи |
| :-------- | :--------- | :------- | :------------ | :---------- | :------------------- |
| Multimedia-Authentication-Request | MAR | Аутентификация при мультимедийной сессии | 303 | 1 | 3GPP AAA-сервер --> HSS |
| Multimedia-Authentication-Answer | МАА | Аутентификация при мультимедийной сессии | 303 | очищен | HSS --> 3GPP AAA-сервер |
| Push-Profile-Request | PPR | Обновление профилей | 305 | 1 | HSS --> 3GPP AAA-сервер |
| Push-Profile-Answer | РРА | Обновление профилей | 305 | очищен | 3GPP AAA-сервер --> HSS |
| Server-Assignment-Request | SAR | Регистрация сервер-сервер | 301 | 1 | 3GPP AAA-сервер --> HSS |
| Server-Assignment-Answer | SAA | Регистрация сервер-сервер | 301 | очищен | HSS --> 3GPP AAA-сервер |
| Registration-Termination-Request | RTR | Окончание регистрации | 304 | 1 | 3GPP AAA-сервер <--> HSS |
| Registration-Termination-Answer | RTA | Окончание регистрации | 304 | очищен | 3GPP AAA-сервер <--> HSS |

## Diameter: SWm, Auth-Application-Id = 16777264 ##

| Сообщение | Сокр. | Описание | Код сообщения | бит R, Флаг | Направление передачи |
| :-------- | :--------- | :------- | :------------ | :---------- | :------------------- |
| Diameter-EAP-Request | DER | Запрос EAP-сессии | 268 | 1 | ePDG --> 3GPP AAA-сервер/прокси |
| Diameter-EAP-Answer | DEA | Запрос EAP-сессии | 268 | очищен | 3GPP AAA-сервер/прокси --> ePDG |
| AA-Request | AAR | Информация о сессии | 265 | 1 | ePDG --> 3GPP AAA-сервер/прокси |
| АА-Answer | AAA | Информация о сессии | 265 | очищен | 3GPP AAA-сервер/прокси --> ePDG |
| Session-Termination-Request | STR | Окончание сессии | 275 | 1 | ePDG --> 3GPP AAA-сервер/прокси |
| Session-Termination-Answer | STA | Окончание сессии | 275 | очищен | 3GPP AAA-сервер/прокси --> ePDG |
| Abort-Session-Request | ASR | Аварийное прекращение сессии | 274 | 1 | 3GPP AAA-сервер/прокси --> ePDG |
| Abort-Session-Answer | ASA | Аварийное прекращение сессии | 274 | очищен | ePDG --> 3GPP AAA-сервер/прокси |
| Re-Auth-Request | RAR | Обновление данных авторизации | 258 | 1 | 3GPP AAA-сервер/прокси --> ePDG |
| Re-Auth-Answer | RAA | Обновление данных авторизации | 258 | очищен | ePDG --> 3GPP AAA-сервер/прокси |

## Diameter: STa, Auth-Application-Id = 16777250 ##
| Сообщение | Сокр. | Описание | Код сообщения | бит R, Флаг | Направление передачи |
| :-------- | :--------- | :------- | :------------ | :---------- | :------------------- |
| Diameter-EAP-Request | DER | Запрос EAP-сессии | 268 | 1 | TWAN --> 3GPP AAA-сервер |
| Diameter-EAP-Answer | DEA | Запрос EAP-сессии | 268 | очищен | 3GPP AAA-сервер --> TWAN |
| Abort-Session-Request | ASR | Аварийное прекращение сессии | 274 | 1 | 3GPP AAA-сервер/прокси --> TWAN |
| Abort-Session-Answer | ASA | Аварийное прекращение сессии | 274 | очищен | TWAN --> 3GPP AAA-сервер/прокси |
| Session-Termination-Request | STR | Окончание сессии | 275 | 1 | TWAN --> 3GPP AAA-сервер/прокси |
| Session-Termination-Answer | STA | Окончание сессии | 275 | очищен | 3GPP AAA-сервер/прокси --> TWAN |
| Re-Auth-Request | RAR | Обновление данных авторизации | 258 | 1 | 3GPP AAA-сервер/прокси --> TWAN |
| Re-Auth-Answer | RAA | Обновление данных авторизации | 258 | очищен | TWAN --> 3GPP AAA-сервер/прокси |
| AA-Request | AAR | Информация о сессии | 265 | 1 | TWAN --> 3GPP AAA-сервер/прокси |
| АА-Answer | AAA | Информация о сессии | 265 | очищен | 3GPP AAA-сервер/прокси --> TWAN |

## Diameter: SWa, Auth-Application-Id = 16777250 ##

| Сообщение | Сокр. | Описание | Код сообщения | бит R, Флаг | Направление передачи |
| :-------- | :--------- | :------- | :------------ | :---------- | :------------------- |
| Diameter-EAP-Request | DER | Запрос EAP-сессии | 268 | 1 | UTWAN --> 3GPP AAA-сервер |
| Diameter-EAP-Answer | DEA | Запрос EAP-сессии | 268 | очищен | 3GPP AAA-сервер --> UTWAN |
| Abort-Session-Request | ASR | Аварийное прекращение сессии | 274 | 1 | 3GPP AAA-сервер/прокси --> UTWAN |
| Abort-Session-Answer | ASA | Аварийное прекращение сессии | 274 | очищен | UTWAN --> 3GPP AAA-сервер/прокси |
| Session-Termination-Request | STR | Окончание сессии | 275 | 1 | UTWAN --> 3GPP AAA-сервер/прокси |
| Session-Termination-Answer | STA | Окончание сессии | 275 | очищен | 3GPP AAA-сервер/прокси --> UTWAN |
| Re-Auth-Request | RAR | Обновление данных авторизации | 258 | 1 | 3GPP AAA-сервер/прокси --> UTWAN |
| Re-Auth-Answer | RAA | Обновление данных авторизации | 258 | очищен | UTWAN --> 3GPP AAA-сервер/прокси |
| AA-Request | AAR | Информация о сессии | 265 | 1 | UTWAN --> 3GPP AAA-сервер/прокси |
| АА-Answer | AAA | Информация о сессии | 265 | очищен | 3GPP AAA-сервер/прокси --> UTWAN |
| Re-Auth-Answer | RAA | Обновление данных авторизации | 258 | очищен | UTWAN --> 3GPP AAA-сервер/прокси |

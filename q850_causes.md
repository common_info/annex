| Код | Описание | Название |
| :-- | :------- | :------- |
| 1 | Unallocated (unassigned) number | Неназначенный номер |
| 2 | No route to specified transit network | Отсутствует маршрут для указанной транзитной сети |
| 3 | No route to destination | Маршрут до адресата отсутствует |
| 6 | Channel unacceptable | Недопустимый канал |
| 7 | Call awarded and being delivered in an established channel | Вызов назначен и доставлен |
| 16 | Normal call clearing | Обычное разъединение соединения |
| 17 | User busy | Пользователь занят |
| 18 | No user responding | Нет ответа от пользователя |
| 19 | No answer from user (user alerted) | Нет ответа от пользователя (пользователь оповещен) |
| 21 | Call rejected | Вызов отклонен |
| 22 | Number changed | Номер изменился |
| 26 | Non-selected user clearing | Очистка невыбранного пользователя |
| 27 | Destination out of order | Узел назначения неисправен |
| 28 | Invalid number format (address incomplete) | Неправильный формат номера |
| 29 | Facility rejected | Услуга отклонена |
| 30 | Response to STATUS ENQUIRY | Ответ на STATUS ENQUIRY |
| 31 | Normal, unspecified | Обычное событие, причина не указана |
| 34 | No circuit/channel available | Нет свободной линии или канала |
| 38 | Network out of order | Сеть неисправна |
| 41 | Temporary failure | Временный сбой |
| 42 | Switching equipment congestion | Перегрузка коммутационного оборудования |
| 43 | Access information discarded | Информация о доступе отклонена |
| 44 | Requested circuit/channel not available | Запрашиваемая линия или канал недоступны |
| 47 | Resource unavailable, unspecified | Ресурс недоступен, причина не указана |
| 49 | Quality of service not available | Качество услуги недоступно |
| 50 | Requested facility not subscribed | Нет подписки на запрашиваемую услугу |
| 57 | Bearer capability not authorized | Пропускная способность канала не разрешена |
| 58 | Bearer capability not presently available | Пропускная способность канала в настоящий момент недоступна |
| 63 | Service or option not available, unspecified | Услуга или функция недоступны, причина не указана |
| 65 | Bearer capability not implemented | Пропускная способность канала не обеспечивается |
| 66 | Channel type not implemented | Тип канала не обеспечивается |
| 69 | Requested facility not implemented | Запрашиваемая услуга не обеспечивается |
| 70 | Only restricted digital information bearer capability is available | Доступен только канал ограниченных цифровых данных |
| 79 | Service or option not implemented, unspecified | Услуга или функция недоступны, причина не указана |
| 81 | Invalid call reference value | Неверное значение ссылки вызова |
| 82 | Identified channel does not exist | Идентифицированный канал не существует |
| 83 | A suspended call exists, but this call identity does not | Приостановленное соединение существует, но оно не идентифицируется |
| 84 | Call identity in use | Идентификатор соединения используется |
| 85 | No call suspended | Нет приостановленных соединений |
| 86 | Call having the requested call identity has been cleared | Соединение с запрошенным идентификатором прекращено |
| 88 | Incompatible destination | Несовместимое назначение |
| 91 | Invalid transit network selection | Указана неверная транзитная сеть |
| 95 | Invalid message, unspecified | Недопустимое сообщение, причина не указана |
| 96 | Mandatory information element is missing | Обязательный информационный элемент пропущен |
| 97 | Message type non-existent or not implemented | Тип сообщения не существует или не обеспечивается |
| 98 | Message not compatible with call state or message type non-existent or not implemented | Сообщение несовместимо с состоянием вызова, или тип сообщения не существует |
| 99 | Information element/parameter non-existent or not implemented | Элемент информации не существует или не введен в действие |
| 100 | Invalid information element contents Information element identifier(s) | Неверное содержимое элемента информации |
| 101 | Message not compatible with call state | Сообщение несовместимо с состоянием соединения |
| 102 | Recovery on timer expiry | Восстановление по истечении времени таймера |
| 111 | Protocol error, unspecified | Ошибка протокола, причина не указана |
| 127 | Interworking, unspecified | Межсетевое взаимодействие, причина не указана |

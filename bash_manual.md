# Bash Commands

`man <command>`<br/>
manual, вывести мануал на экран<br>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# man mkdir
MKDIR(1)                         User Commands                        MKDIR(1)

NAME
       mkdir - make directories

SYNOPSIS
       mkdir [OPTION]... DIRECTORY...

DESCRIPTION
       Create the DIRECTORY(ies), if they do not already exist.

       Mandatory  arguments  to  long  options are mandatory for short options too.

       -m, --mode=MODE
              set file mode (as in chmod), not a=rwx - umask

       -p, --parents
              no error if existing, make parent directories as needed

       -v, --verbose
              print a message for each created directory

 Manual page mkdir(1) line 1 (press h for help or q to quit)
```
  </p>
</details>
<br/>

`pwd` <br/>
print working directory, вывод полного пути до текущей директории<br/>
<details>
  <summary>Пример</summary>
  <p>
```
zbash# pwd
/Users/user
```
  </p>
</details>
<br/>

`ls`  <br/>
list the content, отобразить содержимое директории<br/>
<details>
  <summary>Пример</summary>
  <p>
```
zbash# ls
.localized	Shared		user
```
  </p>
</details>
<br/>

`ls -a`<br/>
list all files, отобразить все содержимое директории, включая скрытые файлы<br>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# ls -a
.		..		.localized	Shared		user
```
  </p>
</details>
<br/>

`ls -l`<br/>
list files in detail, отобразить подробную информацию о содержимом директории<br>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# ls -l
total 0
-rw-r--r--   1 root           wheel     0 Jan  1  2020 .localized
drwxrwxrwt   9 root           wheel   288 Jan  1  2020 Shared
drwxr-xr-x+ 42 AndrewTarasov  staff  1344 May  2 21:44 user
```
  </p>
</details>
<br/>

`cd <path>`<br/>
change directory, перейти в другую директорию<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# cd Applications/Mattermost.app
```
  </p>
</details>
<br>

`cd ..`<br/>
change directory to the parent one, перейти в директорию на уровень выше, в родительскую
<details>
  <summary>Пример</summary>
  <p>

```
zbash# cd ..
/Applications
```
  </p>
</details>
<br/>

`cd /`<br/>
change the directory to the root, перейти в корневую директорию
<details>
  <summary>Пример</summary>
  <p>

```
zbash# cd /
```
</p>
</details>
<br/>

`cd ~`<br/>
change the directory to the home, перейти в директорию, которая задана как домашняя
<details>
  <summary>Пример</summary>
  <p>

  ```
zbash# cd ~
```
  </p>
</details>
<br/>

> Примечание.
> Допускается комбинировать "..", "~", "/" и названия директорий.

`mkdir <dirName>`<br/>
make a directory, создать директорию<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# mkdir NewFolder
```
  </p>
</details>
<br/>

`mkdir -p <dirName>`<br/>
make a directory of that path, создать директорию по этому пути, создавая не существующие директории<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# mkdir -p ./NewFolder2/NewFolder
```

  </p>
</details>
<br/>

`mv <file> <newPlace>`<br/>
move the file, переместить файл<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# mv NewFolder ./Chemistry
```
  </p>
</details>
<br/>

`mv <file> <newName>`<br/>
rename the file, переименовать файл<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# mv NewFolder newName
```
  </p>
</details>
<br/>

`cp <file> <path>`<br/>
copy the file, копировать файл<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# cp NewFolder Chemistry
```
  </p>
</details>
<br/>

`rm <file>`<br/>
remove the item, удалить элемент
<details>
  <summary>Пример</summary>
  <p>

```
zbash# rm text_file.txt
```
</p>
</details>
<br/>

`rm -i <file>`<br/>
remove with the Information table, удалить элемент с подтверждением
<details>
  <summary>Пример</summary>
  <p>

```
zbash# rm -i text_file.txt
remove text_file.txt? y
```
</p>
</details>
<br/>

`rmdir <emptyDir>`<br/>
remove the empty directory, удалить пустую директорию
<details>
  <summary>Пример</summary>
  <p>

```
zbash# rmdir NewFolder2
```
</p>
</details>
<br/>

`rm -rf <dirName>`<br/>
ReMove any DIRectory using the command Recursively and Forcing, удалить директорию, применяя команду принудительно и рекурсивно к файлам внутри папки<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# rm -rf NewFolder
```
  </p>
</details>
<br/>

`apt-get <command>`<br/>
Application Package Tool, программа для работы с пакетами приложений<br/>

`apt-get install <package>`<br/>
install the application, установить пакет<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# apt-get install wget
```
  </p>
</details>
<br/>

`apt-get search <package>`<br/>
search the application, найти пакет<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# apt-get search wget
```
  </p>
</details>
<br/>

`!!`<br/>
repeat the last command, повторить последнюю команду<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# cd ..
zbash# !!
cd ..
```
  </p>
</details>
<br/>

> Примечание.
> Допускается комбинировать с другими командами.

<details>
  <summary>Пример</summary>
  <p>

```
zbash# cd ..
zbash# !!/Users
cd ../Users
```
  </p>
</details>
<br/>

`ps`<br/>
show current user processes, показать все процессы пользователя<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# ps
PID  TTY        TIME    CMD
2468 ttys000    0:00.22 -zsh
8390 ttys000    0:00.15 /Library/Developer/CommandLineTools/usr/bin/git -C /us
8391 ttys000    0:14.15 /Library/Developer/CommandLineTools/usr/libexec/git-co
8452 ttys000    0:28.52 /Library/Developer/CommandLineTools/usr/libexec/git-co
8407 ttys001    0:00.14 -zsh
```
  </p>
</details>
<br/>

`kill <PID>` / `kill -15 <PID>`<br/>
kill the process with TERM, убить процесс с помощью принудительного завершения задачи<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# kill 8525
zbash# kill -15 8526 8527
```
  </p>
</details>
<br/>

`kill -9 <PID>`<br/>
kill the process with SIGKILL, убить процесс сигналом SIGKILL, который имеет наивысший приоритет<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# kill -9 8528
```
  </p>
</details>
<br/>

`sudo <command>`<br/>
become a super user for one command, запустить команду от имени суперпользователя, вводится пароль учетной записи, под которой зашли в систему<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# sudo su
Password:
```
  </p>
</details>
<br/>

`su`<br/>
switch to the superuser, войти в систему как суперпользователь, вводится пароль от учетной записи суперпользователя<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# su
Password:
```
  </p>
</details>
<br/>

`exit`<br/>
leave any shell, выйти из любой оболочки<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# ssh support@192.168.99.101
[support@99.101] exit
zbash#
```
  </p>
</details>
<br/>

`clear`<br/>
move the terminal to the top, переместить строку терминала на самый верх<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# clear
```
  </p>
</details>
<br/>

`<command> -h` / `<command> --help`<br/>
help information for the command, вывести справочную информацию<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# man --help
man, version 1.6g

usage: man [-adfhktwW] [section] [-M path] [-P pager] [-S list]
	[-m system] [-p string] name ...

  a : find all matching entries
  c : do not use cat file
  d : print gobs of debugging information
  D : as for -d, but also display the pages
  f : same as whatis(1)
  h : print this help message
  k : same as apropos(1)
  K : search for a string in all pages
  t : use troff to format pages for printing
  w : print location of man page(s) that would be displayed
      (if no name given: print directories that would be searched)
  W : as for -w, but display filenames only

  C file   : use `file' as configuration file
  M path   : set search path for manual pages to `path'
  P pager  : use program `pager' to display pages
  S list   : colon separated section list
  m system : search for alternate system's man pages
  p string : string tells which preprocessors to run
               e - [n]eqn(1)   p - pic(1)    t - tbl(1)
               g - grap(1)     r - refer(1)  v - vgrind(1)
```
  </p>
</details>
<br/>

`cat <file>`<br/>
concatenate the list of files, вывести содержимое нескольких файлов на одном экране<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# cat TextFile.txt TextFile2.txt
Text 1 Line 1
Text 1 Line N
Text 2 Line 1
Text 2 Line M
```
  </p>
</details>
<br/>

`less <textfile>`<br/>
show text file, отобразить содержимое текстового файла<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# less TextFile.txt
Text 1 Line 1
Text 1 Line N
```
  </p>
</details>
<br/>

`nano`<br/>
minimalistic command-line text editor, минималистичный GUI-текстовый редактор в командной строке<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# nano
GNU nano 2.0.6                New Buffer

^G Get Help  ^O WriteOut  ^R Read File ^Y Prev Page ^K Cut Text  ^C Cur Pos
^X Exit      ^J Justify   ^W Where Is  ^V Next Page ^U UnCut Text^T To Spell
```
  </p>
</details>
<br/>

`top`<br/>
the information about the processes, вывод информации о текущих процессах и состоянии системы<br/>
<details>
  <summary>Пример</summary>
  <p>

```Processes: 264 total, 7 running, 4 stuck, 253 sleeping, 1883 threads   22:24:02
Load Avg: 2.93, 3.28, 3.74  CPU usage: 57.7% user, 10.27% sys, 32.64% idle
SharedLibs: 199M resident, 51M data, 14M linkedit.
MemRegions: 317181 total, 4194M resident, 35M private, 364M shared.
PhysMem: 8042M used (2667M wired), 149M unused.
VM: 1425G vsize, 2305M framework vsize, 537950136(4960) swapins, 542685115(16345
Networks: packets: 105734371/137G in, 29520336/3566M out.
Disks: 126839932/2682G read, 42074223/2487G written.

PID    COMMAND      %CPU  TIME     #TH   #WQ  #PORTS  MEM    PURG  CMPRS  PGRP
1513   Atom Helper  209.9 05:27:26 23/4  1    192     3249M- 0B    236M-  1468
0      kernel_task  17.0  31:26:32 176/4 0    0       612M-  0B    0B     0
135    WindowServer 11.8  125 hrs  13    6    2913-   349M+  152K- 115M-  135
2461   Terminal     9.2   01:47.76 9     3    502-    34M-   868K+ 17M-   2461
10076  top          7.9   00:01.38 1/1   0    26      7528K  0B    0B     10076
9761   gamecontroll 1.3   00:09.88 5/2   4/1  67      1460K+ 0B    532K   9761
2408   plugin-conta 0.8   06:08:46 30    1    3399    787M   0B    741M-  2391
1468   Atom         0.8   06:24:02 34    2    416     116M+  0B    59M    1468
1472   ControlCente 0.7   09:33.92 5     2    560+    39M-   0B    28M-   1472
1588   firefox      0.5   87:15:06 93    5    131476+ 1795M+ 0B    1586M- 1588
74     powerd       0.5   10:27.91 4     3    127+    2540K+ 0B    992K-  74
2391   Ghostery     0.3   51:55:36 86    5    754+    1027M+ 0B    853M-  2391
2404   plugin-conta 0.3   04:12:24 33    1    1324    377M   0B    362M   2391
2400   plugin-conta 0.3   05:08:39 30    1    2189    999M   0B    937M-  2391
61     UserEventAge 0.3   11:24.74 6     3    701+    2972K+ 0B    1596K- 61
522    corespotligh 0.3   00:39.11 7     5    131+    6404K+ 0B    5500K- 522
```
  </p>
</details>
<br/>

`sh <pathScript>`<br/>
execute the shell script, выполнить скрипт оболочки<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# sh /usr/local/Homebrew/Library/Homebrew/brew.sh
```
  </p>
</details>
<br/>

`locate <file>`<br/>
cached list of searching files, fast and outdated, поиск файла по сохраненной базе файлов, быстро, но не отображает последние изменения<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# locate temp
/Users/user/Desktop/temp
```
  </p>
</details>
<br/>

`locate -i <file>`<br/>
cached list of searching files, fast and outdated and ignore the case, поиск файла по сохраненной базе файлов без учета регистра, быстро, но не отображает последние изменения<br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# locate -i temp
/Users/user/Desktop/temp
/Users/user/Desktop/Template_guide.dotm
```
  </p>
</details>
<br/>

`find <path> -name "<fileName>"`<br/>
finds actual files, slow and up-to-date, <br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# find /Users/user/Desktop -iname "temp*"
/Users/user/Desktop/temp
/Users/user/Desktop/Template_guide.dotm
/Users/user/Desktop/Work/Templates
```
  </p>
</details>
<br/>

`find <path> -iname "<fileName>"`<br/>
finds actual files, slow and up-to-date and ignore case, <br/>
<details>
  <summary>Пример</summary>
  <p>

```
zbash# find /Users/user/Desktop -iname "temp*"
/Users/user/Desktop/temp
/Users/user/Desktop/Template_guide.dotm
/Users/user/Desktop/Work/Templates
```
  </p>
</details>
<br/>

`wget <URL>`<br/>
download the file, загрузить файл<br/>
<details>
  <summary>Пример</summary>
  <p>

```
wget http://example.com/example.png
```
  </p>
</details>
<br/>

`wget -r <URL>`<br/>
download the file recursively, загрузить файл рекурсивно<br/>
<details>
  <summary>Пример</summary>
  <p>

```
wget -r http://example.com/example/
```
  </p>
</details>
<br/>

`grep '<pattern>' <file>`<br/>
search Globally for regular expressions and patterns, глобальный поиск по содержимому с помощью регулярных выражений и масок<br/>
<details>
  <summary>Пример</summary>
  <p>

```
grep 'Line \*' /Users/user/Desktop/TextFile.txt
```
  </p>
</details>
<br/>

`grep -i '<pattern>' <file>` / `grep --ignore-case '<pattern>' <file>`<br/>
поиск без учета регистра, по умолчанию регистрозависимый<br/>
`grep -H '<pattern>' <file>`<br/>
выводить название файла вместе с найденными строками<br/>
`grep -h '<pattern>' <file>` / `grep --no-filename '<pattern>' <file>`<br/>
не выводить названия файлов, где надены подходящие строки<br/>
`grep -a '<pattern>' <file>` / `grep --text '<pattern>' <file>`<br/>
рассматривать все файлы как ASCII, в том числе и бинарные<br/>
`grep -n '<pattern>' <file>` / `grep --line-number '<pattern>' <file>`<br/>
указать номер строки, где встречается искомое выражение, для каждого файла, начиная с 1, строки каждого файла нумеруются отдельно<br/>
`grep --null '<pattern>' <file>`<br/>
напечатать \0 после названия файла<br/>
`grep -R '<pattern>' <file>` / `grep -r '<pattern>' <file>` /
`grep --recursive '<pattern>' <file>`<br/>
искать рекурсивно по всем поддиректориям<br/>
`grep -v '<pattern>' <file>` / `grep --invert-match '<pattern>' <file>`<br/>
вывести строки, в которых отсутствует указанное выражение<br/>


* `ls` — list directory contents;

```
username@system ~ % ls -a
Applications	Volumes		etc			sbin
Library			bin			home		tmp
System			cores		opt			usr
Users			dev			private		var
```

* `ls -a` - list directory contents including hidden files;

```
username@system ~ % ls -a
.					System			home
..					Users			opt
.VolumeIcon.icns	Volumes			private
.file				bin				sbin
.vol				cores			tmp
Applications		dev				usr
Library				etc				var
```

* `cd <dir>` — change directory;

```
username@system ~ % cd Documents
username@system Documents % 
```

* `cd ..` - change directory to the parent;

```
username@system ~ % cd ..
username@system /usr %
```

* `cd /` - change directory to the root;

```
username@system ~ % cd /
username@system / %
```

* `cd ~` - change directory to the home;

```
username@system / % cd ~
username@system ~ %
```

* `pwd` — print name of current directory;

```
username@system ~ % pwd
/Users/user
```

* `mkdir <dir>` — make directory;

```
username@system ~ % mkdir new_folder
```

* `rmdir <dir>` — remove empty directory;

```
username@system ~ % mkdir new_folder
```

* `rm <file_dir>` — remove file or directory;

```
username@system ~ % rm file
```

* `rm -r <file_dir>` - remove file or directory and all its contents;

```
username@system ~ % rm -r folder
```

* `rm -f <file_dir>` - remove file or directory forcely;

```
username@system ~ % rm -f folder
```

* `cp <file_dir_from> <file_dir_to>` — copy file and directory;

```
username@system ~ % cp file1 file2
```

* `cp -f <file_dir_from> <file_dir_to>` — copy file or directory and all its contents;

```
username@system ~ % cp -r folder1 folder2
```

* `mv <file_from> <file_to>` — move (rename) files

```
username@system ~ % mv file1 file2
```

```
# cat vs more vs less vs head vs tail vs grep

# cat: print the whole file
# more: print the file page by page
# less: print the file page by page
# head: print the first 10 lines of the file
# tail: print the last 10 lines of the file
# grep: print lines matching a pattern
```

* `cat <file1> <fileN>` — concatenate files and print on the standard output;

```
username@system ~ % cat file1 file2
file_content_1
file_content_2
```


* `more <file>` — page through a file;

```
username@system ~ % more file
file_content
```

* `less <file>` — page through a file;

```
username@system ~ % less file
file_content
```

* head — output the first part of files

$ head test
$ Hello World!

13. tail — output the last part of files

$ tail test
$ Hello World!

* `wc <file1> <fileN>` — print newline, word, and byte counts for each file
  * c - the number of bytes;
  * l - the number of lines;
  * m - the number of characters;
  * w - the number of words;

```
username@system Desktop % wc -clmw file1 file2
     <bytes>     <lines>    <chars> <name>
     <bytes>     <lines>    <chars> total
```

17. sort — sort lines of text files

$ sort test
$ Hello World!

18. uniq — report or omit repeated lines

$ uniq test
$ Hello World!

19. find — search for files in a directory hierarchy

$ find . -name test
$ ./test

20. diff — compare files line by line

$ touch test2
$ diff test test2
$ 1c1
$ < Hello World!

* `chmod <mode> <file>` — change file mode bits;

#change file mode bits to 777
$ chmod 777 test

22. chown — change file owner and group

#change file owner to username
$ chown username test

23. chgrp — change group ownership

#change file group to username
$ chgrp username test

24. ln — make links between files

#create a symbolic link named test2 to test
$ ln -s test test2

25. df — report file system disk space usage

$ df
$ Filesystem 1K-blocks Used Available Use% Mounted on

```
username@system ~ % df
Filesystem     512-blocks      Used Available Capacity iused     ifree %iused  Mounted on
/dev/disk1s5s1  236568496  46477472  40452696    54%  500637 202263480    0%   /
devfs                 669       669         0   100%    1158         0  100%   /dev
/dev/disk1s4    236568496  12585672  40452696    24%       9 202263480    0%   /System/Volumes/VM
/dev/disk1s2    236568496   1155016  40452696     3%    3517 202263480    0%   /System/Volumes/Preboot
/dev/disk1s6    236568496    231360  40452696     1%     408 202263480    0%   /System/Volumes/Update
/dev/disk1s1    236568496 133250264  40452696    77%  929354 202263480    0%   /System/Volumes/Data
map auto_home           0         0         0   100%       0         0  100%   /System/Volumes/Data/home
/dev/disk1s5    236568496  46477472  40452696    54%  502146 202263480    0%   /System/Volumes/Update/mnt1

```

26. du — estimate file space usage

$ du
$ 4 ./test

27. free — display amount of free and used memory in the system

$ free
$ total used free shared buff/cache available

for mac users, use sysctl vm.swapusage

```
username@system ~ % top

Processes: 430 total, 2 running, 428 sleeping, 2202 threads            23:14:08
Load Avg: 2.84, 3.22, 3.61  CPU usage: 14.65% user, 8.83% sys, 76.51% idle
SharedLibs: 251M resident, 44M data, 16M linkedit.
MemRegions: 128633 total, 1262M resident, 55M private, 429M shared.
PhysMem: 8048M used (2038M wired), 144M unused.
VM: 14T vsize, 3136M framework vsize, 96703935(0) swapins, 101177866(0) swapouts
Networks: packets: 97016329/124G in, 42540477/3701M out.
Disks: 668255488/3137G read, 23349035/589G written.

PID    COMMAND      %CPU TIME     #TH   #WQ  #PORT MEM    PURG   CMPRS  PGRP
98337  Terminal     22.9 00:39.31 9     3    502   42M-   456K+  5696K  98337
159    WindowServer 12.2 44:50:02 13    5    1803  386M+  2780K+ 90M    159

```


29. kill — kill a process

#kill process with pid 1234
$ kill 1234

* ps — report a snapshot of the current processes

```
username@system ~ % ps
  PID TTY           TIME CMD
98594 ttys000    0:00.07 -zsh
```

31. sudo — execute a command as another user

#install vim
$ sudo apt-get install vim

32. su — become another user

#become user username
$ su username

33. apt-get — install, remove, or update software packages

#install vim
$ apt-get install vim

34. apt-cache — search for packages

#search for vim
$ apt-cache search vim

35. apt — commandline package manager

#install vim
$ apt install vim

36. yum — install, remove, or update software packages

#install vim
$ yum install vim

37. yum — search for packages

#search for vim
$ yum search vim

38. wget — non-interactive network downloader

#download google.com
$ wget https://www.google.com

39. curl — transfer a URL

#download google.com
$ curl https://www.google.com

40. tar — create, extract, or list files from a tar file

#extract test.tar
$ tar -xvf test.tar

41. gzip — compress or expand files

#compress test
$ gzip test

42. gunzip — uncompress .gz files

#uncompress test.gz
$ gunzip test.gz

43. zip — package and compress (archive) files

#compress test to test.zip
$ zip test.zip test

44. unzip — extract and list files from a ZIP archive

#extract test.zip
$ unzip test.zip

45. ssh — secure shell (remote login program)

#login to host as username
$ ssh username@host

46. scp — secure copy (remote file copy program)

#copy test to host as username
$ scp test username@host:~

47. rsync — a fast, versatile, remote (and local) file-copying tool

#copy test to host as username
$ rsync -avz test username@host:~

48. ping — send ICMP ECHO_REQUEST to network hosts

#ping google.com
$ ping www.google.com

51. whois — client for the whois directory service

#show whois information for google.com
$ whois www.google.com

52. dig — DNS lookup utility

#show DNS information for google.com
$ dig www.google.com

53. host — DNS lookup utility

#show DNS information for google.com
$ host www.google.com

54. nslookup — query Internet name servers interactively

#show DNS information for google.com
$ nslookup www.google.com

55. killall — kill processes by name

#kill all firefox processes
$ killall firefox

56. passwd — change user password

#change password
$ passwd

57. ssh-keygen — SSH key pair generator

#generate ssh key pair
$ ssh-keygen
```
username@system ~ % ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/user/.ssh/id_rsa): <file>
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /Users/user/Desktop/<file>
Your public key has been saved in /Users/user/Desktop/<file>.pub
The key fingerprint is:
SHA256:pOBSQpaasAocDuprcuw+VyN8AVlY62uJ3EFXCqdjRoQ username@system.local
The key's randomart image is:
+---[RSA 3072]----+
|  o. B* . .      |
|o+. E. = o       |
|*+o o.B +        |
|*o + *.=         |
|+ ... +.S        |
|.. ooo++         |
| .. o+=.         |
|.o+ ..           |
|o+oo             |
+----[SHA256]-----+
```

58. ssh-copy-id — install SSH public key

#copy ssh public key to host as username
$ ssh-copy-id username@host

59. ssh-agent — run a program with an identity from an authentication agent

#run bash with ssh-agent
$ ssh-agent bash

60. ssh-add — add identities to the authentication agent

#add ssh private key to ssh-agent
$ ssh-add ~/.ssh/id_rsa

61. ssh — secure shell (remote login program)

#login to host as username
$ ssh username@host

62. scp — secure copy (remote file copy program)

#copy test to host as username
$ scp test username@host:~

63. rsync — a fast, versatile, remote (and local) file-copying tool

#copy test to host as username
$ rsync -avz test username@host:~

64. lsof — list open files

#list open files
$ lsof

65. ssh — secure shell (remote login program)

#tunnel port 80 on host as username to port 8080 on localhost
$ ssh -L 8080:localhost:80 username@host

* `ifconfig` — configure a network interface;

```
username@system ~ % ifconfig
lo0: flags=8049<UP,LOOPBACK,RUNNING,MULTICAST> mtu 16384
	options=1203<RXCSUM,TXCSUM,TXSTATUS,SW_TIMESTAMP>
	inet 127.0.0.1 netmask 0xff000000 
	inet6 ::1 prefixlen 128 
	inet6 fe80::1%lo0 prefixlen 64 scopeid 0x1 
	nd6 options=201<PERFORMNUD,DAD>
gif0: flags=8010<POINTOPOINT,MULTICAST> mtu 1280
stf0: flags=0<> mtu 1280
XHC20: flags=0<> mtu 0
en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=400<CHANNEL_IO>
	ether 30:35:ad:db:d2:60 
	inet 192.168.0.10 netmask 0xffffff00 broadcast 192.168.0.255
	media: autoselect
	status: active
en1: flags=8963<UP,BROADCAST,SMART,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1500
	options=460<TSO4,TSO6,CHANNEL_IO>
	ether 82:18:46:04:f4:00 
	media: autoselect <full-duplex>
	status: inactive
bridge0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=63<RXCSUM,TXCSUM,TSO4,TSO6>
	ether 82:18:46:04:f4:00 
	Configuration:
		id 0:0:0:0:0:0 priority 0 hellotime 0 fwddelay 0
		maxage 0 holdcnt 0 proto stp maxaddr 100 timeout 1200
		root id 0:0:0:0:0:0 priority 0 ifcost 0 port 0
		ipfilter disabled flags 0x0
	member: en1 flags=3<LEARNING,DISCOVER>
	        ifmaxaddr 0 port 6 priority 0 path cost 0
	media: <unknown type>
	status: inactive
p2p0: flags=8843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST> mtu 2304
	options=400<CHANNEL_IO>
	ether 02:35:ad:db:d2:60 
	media: autoselect
	status: inactive
awdl0: flags=8943<UP,BROADCAST,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1484
	options=400<CHANNEL_IO>
	ether e2:4f:45:97:aa:d5 
	inet6 fe80::e04f:45ff:fe97:aad5%awdl0 prefixlen 64 scopeid 0x9 
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect
	status: active
llw0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=400<CHANNEL_IO>
	ether e2:4f:45:97:aa:d5 
	inet6 fe80::e04f:45ff:fe97:aad5%llw0 prefixlen 64 scopeid 0xa 
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect
	status: active
utun0: flags=8051<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 1380
	inet6 fe80::fe74:4102:cf53:76ad%utun0 prefixlen 64 scopeid 0xb 
	nd6 options=201<PERFORMNUD,DAD>
utun1: flags=8051<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 2000
	inet6 fe80::c9f4:2f34:b38c:4dd6%utun1 prefixlen 64 scopeid 0xc 
	nd6 options=201<PERFORMNUD,DAD>
utun2: flags=8051<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 1000
	inet6 fe80::ce81:b1c:bd2c:69e%utun2 prefixlen 64 scopeid 0xd 
	nd6 options=201<PERFORMNUD,DAD>
```

* netstat — print network connections, routing tables, interface statistics, masquerade connections, and multicast memberships

```
username@system ~ % netstat
Active Internet connections
Proto Recv-Q Send-Q  Local Address          Foreign Address        (state)    
tcp4       0      0  192.168.0.10.56320     140.227.186.35.b.https ESTABLISHED
tcp4       0      0  192.168.0.10.56319     ec2-52-88-40-31..https ESTABLISHED
tcp4       0      0  192.168.0.10.56302     le-in-f119.1e100.https ESTABLISHED
tcp4       0      0  192.168.0.10.56300     led03s03-in-f22..https ESTABLISHED
tcp4       0      0  192.168.0.10.56299     led03s03-in-f22..https ESTABLISHED
tcp4       0      0  192.168.0.10.55544     srv129-129-240-8.https ESTABLISHED
tcp4       0      0  192.168.0.10.55527     lg-in-f198.1e100.https ESTABLISHED
tcp4       0      0  192.168.0.10.55516     srv131-129-240-8.https ESTABLISHED
tcp4       0      0  192.168.0.10.55514     lg-in-f109.1e100.imaps ESTABLISHED
tcp4       0      0  192.168.0.10.55510     149.154.167.41.https   ESTABLISHED
tcp4       0      0  192.168.0.10.55436     149.154.167.51.https   ESTABLISHED
tcp4       0      0  192.168.0.10.55422     srv67-132-240-87.https ESTABLISHED
tcp4       0      0  192.168.0.10.55405     ec2-35-173-89-61.https ESTABLISHED
tcp4       0      0  192.168.0.10.53575     mail.protei.ru.imap    ESTABLISHED
tcp4       0      0  192.168.0.10.53569     ec2-52-88-218-18.https ESTABLISHED
tcp4       0      0  192.168.0.10.53563     srv90-190-240-87.https ESTABLISHED
tcp4       0      0  192.168.0.10.53559     104.16.249.249.https   ESTABLISHED
tcp4       0      0  192.168.0.10.53379     17.57.146.136.5223     ESTABLISHED
tcp4       0      0  192.168.0.10.56289     srv139-227.vkont.https TIME_WAIT  
tcp4       0      0  192.168.0.10.56290     srv7-169-240-87..https TIME_WAIT  
tcp4       0      0  192.168.0.10.56291     srv8-169-240-87..https TIME_WAIT  
tcp4       0      0  192.168.0.10.56292     srv141-185-240-8.https TIME_WAIT  
tcp4       0      0  192.168.0.10.56288     ip158.156.mycdn..https TIME_WAIT  
udp4       0      0  *.53389                *.*                               
Active Multipath Internet connections
Proto/ID  Flags      Local Address          Foreign Address        (state)    
Active LOCAL (UNIX) domain sockets
Address          Type   Recv-Q Send-Q            Inode             Conn             Refs          Nextref Addr
86eeee119f6da199 stream      0      0                0 86eeee119f6d9a91                0                0 /var/run/mDNSResponder
86eeee119f6d9a91 stream      0      0                0 86eeee119f6da199                0                0
86eeee119f6de5e9 stream      0      0                0 86eeee119f6ddd51                0                0
86eeee119f6ddd51 stream      0      0                0 86eeee119f6de5e9                0                0
86eeee119f6d3a91 stream      0      0                0 86eeee119f6d99c9                0                0
86eeee119f6d99c9 stream      0      0                0 86eeee119f6d3a91                0                0
86eeee119f6d7d51 stream      0      0                0 86eeee119f6db391                0                0
86eeee119f6db391 stream      0      0                0 86eeee119f6d7d51                0                0
86eeee119f6d4fa9 stream      0      0                0 86eeee119f6d4ee1                0                0 /var/run/mDNSResponder
86eeee119f6d4ee1 stream      0      0                0 86eeee119f6d4fa9                0                0
86eeee119f6d4c89 stream      0      0                0 86eeee119f6d4bc1                0                0 /var/run/mDNSResponder
86eeee119f6d4bc1 stream      0      0                0 86eeee119f6d4c89                0                0
86eeee119f6d4a31 stream      0      0                0 86eeee119f6d77d9                0                0 /var/run/mDNSResponder
86eeee119f6d77d9 stream      0      0                0 86eeee119f6d4a31                0                0
86eeee119f6d9771 stream      0      0                0 86eeee119f6db841                0                0 /var/run/mDNSResponder
86eeee119f6db841 stream      0      0                0 86eeee119f6d9771                0                0
86eeee119f6d5fa1 stream      0      0                0 86eeee119f6ddee1                0                0 /var/run/mDNSResponder
86eeee119f6ddee1 stream      0      0                0 86eeee119f6d5fa1                0                0
86eeee119f6d8521 stream      0      0                0 86eeee119f6d8459                0                0
86eeee119f6d8459 stream      0      0                0 86eeee119f6d8521                0                0
86eeee119f6d9069 stream      0      0                0 86eeee119f6d8d49                0                0
86eeee119f6d8d49 stream      0      0                0 86eeee119f6d9069                0                0
86eeee119f6d6db1 stream      0      0                0 86eeee119f6d9131                0                0 /var/run/usbmuxd
86eeee119f6d9131 stream      0      0                0 86eeee119f6d6db1                0                0
Registered kernel control modules
id       flags    pcbcount rcvbuf   sndbuf   name 
       1        9        1   131072   131072 com.apple.flow-divert 
       2        1       25    65536    65536 com.apple.net.netagent 
       3        9        0   524288   524288 com.apple.content-filter 
       4       29        3   524288   524288 com.apple.net.utun_control 
       5       21        0    65536    65536 com.apple.net.ipsec_control 
       6        0       39     8192     2048 com.apple.netsrc 
       7       18        3     8192     2048 com.apple.network.statistics 
       8        5        0     8192    32768 com.apple.network.tcp_ccdebug 
       9        1        0     8192     2048 com.apple.network.advisory 
       a        1        0    65536    65536 com.apple.net.rvi_control 
       b        1        1    16384     2048 com.apple.nke.sockwall 
Active kernel event sockets
Proto Recv-Q Send-Q vendor  class  subcl
kevt       0      0      1      1      2
kevt       0      0      1      1      2
kevt       0      0      1      1     11
kevt       0      0      1      6      1
kevt       0      0      1      6      1
kevt       0      0      1      6      1
kevt       0      0      1      6      1
kevt       0      0      1      6      1
kevt       0      0      1      6      1
kevt       0      0      1      6      1
kevt       0      0      1      6      1
kevt       0      0      1      1      2
kevt       0      0      1      6      1
kevt       0      0      1      1     10
kevt       0      0   1001      5     11
kevt       0      0      1      1      7
kevt       0      0      1      1      1
kevt       0      0      1      1      2
kevt       0      0      1      1      0
kevt       0      0      1      3      3
Active kernel control sockets
Proto Recv-Q Send-Q   unit     id name
kctl       0      0      1      1 com.apple.flow-divert
kctl       0      0      1      2 com.apple.net.netagent
kctl       0      0      2      2 com.apple.net.netagent
kctl       0      0      3      2 com.apple.net.netagent
kctl       0      0      4      2 com.apple.net.netagent
kctl       0      0      5      2 com.apple.net.netagent
kctl       0      0      6      2 com.apple.net.netagent
kctl       0      0      7      2 com.apple.net.netagent
kctl       0      0      8      2 com.apple.net.netagent
kctl       0      0      9      2 com.apple.net.netagent
kctl       0      0     10      2 com.apple.net.netagent
kctl       0      0     11      2 com.apple.net.netagent
kctl       0      0     12      2 com.apple.net.netagent
kctl       0      0     13      2 com.apple.net.netagent
kctl       0      0     14      2 com.apple.net.netagent
kctl       0      0     15      2 com.apple.net.netagent
kctl       0      0     16      2 com.apple.net.netagent
kctl       0      0     17      2 com.apple.net.netagent
kctl       0      0     18      2 com.apple.net.netagent
kctl       0      0     19      2 com.apple.net.netagent
kctl       0      0     20      2 com.apple.net.netagent
kctl       0      0     21      2 com.apple.net.netagent
kctl       0      0     22      2 com.apple.net.netagent
kctl       0      0     23      2 com.apple.net.netagent
kctl       0      0     24      2 com.apple.net.netagent
kctl       0      0     25      2 com.apple.net.netagent
kctl       0      0      1      4 com.apple.net.utun_control
kctl       0      0      2      4 com.apple.net.utun_control
kctl       0      0      3      4 com.apple.net.utun_control
kctl       0      0      1      6 com.apple.netsrc
kctl       0      0      2      6 com.apple.netsrc
kctl       0      0      3      6 com.apple.netsrc
kctl       0      0      4      6 com.apple.netsrc
kctl       0      0      5      6 com.apple.netsrc
kctl       0      0      6      6 com.apple.netsrc
kctl       0      0      7      6 com.apple.netsrc
kctl       0      0      8      6 com.apple.netsrc
kctl       0      0      9      6 com.apple.netsrc
kctl       0      0     10      6 com.apple.netsrc
kctl       0      0     11      6 com.apple.netsrc
kctl       0      0     12      6 com.apple.netsrc
kctl       0      0     13      6 com.apple.netsrc
kctl       0      0     14      6 com.apple.netsrc
kctl       0      0     15      6 com.apple.netsrc
kctl       0      0     16      6 com.apple.netsrc
kctl       0      0     17      6 com.apple.netsrc
kctl       0      0     18      6 com.apple.netsrc
kctl       0      0     19      6 com.apple.netsrc
kctl       0      0     20      6 com.apple.netsrc
kctl       0      0     21      6 com.apple.netsrc
kctl       0      0     22      6 com.apple.netsrc
kctl       0      0     23      6 com.apple.netsrc
kctl       0      0     24      6 com.apple.netsrc
kctl       0      0     25      6 com.apple.netsrc
kctl       0      0     26      6 com.apple.netsrc
kctl       0      0     27      6 com.apple.netsrc
kctl       0      0     28      6 com.apple.netsrc
kctl       0      0     29      6 com.apple.netsrc
kctl       0      0     30      6 com.apple.netsrc
kctl       0      0     31      6 com.apple.netsrc
kctl       0      0     32      6 com.apple.netsrc
kctl       0      0     33      6 com.apple.netsrc
kctl       0      0     34      6 com.apple.netsrc
kctl       0      0     35      6 com.apple.netsrc
kctl       0      0     36      6 com.apple.netsrc
kctl       0      0     37      6 com.apple.netsrc
kctl       0      0     39      6 com.apple.netsrc
kctl       0      0     40      6 com.apple.netsrc
kctl       0      0      1      7 com.apple.network.statistics
kctl   69472      0      2      7 com.apple.network.statistics
kctl       0      0      3      7 com.apple.network.statistics
kctl       0      0      1     11 com.apple.nke.sockwall
```

* sshfs — filesystem client based on SSH File Transfer Protocol

#mount host as ~/host
$ sshfs username@host:~ ~/host

* awk  — pattern scanning and processing language

```
username@system ~ % awk { print $2, $1 }
```

70. sed — stream editor for filtering and transforming text

#replace Hello with Hi in test
username@system ~ % sed 's/Hello/Hi/g' test

71. crontab — maintain crontab files

#edit crontab
$ crontab -e

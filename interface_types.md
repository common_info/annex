# Типы интерфейсов #
3GPP TS 29.275

| Code | Interface |
| :--- | :-------- |
| 0 | S1-U eNodeB GTP-U |
| 1 | S1-U SGW GTP-U |
| 2 | S12 RNC GTP-U |
| 3 | S12 SGW GTP-U |
| 4 | S5/S8 SGW GTP-U |
| 5 | S5/S8 PGW GTP-U |
| 6 | S5/S8 SGW GTP-C |
| 7 | S5/S8 PGW GTP-C |
| 8 | S5/S8 SGW PMIPv6 (32-битный ключ GRE закодирован в 32-битном поле TEID) |
| 9 | S5/S8 PGW PMIPv6 (32-битный ключ GRE закодирован в 32-битном поле TEID) |
| 10 | S11 MME GTP-C |
| 11 | S11/S4 SGW GTP-C |
| 12 | S10/N26 MME GTP-C |
| 13 | S3 MME GTP-C |
| 14 | S3 SGSN GTP-C |
| 15 | S4 SGSN GTP-U |
| 16 | S4 SGW GTP-U |
| 17 | S4 SGSN GTP-C |
| 18 | S16 SGSN GTP-C |
| 19 | eNodeB/gNodeB GTP-U для трансляции данных по нисходящей линии |
| 20 | eNodeB GTP-U для трансляции данных по восходящей линии |
| 21 | RNC GTP-U для трансляции данных |
| 22 | SGSN GTP-U interface для трансляции данных |
| 23 | SGW/UPF GTP-U для трансляции данных по нисходящей линии |
| 24 | Sm MBMS GW GTP-C |
| 25 | Sn MBMS GW GTP-C |
| 26 | Sm MME GTP-C |
| 27 | Sn SGSN GTP-C |
| 28 | SGW GTP-U для трансляции данных по восходящей линии |
| 29 | Sn SGSN GTP-U |
| 30 | S2b ePDG GTP-C |
| 31 | S2b-U ePDG GTP-U |
| 32 | S2b PGW GTP-C |
| 33 | S2b-U PGW GTP-U |
| 34 | S2a TWAN GTP-U |
| 35 | S2a TWAN GTP-C |
| 36 | S2a PGW GTP-C |
| 37 | S2a PGW GTP-U |
| 38 | S11 MME GTP-U |
| 39 | S11 SGW GTP-U |
| 40 | N26 AMF GTP-C |
| 41 | N19mb UPF GTP-U |

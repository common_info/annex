## Error indications related to mobile terminated short message transfer which may be transferred to the originating SC ##

| Error indication | S | Meaning |
| ---------------- | - | ------- |
| Unknown subscriber | P | The PLMN rejects the short message TPDU because there is not allocated an IMSI or a directory number for the mobile subscriber in the HLR (see 3GPP TS 29.002). |
| Teleservice not provisioned | P | The PLMN rejects the short message TPDU because the recipient MS has no SMS subscription (see 3GPP TS 29.002). |
| Call barred | T | The PLMN rejects the short message TPDU due to barring of the MS (see 3GPP TS 29.002, description of the Barring supplementary service, 3GPP TS 22.004 and 3GPP TS 23.011), description of Call barred due to Unauthorised Message Originator, 3GPP TS 29.002, and description of Operator Determined Barring, 3GPP TS 22.041 and 3GPP TS 23.015). |
| Facility not supported | T | The VPLMN rejects the short message TPDU due to no provision of the SMS in the VPLMN (see 3GPP TS 29.002). |
| Absent subscriber | T | The PLMN rejects the short message TPDU because:<br>- there was no paging response via the SGSN, MSC or both (see GSM 44.008 & 3GPP TS 29.002);<br>- the IMSI GPRS or both records are marked detached (see 3GPP TS 29.002);<br>- the MS is subject to roaming restrictions (see "Roaming not allowed", 3GPP TS 29.002);<br>- deregistered in the HLR. The HLR does not have an MSC, SGSN or both numbers stored for the target MS (see 3GPP TS 29.002);<br>- Unidentified subscriber (see 3GPP TS 29.002);<br>- MS purged (see 3GPP TS 29.002);<br>- the MS is not registered in the HSS/HLR for IMS;<br>- there was no SIP response received by the IP-SM-GW;<br>- the MS is temporarily unavailable (e.g. in power saving mode due to eDRX). |
| MS busy for MT SMS | T | The PLMN rejects the short message TPDU because of congestion encountered at the visited MSC or the SGSN. Possible reasons include any of the following events in progress:<br>- short message delivery from another SC;<br>- IMSI or GPRS detach;<br>- Location Update or Inter SGSN Routing Area Update;<br>- paging;<br>- emergency call;<br>- call setup. |
| SMS lower layers capabilities not provisioned | T | The PLMN rejects the short message TPDU due to MS not being able to support the Short Message Service.<br>The short message transfer attempt is rejected either due to information contained in the class-mark, or the MSC not being able to establish connection at SAPI = 3 (see GSM 44.008 and 3GPP TS 29.002). |
| Error in MS | T | The PLMN rejects the short message TPDU due to an error occurring within the MS at reception of a short message, e.g. protocol error. |
| Illegal Subscriber | P | The PLMN rejects the short message TPDU because the MS failed authentication. |
| Illegal Equipment | P | The PLMN rejects the short message TPDU because the IMEI of the MS was black-listed in the EIR. |
| System failure | T | The PLMN rejects the short message TPDU due to network or protocol failure others than those listed above (see 3GPP TS 29.002). |
| Memory Capacity Exceeded | T | The MS rejects the short message since it has no memory capacity available to store the message. |

**Status (Permanent or Temporary)**<br>
The relation between the two sets of error indications is given in the table above. Each error is classified as either "Temporary" or "Permanent". This classification gives an indication of whether or not it is probable that the MS becomes attainable within a reasonable period, and so provides the recommended action to be taken by the SC, i.e. either to store the message for later transfer, or to discard it.

### Assignment of values to reasons for absence ###

**Values must be in the range of 0 to 255.**

| Values | Reason for absence |
| ------ | ------------------ |
| 0 | No paging response via the MSC |
| 1 | IMSI detached |
| 2 | Roaming restriction |
| 3 | Deregistered in the HLR for non GPRS |
| 4 | MS purged for non GPRS |
| 5 | No paging response via the SGSN |
| 6 | GPRS detached |
| 7 | Deregistered in the HLR for GPRS |
| 8 | MS purged for GPRS |
| 9 | Unidentified subscriber via the MSC |
| 10 | Unidentified subscriber via the SGSN |
| 11 | Deregistered in the HSS/HLR for IMS |
| 12 | No response via the IP-SM-GW |
| 13 | The MS is temporarily unavailable |

All 'non GPRS' reasons (except for roaming restriction) can be combined with all 'GPRS' reasons and vice-versa.<br>
All other integer values are reserved.

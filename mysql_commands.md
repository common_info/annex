# MySQL Commands #

| Command | Description |
| ------- | ----------- |
| show databases; | output all available databases |
| use `<database_name>`; | connect to the database `<database_name>` |
| show tables; | output all tables in the database |
| show columns from `<table_name>`; | output table column information |
| exit; | leave the server |

# Запросы GTP #

| Код | Запрос | Описание |
| :-- | :----- | :------- |
| 1 | Echo Request | Запрос "эхо" |
| 2 | Echo Response | Ответ "эхо" |
| 3 | Version Not Supported Indication | Версия не поддерживается |
|  | От SGSN/MME к MSC-серверу, при хэндовере -- Sv |  |
| 25 | SRVCC PS to CS Request | Запрос SRVCC при переходе от сети с коммутацией пакетов к сети с коммутацией каналов |
| 26 | SRVCC PS to CS Response | Ответ на запрос SRVCC при переходе от сети с коммутацией пакетов к сети с коммутацией каналов |
| 27 | SRVCC PS to CS Complete Notification | Уведомление о выполнении SRVCC при переходе от сети с коммутацией пакетов к сети с коммутацией каналов |
| 28 | SRVCC PS to CS Complete Acknowledge | Подтверждение выполнения SRVCC при переходе от сети с коммутацией пакетов к сети с коммутацией каналов |
| 29 | SRVCC PS to CS Cancel Notification | Уведомление о завершении SRVCC при переходе от сети с коммутацией пакетов к сети с коммутацией каналов |
| 30 | SRVCC PS to CS Cancel Acknowledge | Подтверждение завершения SRVCC при переходе от сети с коммутацией пакетов к сети с коммутацией каналов |
|  | От SGSN/MME к PGW -- S4/S11, S5/S8 |  |
| 32 | Create Session Request | Запрос создания сеанса |
| 33 | Create Session Response | Ответ на запрос создания сеанса |
| 34 | Modify Bearer Request | Запрос изменения носителя |
| 35 | Modify Bearer Response | Ответ на запрос изменения канала передачи данных |
| 36 | Delete Session Request | Запрос удаления сеанса |
| 37 | Delete Session Response | Ответ на запрос удаления сеанса |
| 38 | Change Notification Request | Запрос уведомления об изменении |
| 39 | Change Notification Response | Ответ на запрос уведомления об изменении |
| 164 | Resume Notification | Уведомление о возобновлении связи |
| 165 | Resume Acknowledge | Подтверждение возобновления связи |
|  | Messages without explicit response (Сообщения без явного ответа) |  |
| 64 | Modify Bearer Command | Команда изменения канала передачи данных, MME/SGSN к PGW - S11/S4, S5/S8 |
| 65 | Modify Bearer Failure Indication | Индикация неудачного изменения канала передачи данных, PGW к MME/SGSN - S5/S8, S11/S4 |
| 66 | Delete Bearer Command | Команда освобождения канала передачи данных, MME/SGSN к PGW - S11/S4, S5/S8 |
| 67 | Delete Bearer Failure Indication | Индикация неудачного освобождения канала передачи данных, PGW к MME/SGSN - S5/S8, S11/S4 |
| 68 | Bearer Resource Command | Команда распределения ресурсов канала передачи данных, MME/SGSN к PGW - S11/S4, S5/S8 |
| 69 | Bearer Resource Failure Indication | Индикация неудачного распределения ресурсов канала передачи данных, PGW к MME/SGSN - S5/S8, S11/S4 |
| 70 | Downlink Data Notification Failure Indication | Индикация неудачного уведомления о передаче данных "вниз", SGSN/MME к SGW - S4/S11 |
| 71 | Trace Session Activation | Активация сеанса трассировки, MME/SGSN к PGW - S11/S4, S5/S8 |
| 72 | Trace Session Deactivation | Деактивация сеанса трассировки MME/SGSN к PGW - S11/S4, S5/S8 |
| 73 | Stop Paging Indication | Индикация остановки поиска SGW к MME/SGSN - S11/S4 |
|  | От PGW к SGSN/MME -- S5/S8, S4/S11 |  |
| 95 | Create Bearer Request | Запрос активации канала передачи данных |
| 96 | Create Bearer Response | Ответ на запрос активации канала передачи данных |
| 97 | Update Bearer Request | Запрос обновления канала передачи данных |
| 98 | Update Bearer Response | Ответ на запрос обновления канала передачи данных |
| 99 | Delete Bearer Request | Запрос освобождения канала передачи данных |
| 100 | Delete Bearer Response | Ответ на запрос освобождения канала передачи данных |
|  | От PGW к ММЕ, от ММЕ к PGW, от SGW к PGW, от SGW к ММЕ -- S5/S8, S11 |  |
| 101 | Delete PDN Connection Set Request | Запрос удаления соединения |
| 102 | Delete PDN Connection Set Response | Ответ на запрос удаления соединения |
|  | От ММЕ к ММЕ, от SGSN к ММЕ, от ММЕ к SGSN, от SGSN к SGSN -- S3/S10/S16 |  |
| 128 | Identification Request | Запрос идентификации |
| 129 | Identification Response | Ответ на запрос идентификации |
| 130 | Context Request | Запрос контекста |
| 131 | Context Response | Ответ на запрос контекста |
| 132 | Context Acknowledge | Подтверждение ответа на запрос контекста |
| 133 | Forward Relocation Request | Запрос передачи при перемещении АС |
| 134 | Forward Relocation Response | Ответ на запрос передачи при перемещении АС |
| 135 | Forward Relocation Complete Notification | Уведомление выполнения передачи при перемещении АС |
| 136 | Forward Relocation Complete Acknowledge | Подтверждение выполнения передачи при перемещении АС |
| 137 | Forward Access Context Notification | Уведомление о передаче контекста |
| 138 | Forward Access Context Acknowledge | Подтверждение передачи контекста |
| 139 | Relocation Cancel Request | Запрос отмены перемещения |
| 140 | Relocation Cancel Response | Ответ на запрос отмены перемещения |
| 141 | Configuration Transfer Tunnel | Конфигурация туннеля передачи |
| 152 | RAN Information Relay | Передача информации сети радио доступа |
|  | От SGSN к ММЕ, от ММЕ к SGSN -- S3 |  |
| 149 | Detach Notification | Уведомление об отключении |
| 150 | Detach Acknowledge | Подтверждение отключения |
| 151 | CS Paging Indication | Индикация поиска в сети с коммутацией каналов |
| 153 | Alert ММЕ Notification | Уведомление ММЕ |
| 154 | Alert ММЕ Acknowledge | Подтверждение на уведомление ММЕ |
| 155 | UE Activity Notification | Уведомление активации AC |
| 156 | UE Activity Acknowledge | Подтверждение активации AC |
|  | От SGSN/MME к SGW, от SGSN к ММЕ -- S4/S11/S3, от SGSN к SGSN -- S16, от SGW к PGW -- S5/S8 |  |
| 162 | Suspend Notification | Уведомление о прерывании связи |
| 163 | Suspend Acknowledge | Подтверждение прерывания связи |
|  | От SGSN/MME к SGW -- S4/S11 |  |
| 160 | Create Forwarding Tunnel Request | Запрос создания туннеля передачи |
| 161 | Create Forwarding Tunnel Response | Ответ на запрос создания туннеля передачи |
| 166 | Create Indirect Data Forwarding Tunnel Request | Запрос создания туннеля передачи косвенных данных |
| 167 | Create Indirect Data Forwarding Tunnel Response | Ответ на запрос создания туннеля передачи косвенных данных |
| 168 | Delete Indirect Data Forwarding Tunnel Request | Запрос удаления туннеля передачи косвенных данных |
| 169 | Delete Indirect Data Forwarding Tunnel Response | Ответ на запрос удаления туннеля передачи косвенных данных |
| 170 | Release Access Bearers Request | Запрос освобождения доступа к каналу передачи данных |
| 171 | Release Access Bearers Response | Ответ на запрос освобождения доступа к каналу передачи данных |
|  | От SGW к SGSN/MME -- S4/S11 |  |
| 176 | Downlink Data Notification | Уведомление о передаче данных "вниз" |
| 177 | Downlink Data Notification Acknowledge | Подтверждение уведомления о передаче данных "вниз" |
| 179 | PGW Restart Notification | Уведомление о рестарте PGW |
| 180 | PGW Restart Notification Acknowledge | Подтверждение на уведомление о рестарте PGW |
|  | От SGW к PGW, от PGW к SGW -- S5/S8 |  |
| 200 | Update PDN Connection Set Request | Запрос обновления соединения |
| 201 | Update PDN Connection Set Response | Ответ на запрос удаления соединения |
|  | От ММЕ к SGW -- S11 |  |
| 211 | Modify Access Bearers Request | Запрос изменения канала доступа |
| 212 | Modify Access Bearers Response | Ответ на запрос изменения канала доступа |

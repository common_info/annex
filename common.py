from enum import Enum


class Zones(Enum):
    ZONE_149_255_118_70 = ("PCSCF", "ICSCF", "SCSCF", "MKD", "SBC", "SSW4", "TAS")
    ZONE_192_168_110_116 = ("SN2",)
    ZONE_192_168_110_189 = ("Protei_SB", "Protei_EIR"),
    ZONE_192_168_110_19 = ("SMSFW",),
    ZONE_192_168_126_153 = ("Protei_CAPL", "Protei_SigtranTester", "Protei_STP", "Protei_CAPL_extSCP"),
    ZONE_192_168_126_186 = (
                               "Protei_IPSMGW", "Protei_SRF", "Protei_SCL", "Protei_SMPP_BS", "Protei_STP",
                               "Protei_SCL_GIS", "Protei_SMSC", "Protei_SMSC_2"
                           ),
    ZONE_192_168_126_3 = (
                             "GLR_API", "Protei_HSS", "Protei_SC_Lite", "HLR_Load", "Protei_ITG", "protei_srf",
                             "HSS_API", "Protei_SS7FW", "protei_mv_radius", "MI_API", "Protei_OTA_SMS", "Protei_STP",
                             "CLI-Server", "Protei_CAPL", "Protei_RDS", "Protei_STP2", "Protei_DRA", "Protei_RG",
                             "Protei_UDM", "Protei_GMSC", "SB"
                         ),
    ZONE_192_168_77_54 = ("SS7FW",)


class CommonInfo:
    def __init__(self):
        self.dict_items: dict = dict()
        self.dict_items["zones"] = Zones.__members__

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.dict_items.keys()
        else:
            return item in self.dict_items.values()

    def __getitem__(self, item):
        if item in self:
            return self.dict_items.__dict__[item]
        else:
            raise KeyError()

    def __setitem__(self, key, value):
        if key in self:
            self.dict_items.__dict__[key] = value
        else:
            return

    def __getattribute__(self, item):
        if isinstance(item, str):
            if item in self:
                return self.dict_items[item]
            else:
                raise AttributeError()

    def print_info(self, attr: str):
        return getattr(self, attr)


def main():
    pass


if __name__ == "__main__":
    main()

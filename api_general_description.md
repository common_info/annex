# HTTP Request Description #

## Main Operating Principles ##

Requests are handled in synchronous mode. Each client request is processed by the server party and the answer is returned on a synchronous basis.<br>
Requests are sent through the HTTP protocol.<br>
The requests and responses are constructed in JSON.<br>
The JSON requests and responses are based on provided JSON schemes. Each request supports one operation.<br>
The application uses one endpoint URL, webhook, to send requests to the RBT:

`http(s)://{ip}:{port}/{request_endpoint}`

* ip: IP address of the PROTEI RBT API or DNS name;
* port: port number to establish the connection;
* request_endpoint: specified URL to send the request.

**Note.** Most requests validate the Accept header value. To receive the proper response, set the Accept header to the value given in the appropriate request or accept all types:

`-H 'accept */*'`

The system is using the API Key Authorization (Cookie).

## Common Request Parts ##

To use API requests, the user has to provide the JSESSIONID value in the Cookie header in each request.<br>
Sample:
```http
curl -X 'GET' \
  '/api/api_entity' \
  -H 'accept: */*'
  -H 'Content-Type: application/json' \
  -H 'Cookie: JSESSIONID=580E95EFC4B77BBC2B588345E1E3B0CC'
  -d '{}'
```
If the user does not have the JSESSIONID or the current is expired:
* send the log in request:
```http
curl -X 'POST' \
  '/api/subscriber/login' \
  -H 'Content-Type: application/x-www-form-urlencoded'
  -d 'username={username}&password={password}'
```
* if the request is successful, get the JSESSIONID value from the Set-Cookie header in the response:
```http
Set-Cookie: JSESSIONID=580E95EFC4B77BBC2B588345E1E3B0CC; Path=/;
Expires: Wed, 1 Jan 1970 12:00:00 GMT
```

**Note.** Further, these parts are not mentioned, but setting the headers is implied.

## Common Response Parts ##

Any system response contains the following part:
```json
{
  "code": "<response_code>",
  "message": "<message_description>"
}
```
* code – HTTP request code; All the codes are provided in [RFC 7231](https://datatracker.ietf.org/doc/html/rfc7231) and [IANA assignments](https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml);
* message – message text for the appropriate response code.

**Note.** The system response is not described in the document further if there is no object or information except for the status.<br>
Specified codes and message bodies are provided in the request descriptions.

## Data Types and Nullable ##

The table below provides the description of the data types. The full description is provided in the [MariaDB Knowledge Base](https://mariadb.com/kb/en/).

**Note.** The "U" character after the type is a short name for UNSIGNED.

| Type | Description |
| :--- | :---------- |
| binary | Sequence of bytes to present a non-text file. |
| bool | Boolean value that specifies a flag: either false or true. |
| float | Numeric 32-bit value with a floating radix point. |
| hex | Numeric integer in the base of 16. Format: the digits of 0–9, A, B, C, D, E, F. |
| int32 | Numeric 32-bit integer. Range: from –2 ** 31 to 2 ** 31 – 1. |
| int64 | Numeric 64-bit integer. Range: from –2 ** 63 to 2 ** 63 – 1. |
| list | Finite sequence that contains a number of items of the same type or structure. |
| object | Tuple that contains a predefined number of type-different parameters. |
| string | Text value. Format: the Latin letters, the digits of 0–9, special symbols, and punctuation marks. |

Mandatory parameters are marked with the asterisk (\*) sign.
